﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MRegistrarUpdater
{
    public class Config
    {
        public static Config singleton { get; } = new Config();
        public string updatePath { get; private set; }
        public string updateSource { get; private set; }

        private Config() { }

        public static void init()
        {
            try
            {
                var settings = ConfigurationManager.AppSettings;
                var config = singleton;
                config.updatePath = settings["updatePath"];
                config.updateSource = settings["updateSource"];
            }
            catch(Exception e)
            {

            }
        }
    }
}
