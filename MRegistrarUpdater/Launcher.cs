﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrarUpdater
{
    public class Launcher
    {
        internal static void startApp()
        {
            try
            {
                var filename = Path.Combine(Config.singleton.updatePath, "MRegistrar.exe");
                var psi = new ProcessStartInfo(filename) { WorkingDirectory = Config.singleton.updatePath, UseShellExecute=false };
                Process.Start(psi);
            }
            catch(Exception e)
            {
                //todo: log
            }
        }
    }
}
