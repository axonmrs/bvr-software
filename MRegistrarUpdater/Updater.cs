﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrarUpdater
{
    public static class Updater
    {
        public static (bool successful, string message) run(bool deleteWhenDone)
        {
            var config = Config.singleton;
            var files = getUpdates(config.updateSource);
            if (!files.Any()) return (true, "No updates found");
            var errorCount = 0;
            var okCount = 0;
            foreach(var file in files.OrderBy(x => x))
            {
                var ok = updateWith(file, config.updatePath);
                if (deleteWhenDone) deleteFile(file);
                if (ok) okCount++;
                else errorCount++;
            }
            if (errorCount == 0) return (true, "Update completed successfully");
            if (errorCount > 0) return (false, $"Some updates failed");
            return (true, "All updates completed successfully");
        }

        private static bool updateWith(string filename, string appFolder)
        {
            var tempFolder = Path.GetTempFileName();
            try
            {
                File.Delete(tempFolder); //delete the file
                Directory.CreateDirectory(tempFolder);
                ZipFile.ExtractToDirectory(filename, tempFolder);
                //copy the file
                foreach(var file in Directory.GetFiles(tempFolder, "*.*", SearchOption.AllDirectories))
                {
                    //todo: backup the existing file
                    Directory.CreateDirectory(Path.GetDirectoryName(file));
                    try
                    {
                        File.Copy(file, file.Replace(tempFolder, appFolder), true);
                        deleteFile(file);
                    }
                    catch(Exception e)
                    {
                        
                    }
                }
                deleteFolder(tempFolder);
                return true; //? what if some files failed?
            }
            catch {
                deleteFolder(tempFolder);
                return false;
            }
        }

        private static void deleteFile(string filename)
        {
            try { File.Delete(filename); } catch { }
        }

        private static void deleteFolder(string name)
        {
            try { Directory.Delete(name, true); } catch { }
        }

        private static string[] getUpdates(string path)
        {
            return Directory.GetFiles(path, "*.mrx");
        }
    }
}
