﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MRegistrarUpdater
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Config.init();
            Task.Run(() =>
            {
                setMessage("Initializing...");
                var result = Updater.run(true);
                if (!result.successful)
                {
                    //todo: log the failure
                }
                setMessage("Starting...");
                Launcher.startApp();
                close();
            });
        }

        private void setMessage(string msg)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new SetMessage(setMessage), msg);
                return;
            }
            progressLabel.Text = msg;
        }

        private void close()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(close));
                return;
            }
            Close();
        }

        private delegate void SetMessage(string msg);
    }
}
