﻿using System;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels.Interfaces
{
    public interface IViewSettings
    {
        bool showTray { get; }
        bool showStatusSummaries { get; }
        bool enableLogOut { get; }
        Func<Task> onCancel { get; }
        Func<Task> onBack { get; }
        bool enableShutDown { get; }
        bool enableTransfer { get; }
        bool enableBackup { get; }
    }
}
