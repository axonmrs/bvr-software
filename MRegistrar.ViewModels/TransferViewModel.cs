﻿using MRegistrar.Config;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
//using System.Printing;

namespace MRegistrar.ViewModels
{
    public class TransferViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public RoutingState Router { get; }
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => null;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "Data Transfer";

        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public bool backupDriveReady { get; set; }
        public ReactiveCommand<Unit, Unit> clearError { get; }
        public ReactiveCommand<Unit, Unit> retry { get; }


        public TransferViewModel(IScreen screen = null)
        {
            try
            {
                HostScreen = screen ?? Locator.Current.GetService<IScreen>();
                retry = ReactiveCommand.Create(() =>
                {
                    try
                    {
                        processTransfer();
                    }
                    catch (Exception e)
                    {
                        errorMessage = e.GetBaseException().Message;
                    }
                });
                clearError = ReactiveCommand.Create(() =>
                {
                    //errorMessage = "";
                });
                processTransfer();
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }
        }

        private void processTransfer()
        {
            errorMessage = "Data Transfer process started ...";
            //todo: check if there are files to be transfered

            //move all old failed exports
            var exportsDir = Path.Combine(@".\Data", "Exports");
            if (!Directory.Exists(exportsDir)) Directory.CreateDirectory(exportsDir);

            //check connected drive
            var transferDrive = CheckConnectedDrive();

            //check available space
            CheckAvailableSpace(exportsDir, transferDrive);

            //move files to drive
            MoveFilesToDrive(exportsDir, transferDrive);

            //call for data to be exported
            //todo: get the key exchanged during the setup. salt is device reference
            var key = "MRegistrar";
            var salt = "DeviceReference";
            var res = Data.Registrations.transmit(key, salt);
            if (res.IsError)
            {
                errorMessage = res.ErrorValue.Message;
                return;
            }

            //check if item 1 is true
            if (!res.ResultValue.Item1)
            {
                errorMessage = res.ResultValue.Item2;
                return;
            }

            //move files to a permanent folder
            var tempDir = res.ResultValue.Item2;            
            var zipFiles = Directory.GetFiles(tempDir, "*.zip", SearchOption.AllDirectories);
            foreach (var file in zipFiles)
            {
                var path = Path.Combine(exportsDir, Path.GetFileName(file));
                File.Copy(file, path, true);
            }

            //delete temp folder
            Directory.Delete(tempDir, true);

            //check available space
            CheckAvailableSpace(exportsDir, transferDrive);

            //move files to drive
            MoveFilesToDrive(exportsDir, transferDrive);

            //return to the previous page
            errorMessage = "Data Transfer successful ...";
            GotoPreviousPage();
        }

        public void GotoPreviousPage()
        {
            if (Router == null) return;
            Router.NavigateBack.Execute();
        }

        public USBDevice CheckConnectedDrive()
        {
            //check that the backup drive is connected
            var transferDrive = new USBDevice(0, "", "", 0, "", "");
            var usbDevices = USBUtils.GetUSBDevices().ToList();
            usbDevices = usbDevices.Where(x => x.Label.ToLower().Contains(Configuration.Settings.TransferDriveLabelIdentifier.ToLower())).ToList();
            if (!usbDevices.Any()) throw new Exception("Please insert the transfer stick and start again");
            if (usbDevices.Count() > 1) throw new Exception("Please ensure that you have only one transfer stick inserted");
            return usbDevices.First();
            //var backupDrive = usbDevices.First();
            //transferDrive = usbDevices.FirstOrDefault(x => x.Label.ToLower().Contains(Configuration.Settings.PollingStationCode.ToLower()));
            
            ////if (transferDrive == null) throw new Exception("Transfer Drive Error:: Invalid transfer drive or transfer drive not meant for device.");

            ////check the authenticity of the transfer drive
            ////USBUtils.CheckAuthenticityOfDrive(transferDrive);

            //return transferDrive;
        }

        public void CheckAvailableSpace(string exportsDir, USBDevice transferDrive)
        {
            //check the size of the export folder
            var f = new DirectoryInfo(exportsDir);
            var exportsSize = f.EnumerateFiles().Sum(file => file.Length);

            //check the available space on the transfer drive
            if (exportsSize > transferDrive.AvailableSpace) throw new Exception("Transfer Drive Space Error:: Please check the available space on the Transfer drive.");
        }

        public void MoveFilesToDrive(string exportsDir, USBDevice transferDrive)
        {
            //copy files from the permanent folder to the sd card
            var files = Directory.GetFiles(exportsDir, "*.zip", SearchOption.AllDirectories);
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var fn = Path.GetFileName(file);
                    File.Move(file, Path.Combine(transferDrive.Root, fn));
                }
            }
        }
    }
}
