﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive;

namespace MRegistrar.ViewModels
{
    public class RegistrationEntryViewModel : ReactiveObject
    {
        [Reactive] public int id { get; set; }
        [Reactive] public int num { get; set; }
        [Reactive] public string voterId { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public string otherNames { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public byte[] photo { get; set; }
        public ReactiveCommand<Unit, Unit> viewRegistration { get; }

        public RegistrationEntryViewModel()
        {
            viewRegistration = ReactiveCommand.Create(() => openViewRegistration(voterId));
        }
        private void openViewRegistration(string voterId)
        {
            var result = Data.Registrations.find(voterId);
            if (result.IsOk)
            {
                var x = result.ResultValue;
                var vm = new PrintCardViewModel { age = x.estimatedAge, dateOfBirth = x.dateOfBirth, fatherName = x.fatherName, motherName = x.motherName, hometown = x.homeTown, hometownAddress = x.homeTownAddress, hometownDistrict = x.homeTownDistrict, hometownRegion = x.hometownRegion, idNumber = x.idNumber, idType = Data.Utils.getValueOrDefault("", x.idType), isDisabled = x.isDisabled, isVisuallyImpaired = x.isVisuallyImpaired, otherNames = x.otherNames, phoneNumber = x.phoneNumber, residentialAddress = x.residentialAddress, residentialDistrict = x.residentialDistrict, residentialTown = x.residentialTown, residentialRegion = x.residentialRegion, sex = x.sex, surname = x.surname, voterId = x.voterId, photo = x.photo, pollingStationCode = Data.App.config.code, name = $"{x.surname} {x.otherNames}", dateOfRegistration = x.dateRegistered, cardPrintCount = x.cardPrintCount};
                SharedViewModel.singleton.activeViewModel = vm;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
    }
}
