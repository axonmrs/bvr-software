﻿using MRegistrar.ViewModels.Helpers;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using static MRegistrar.Data.Utils;

namespace MRegistrar.ViewModels
{
    public class FindByVoterIdViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Registration of Existing Voter";

        public IScreen HostScreen { get; }

        [Reactive] public string voterId { get; set; }
        [Reactive] public string otherNames { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public DateTime dateOfBirth { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public string sex { get; set; }
        [Reactive] public string phoneNumber { get; set; }
        [Reactive] public string residentialAddress { get; set; }
        [Reactive] public string residentialTown { get; set; }
        [Reactive] public long residentialDistrictId { get; set; }
        [Reactive] public string residentialDistrict { get; set; }
        [Reactive] public long residentialRegionId { get; set; }
        [Reactive] public string residentialRegion { get; set; }
        [Reactive] public long idTypeId { get; set; }
        [Reactive] public string idType { get; set; }
        [Reactive] public string idNumber { get; set; }
        [Reactive] public DateTime idExpiry { get; set; }
        [Reactive] public string fatherName { get; set; }
        [Reactive] public string motherName { get; set; }
        [Reactive] public string hometownAddress { get; set; }
        [Reactive] public string hometown { get; set; }
        [Reactive] public long hometownDistrictId { get; set; }
        [Reactive] public string hometownDistrict { get; set; }
        [Reactive] public long hometownRegionId { get; set; }
        [Reactive] public string hometownRegion { get; set; }
        [Reactive] public bool isVisuallyImpaired { get; set; }
        [Reactive] public bool isDisabled { get; set; }
        [Reactive] public byte[] photo { get; set; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public bool canContinue { get; set; }
        public ReactiveCommand<Unit, Unit> search { get; }
        public ReactiveCommand<Unit, Unit> next { get; }

        public FindByVoterIdViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            search = ReactiveCommand.Create(() => runSearch(voterId), this.WhenAnyValue(x => x.voterId, id => !string.IsNullOrWhiteSpace(id)));
            next = ReactiveCommand.Create(runNext, this.WhenAnyValue(x => x.canContinue));
        }

        private void runSearch(string voterId)
        {
            canContinue = false;
            var result = Data.Registrations.findFromOldRegister(voterId);
            //todo: 
            if (result.IsOk)
            {
                errorMessage = "";
                var x = result.ResultValue;
                age = x.estimatedAge;
                dateOfBirth = x.dateOfBirth;
                fatherName = x.fatherName;
                motherName = x.motherName;
                hometown = x.homeTown;
                hometownAddress = x.homeTownAddress;
                idNumber = x.idNumber;
                idTypeId = x.idTypeId;
                isDisabled = x.isDisabled;
                isVisuallyImpaired = x.isVisuallyImpaired;
                otherNames = x.otherNames;
                phoneNumber = x.phoneNumber;
                residentialAddress = x.residentialAddress;
                residentialDistrictId = x.residentialDistrictId;
                residentialTown = x.residentialTown;
                sex = x.sex;
                surname = x.surname;
                hometownRegionId = getValueOrDefault(0L, x.homeTownRegionId);
                residentialRegionId = getValueOrDefault(0L, x.residentialRegionId);
                hometownDistrictId = getValueOrDefault(0L, x.homeTownDistrictId);
                residentialDistrictId = getValueOrDefault(0L, x.residentialDistrictId);
                hometownRegion = getValueOrDefault("", x.hometownRegion);
                residentialRegion = getValueOrDefault("", x.residentialRegion);
                hometownDistrict = getValueOrDefault("", x.hometownDistrict);
                residentialDistrict = getValueOrDefault("", x.residentialDistrict);
                canContinue = true;
            }
            else
            {
                errorMessage = result.ErrorValue.GetBaseException().Message;
                canContinue = false;
            }
        }

        private void runNext ()
        {
            var formData = new RegistrationFormData
            {
                otherNames = otherNames,
                surname = surname,
                dateOfBirth = dateOfBirth,
                age = age,
                sex = sex,
                phoneNumber = phoneNumber,
                residentialAddress = residentialAddress,
                residentialTown = residentialTown,
                residentialDistrictId = residentialDistrictId,
                residentialRegionId = residentialRegionId,
                residentialDistrict = residentialDistrict,
                residentialRegion = residentialRegion,
                idTypeId = idTypeId,
                idNumber = idNumber,
                //idExpiry = idExpiry,
                fatherName = fatherName,
                motherName = motherName,
                hometownAddress = hometownAddress,
                hometown = hometown,
                hometownDistrictId = hometownDistrictId,
                hometownRegionId = hometownRegionId,
                hometownDistrict = hometownDistrict,
                hometownRegion = hometownRegion,
                isVisuallyImpaired = isVisuallyImpaired,
                isDisabled = isDisabled
            };
            SharedViewModel.singleton.registrationData.form = formData;
            SharedViewModel.singleton.activeViewModel = new CaptureFingersViewModel();
        }
    }
}
