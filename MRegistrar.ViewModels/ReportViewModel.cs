﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class ReportViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Report Viewer";
        public IScreen HostScreen { get; }

        [Reactive] public ReportTypes reportType { get; set; }
        [Reactive] public string reportTitle { get; set; }
        [Reactive] public long generateRequests { get; set; }
        [Reactive] public bool busy { get; set; }
        public ReactiveCommand<Unit, Unit> generateReport { get; }

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;

        public ReportViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            generateReport = ReactiveCommand.Create(() => {
                if (busy) return;
                generateRequests++;
            });
            this.WhenAnyValue(x => x.reportType)
                .Select(getTitle)
                .BindTo(this, x => x.reportTitle);
        }
        private string getTitle(ReportTypes type)
        {
            switch (type)
            {
                case ReportTypes.AllRegistrations: return "All Registrations Report";
                case ReportTypes.StartOfDayStatistics: return "Start Of Day Statistics Report";
                case ReportTypes.EndOfDayStatistics: return "End Of Day Statistics Report";
                default: return "Unknown Report";
            }
        }

        public enum ReportTypes
        {
            AllRegistrations,
            StartOfDayStatistics, EndOfDayStatistics
        }
    }
}
