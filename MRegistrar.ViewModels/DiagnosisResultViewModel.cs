﻿namespace MRegistrar.ViewModels
{
    public class DiagnosisResultViewModel
    {
        public DiagnosisSource type { get; }
        public string name { get; }
        public string notes { get; set; }
        public string icon { get; }
        public DiagnosisStatus status { get; set; }
        public bool isEssential { get; }

        public DiagnosisResultViewModel(DiagnosisSource type, string notes, DiagnosisStatus status)
        {
            this.type = type;
            switch (type)
            {
                case DiagnosisSource.Battery:
                    name = "Battery";
                    icon = "BatteryHeart";
                    isEssential = false;
                    break;
                case DiagnosisSource.ReportPrinter:
                    name = "Printer";
                    icon = "Printer";
                    isEssential = false;
                    break;
                case DiagnosisSource.SDCard:
                    name = "SD Card";
                    icon = "SdCard";
                    isEssential = true;
                    break;
                case DiagnosisSource.FingerprintReader:
                    name = "Fingerprint Reader";
                    icon = "Fingerprint";
                    isEssential = Config.Configuration.Settings.RequireFingerPrintDeviceConnected;
                    break;
                case DiagnosisSource.Licenses:
                    name = "Licenses";
                    icon = "FileKey";
                    isEssential = false;
                    break;
                case DiagnosisSource.DataIntegrity:
                    name = "Data Integrity";
                    icon = "DatabaseTick";
                    isEssential = true;
                    break;
                case DiagnosisSource.Camera:
                    name = "Camera";
                    icon = "WebCamera";
                    isEssential = true;
                    break;
            }
            this.notes = notes;
            this.status = status;
        }
    }
    public enum DiagnosisSource
    {
        Battery, ReportPrinter, Camera,
        SDCard, FingerprintReader, Licenses, DataIntegrity
    }

    public enum DiagnosisStatus { Ok, Error, NotFound, NotChecked }
}
