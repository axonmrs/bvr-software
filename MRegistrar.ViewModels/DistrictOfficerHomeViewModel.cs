﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class DistrictOfficerHomeViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => null;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "District Officer";

        public IScreen HostScreen { get; }
        public ReactiveCommand<Unit, Unit> openRegistrationPeriod { get; }
        public ReactiveCommand<Unit, Unit> openRegistrationOfficers { get; }
        public ReactiveCommand<Unit, Unit> openDataEntryClerks { get; }
        public ReactiveCommand<Unit, Unit> openROView { get; }

        public DistrictOfficerHomeViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            openRegistrationPeriod = ReactiveCommand.Create(() =>
            {
                //todo: get the registration period from the appsettings table
                var rpVm = new RegistrationPeriodViewModel
                {
                    startDate = App.config.startDate,
                    endDate = App.config.endDate,
                    startTime = App.config.startTime.ToString(),
                    endTime = App.config.endTime.ToString()
                };
                SharedViewModel.singleton.activeViewModel = rpVm;
            });

            openROView = ReactiveCommand.Create(() =>
            {
                //todo: get the registration period from the appsettings table
                //we can initialize biometric client here
                var roVm = new RegisterRegistrationOfficerViewModel();
                SharedViewModel.singleton.activeViewModel = roVm;
            });

            

            openRegistrationOfficers = ReactiveCommand.Create(() => getSelectedRegistrationOfficers());

            openDataEntryClerks = ReactiveCommand.Create(() => getSelectedDataEntryClerks());
        }

        private void getSelectedRegistrationOfficers()
        {
            var vm = new RegistrationOfficersViewModel();
            SharedViewModel.singleton.activeViewModel = vm;
        }

        private void getSelectedDataEntryClerks()
        {
            var vm = new DataEntryClerksViewModel();
            SharedViewModel.singleton.activeViewModel = vm;
        }
    }
}
