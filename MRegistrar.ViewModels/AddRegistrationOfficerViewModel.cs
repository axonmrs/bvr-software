﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class AddRegistrationOfficerViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Add Registration Officer";
        public IScreen HostScreen { get; }

        [Reactive] public long registrationOfficerId { get; set; }
        [Reactive] public List<RegistrationUserViewModel> officers { get; set; }
        public ReactiveCommand<Unit, Unit> add { get; }
        [Reactive] public string message { get; set; }

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => false;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;

        public AddRegistrationOfficerViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            add = ReactiveCommand.Create(() => addOfficer(registrationOfficerId));
        }
        private void addOfficer(long id)
        {
            var result = Data.Users.toggleSelectUser(id, true);
            if (result.IsOk)
            {
                var vm = new RegistrationOfficersViewModel();
                SharedViewModel.singleton.activeViewModel = vm;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
    }
}
