﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class LoginViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Login";
        public IScreen HostScreen { get; }

        [Reactive] public string username { get; set; }
        [Reactive] public string password { get; set; }
        /// <summary>
        /// Holds any error messages
        /// </summary>
        [Reactive] public string errorMessage { get; set; }
        public bool busy { [ObservableAsProperty]get; }
        public ReactiveCommand<Unit, bool> attemptLogin { get; }
        public ReactiveCommand<Unit, Unit> clearError { get; }

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => false;
        Func<Task> IViewSettings.onCancel => null;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;

        public LoginViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            //setup login button
            attemptLogin = ReactiveCommand.Create(
                () =>
                {
                    var result = processLogin(username, password);
                    return result;
                },
                this.WhenAnyValue(x => x.username, x => x.password,
                    (username, password) =>
                        !string.IsNullOrWhiteSpace(username) &&
                        !string.IsNullOrWhiteSpace(password)));
            attemptLogin.IsExecuting.ToPropertyEx(this, x => x.busy);
            //setup clear button on notification
            clearError = ReactiveCommand.Create(() =>
            {
                errorMessage = "";
            });
        }

        private bool processLogin(string username, string password)
        {
            var result = App.login(username, password);
            if (result.IsOk)
            {
                errorMessage = "";
                SharedViewModel.singleton.user = result.ResultValue;
                var user = result.ResultValue;
                ActivityLogs.save(new ActivityLogs.ActivityLog(SharedViewModel.Activity.Login.ToString(), $"{user.name} logged in as {user.type}", user.username));
                return true;
            }
            else
            {
                errorMessage = result.ErrorValue.GetBaseException().Message;
                return false;
            }
        }
    }
}
