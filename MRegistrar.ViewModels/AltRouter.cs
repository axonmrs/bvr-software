﻿using NLog;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRegistrar.ViewModels
{
    public class AltRouter
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        private List<IRoutableViewModel> viewModels { get; } = new List<IRoutableViewModel>();

        public IRoutableViewModel Push(IRoutableViewModel viewModel, bool clear = false)
        {
            logger.Trace($"Pushing ViewModel: {viewModel.GetType().Name}");
            if (clear) clearAll();
            if (viewModel == null)
            {
                logger.Trace($"ViewModel is null so skipping");
                return viewModel;
            }
            if (viewModels.Any() && isTopMost(viewModel))
            {
                logger.Trace($"ViewModel is already active so skipping");
                return viewModel;
            }
            viewModels.Insert(0, viewModel);
            return viewModel;
        }

        public IRoutableViewModel Back()
        {
            logger.Trace("Going back");
            if (!viewModels.Any()) return null;
            viewModels.RemoveAt(0);
            var viewModel = viewModels.FirstOrDefault();
            logger.Trace($"Current view is now {viewModel?.GetType().Name}");
            return viewModel;
        }

        public IRoutableViewModel currentViewModel => viewModels.FirstOrDefault();
        private void clearAll()
        {
            logger.Trace($"Clearing all existing view models: {viewModels.Count}");
            //todo: for the list of view models, try cleaning up all the views
            viewModels.Clear();
        }

        private bool isTopMost(IRoutableViewModel viewModel)
        {
            if (!viewModels.Any()) return false;
            if (viewModel == null) return false;
            return viewModels.First().GetType().FullName == viewModel.GetType().FullName;
        }
    }
}
