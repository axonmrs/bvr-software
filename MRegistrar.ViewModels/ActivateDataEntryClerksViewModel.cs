﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
//using System.Printing;

namespace MRegistrar.ViewModels
{
    public class ActivateDataEntryClerksViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Manage Data Entry Clerks";

        //public ReactiveCommand<Unit, Unit> addRO { get; }
        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public List<RegistrationUserViewModel> officers { get; set; }

        public ReactiveCommand<Unit, Unit> addDec { get; }
        public ActivateDataEntryClerksViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            loadData();
            addDec = ReactiveCommand.Create(() =>
            {
                SharedViewModel.singleton.activeViewModel = new CreateUserFormViewModel(role: "DataEntryClerk");
            });
            //.WhenAnyValue(x => x., x => x.password,
            //       (username, password) =>
            //           !string.IsNullOrWhiteSpace(username) &&
            //           !string.IsNullOrWhiteSpace(password)));
        }

        private void loadData()
        {
            var result = Data.Users.findByTypeAndSelected("DataEntryClerk", true);
            if (result.Any())
            {
                var cnt = 1;
                officers = new List<RegistrationUserViewModel>();
                foreach (var r in result)
                {
                    officers.Add(new RegistrationUserViewModel
                    {
                        num = cnt,
                        id = r.id,
                        name = r.name,
                        username = r.username,
                        isActive = r.isActive
                    });
                    cnt++;
                }
            }
            else
            {
                //errorMessage = "Could not find any selected registration officers";
            }
        }
    }
}
