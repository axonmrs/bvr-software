﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
//using System.Printing;

namespace MRegistrar.ViewModels
{
    public class ConfirmShutdownViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "Shutdown Check List";

        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public string error { get; set; }
        [Reactive] public bool noPendingBackups { get; set; }
        [Reactive] public bool noPendingTransmissions { get; set; }
        [Reactive] public bool printedStartOfDayReport { get; set; }
        [Reactive] public bool printedEndOfDayReport { get; set; }
        public ReactiveCommand<Unit, Unit> clearError { get; }
        public ReactiveCommand<Unit, Unit> shutDown { get; }

        public ConfirmShutdownViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            shutDown = ReactiveCommand.Create(() => processSystemShutdown());
            clearError = ReactiveCommand.Create(() =>
            {
                errorMessage = "";
            });
        }

        private void processSystemShutdown()
        {
            if(string.IsNullOrEmpty(error))
            {
                //log activity
                Data.ActivityLogs.save(new Data.ActivityLogs.ActivityLog(SharedViewModel.Activity.Shutdown.ToString(), $"{SharedViewModel.singleton.user.name ?? "System"} shut down the system", SharedViewModel.singleton.user.username ?? "System"));

                //clear user
                SharedViewModel.singleton.user = new Data.Model.User("", "", "", "", true, true);

                //shut down now
                var psi = new ProcessStartInfo("shutdown", "/s /t 0");
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process.Start(psi);
            }
        }
    }
}
