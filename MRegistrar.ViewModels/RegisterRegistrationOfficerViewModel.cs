﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class RegisterRegistrationOfficerViewModel : ReactiveObject, IRoutableViewModel,  IViewSettings
    {

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Create Registration Officer";

        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        public ReactiveCommand<Unit, Unit> addRO { get; }
        [Reactive] public List<RegistrationUserViewModel> officers { get; set; }

        public RegisterRegistrationOfficerViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            loadData();
            addRO = ReactiveCommand.Create(() =>
            {
                //todo: get the registration period from the appsettings table
                //var vm = new AddDataEntryClerkViewModel();
                //vm.officers = loadNotSelectedData();
                //SharedViewModel.singleton.activeViewModel = vm;

                SharedViewModel.singleton.activeViewModel = new CreateUserFormViewModel(role:"RegistrationOfficer");
            });
        }

        private void loadData()
        {
            var result = Data.Users.findByTypeAndSelected("RegistrationOfficer", true);
            if (result.Any())
            {
                var cnt = 1;
                officers = new List<RegistrationUserViewModel>();
                foreach (var r in result)
                {
                    officers.Add(new RegistrationUserViewModel
                    {
                        num = cnt,
                        id = r.id,
                        name = r.name,
                        username = r.username,
                        isActive = r.isActive
                    });
                    cnt++;
                }
            }
            else
            {
                //errorMessage = "Could not find any selected registration officers";
            }
        }

        private new List<RegistrationUserViewModel> loadNotSelectedData()
        {
            var recs = new List<RegistrationUserViewModel>();
            var result = Data.Users.findByTypeAndSelected("Operator", false);
            if (result.Any())
            {
                var cnt = 1;
                foreach (var r in result)
                {
                    recs.Add(new RegistrationUserViewModel
                    {
                        num = cnt,
                        id = r.id,
                        name = r.name,
                        username = r.username,
                        isActive = r.isActive
                    });
                    cnt++;
                }
            }
            else
            {
                //errorMessage = "Could not find any selected registration officers";
            }
            return recs;
        }
    }
}
