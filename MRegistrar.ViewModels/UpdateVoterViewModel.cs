﻿using MRegistrar.ViewModels.Helpers;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using ReactiveUI.Validation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class UpdateVoterViewModel : ReactiveObject, IRoutableViewModel, IViewSettings, IValidatableViewModel
    {
        public string UrlPathSegment => $"Update Voter Details: {voterId}";
        public IScreen HostScreen { get; }
        public bool showTray => true;
        public bool showStatusSummaries => true;
        public bool enableLogOut => true;
        public Func<Task> onCancel => () => Task.CompletedTask;
        public Func<Task> onBack => null;
        public bool enableShutDown => true;
        public bool enableTransfer => true;
        public bool enableBackup => true;

        public string voterId { get; set; }

        [Reactive] public string otherNames { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public DateTime dateOfBirth { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public bool isEstimatedAge { get; set; }
        [Reactive] public string sex { get; set; }
        [Reactive] public string phoneNumber { get; set; }
        [Reactive] public string residentialAddress { get; set; }
        [Reactive] public string residentialTown { get; set; }
        [Reactive] public long? residentialDistrictId { get; set; }
        [Reactive] public long? residentialRegionId { get; set; }
        [Reactive] public string idNumber { get; set; }
        [Reactive] public string fatherName { get; set; }
        [Reactive] public string motherName { get; set; }
        [Reactive] public string hometownAddress { get; set; }
        [Reactive] public string hometown { get; set; }
        [Reactive] public long? hometownDistrictId { get; set; }
        [Reactive] public long? hometownRegionId { get; set; }
        [Reactive] public bool isVisuallyImpaired { get; set; }
        [Reactive] public bool isDisabled { get; set; }
        [Reactive] public string formTitle { get; set; }
        [Reactive] public bool hearingImpaired { get; set; }
        [Reactive] public bool leper { get; set; }
        [Reactive] public bool missingFingersL { get; set; }
        [Reactive] public bool missingFingersR { get; set; }
        [Reactive] public bool amputatedHandsL { get; set; }
        [Reactive] public bool amputatedHandsR { get; set; }
        [Reactive] public bool busy { get; set; }
        [Reactive] public byte[] photo { get; set; }

        private string idType { get; set; }
        private string hometownDistrict { get; set; }
        private string residentialDistrict { get; set; }
        private string hometownRegion { get; set; }
        private string residentialRegion { get; set; }
        private DateTime dateRegistered { get; set; }

        ///workaround the listbox not binding the selectedItems property as expected
        public ReactiveCommand<Unit, Unit> gotoNext { get; }

        public ValidationContext ValidationContext { get; } = new ValidationContext();
        public ValidationHelper ageRule { get; }
        public ValidationHelper phoneNumberRule { get; }
        public ValidationHelper residentialRegionRule { get; }
        public ValidationHelper residentialDistrictRule { get; }
        public ValidationHelper idTypeRule { get; }
        public ValidationHelper hometownRegionRule { get; }
        public ValidationHelper hometownDistrictRule { get; }


        public UpdateVoterViewModel(string voterId)
        {
            this.voterId = voterId;

            //activeForm = Forms.BioForm;
            this.WhenAnyValue(x => x.dateOfBirth)
                .Select(x => computeAge(x))
                .Where(x => x != age)
                .BindTo(this, x => x.age);
            this.WhenAnyValue(x => x.age)
                .Select(x => new DateTime(DateTime.Now.Year - x, 1, 1))
                .Where(x => x != dateOfBirth)
                .BindTo(this, x => x.dateOfBirth);
            age = 18;
            Task.Run(() => populateExistingData(voterId));
            //set the region ids
            this.WhenAnyValue(x => x.hometownDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == hometownDistrictId))
                .Select(x => x?.parentId)
                .BindTo(this, x => x.hometownRegionId);
            this.WhenAnyValue(x => x.residentialDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == residentialDistrictId))
                .Select(x => x?.parentId)
                .BindTo(this, x => x.residentialRegionId);
            //set the district names
            this.WhenAnyValue(x => x.hometownDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == hometownDistrictId))
                .Select(x => x?.name)
                .BindTo(this, x => x.hometownDistrict);
            this.WhenAnyValue(x => x.residentialDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == residentialDistrictId))
                .Select(x => x?.name)
                .BindTo(this, x => x.residentialDistrict);
            //set the region names
            this.WhenAnyValue(x => x.hometownRegionId)
                .SelectMany(x => SharedViewModel.singleton.regions)
                .Select(xs => xs.FirstOrDefault(x => x.id == hometownRegionId))
                .Select(x => x?.name)
                .BindTo(this, x => x.hometownRegion);
            this.WhenAnyValue(x => x.residentialRegionId)
                .SelectMany(x => SharedViewModel.singleton.regions)
                .Select(xs => xs.FirstOrDefault(x => x.id == residentialRegionId))
                .Select(x => x?.name)
                .BindTo(this, x => x.residentialRegion);

            // validations
            this.ValidationRule(vm => vm.surname, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.otherNames, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.residentialAddress, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.residentialTown, x => !string.IsNullOrWhiteSpace(x), "Required");
            //this.ValidationRule(vm => vm.idNumber, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.hometownAddress, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.hometown, x => !string.IsNullOrWhiteSpace(x), "Required");
            ageRule = this.ValidationRule(vm => vm.age, age => age >= 18, "Must be 18 years and above");

            phoneNumberRule = this.ValidationRule(vm => vm.phoneNumber, isPhoneNumber, "Invalid Phone Number.");
            residentialRegionRule = this.ValidationRule(vm => vm.residentialRegionId, x => x != null, "Required");
            residentialDistrictRule = this.ValidationRule(vm => vm.residentialDistrictId, x => x != null, "Required");
            //idTypeRule = this.ValidationRule(vm => vm.idTypeId, x => x != null, "Required");
            hometownRegionRule = this.ValidationRule(vm => vm.hometownRegionId, x => x != null, "Required");
            hometownDistrictRule = this.ValidationRule(vm => vm.hometownDistrictId, x => x != null, "Required");

            var formIsValid = this.IsValid();

            gotoNext = ReactiveCommand.CreateFromTask(async () =>
            {
                busy = true;
                //todo: ensure something has changed
                //todo: get supervisor authorization
                try
                {
                    var data = new Data.Registrations.UpdateRegistrationData(surname, otherNames, dateOfBirth, age, sex, phoneNumber, residentialAddress, residentialTown, residentialDistrictId ?? 0, fatherName, motherName, hometownAddress, hometown, hometownDistrictId ?? 0, isVisuallyImpaired, isDisabled, isEstimatedAge, hearingImpaired, leper, missingFingersL, missingFingersR, amputatedHandsL, amputatedHandsR, voterId);
                    var result = Data.Registrations.update(data);
                    if (result.IsError)
                    {
                        //todo: log this
                        //todo: show this error
                    }
                    else
                    {
                        void onPrintComplete()
                        {
                            SharedViewModel.singleton.activeViewModel = new OperatorHomeViewModel(); //todo: get the correct home based on the logged in user
                        }
                        var x = this;
                        SharedViewModel.singleton.activeViewModel = new PrintCardViewModel { age = x.age, dateOfBirth = x.dateOfBirth, fatherName = x.fatherName, motherName = x.motherName, hometown = x.hometown, hometownAddress = x.hometownAddress, hometownDistrict = x.hometownDistrict, hometownRegion = x.hometownRegion, idNumber = x.idNumber, idType = x.idType, isDisabled = x.isDisabled, isVisuallyImpaired = x.isVisuallyImpaired, otherNames = x.otherNames, phoneNumber = x.phoneNumber, residentialAddress = x.residentialAddress, residentialDistrict = x.residentialDistrict, residentialTown = x.residentialTown, residentialRegion = x.residentialRegion, sex = x.sex, surname = x.surname, voterId = x.voterId, photo = x.photo, pollingStationCode = Data.App.config.code, name = $"{x.otherNames} {x.surname}", dateOfRegistration = x.dateRegistered, cardPrintCount = 0, canGoBack = false, onPrintComplete = onPrintComplete };
                    }
                }
                catch(Exception e)
                {
                    //todo: log this 
                }                
            }, formIsValid);
        }

        private int computeAge(DateTime dob)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - dob.Year;
            if (today < dob.AddYears(age)) age--;
            return age;
        }

        private void populateExistingData(string voterId) //todo: update
        {
            try
            {
                var voter = Data.Registrations.find(voterId);
                if (voter.IsError)
                {
                    //todo: set the error

                }
                else
                {
                    var existing = voter.ResultValue;
                    var isAgeEstimated = false;
                    try { isAgeEstimated = existing.is_estimated_age.Value; } catch { }
                    otherNames = existing.otherNames;
                    surname = existing.surname;
                    dateOfBirth = existing.dateOfBirth;
                    age = existing.estimatedAge;
                    isEstimatedAge = isAgeEstimated;
                    sex = existing.sex;
                    phoneNumber = existing.phoneNumber;
                    residentialAddress = existing.residentialAddress;
                    residentialTown = existing.residentialTown;
                    residentialDistrictId = existing.residentialDistrictId;
                    residentialRegionId = existing.residentialRegionId;
                    residentialDistrict = existing.residentialDistrict;
                    residentialRegion = existing.residentialRegion;
                    //idTypeId = existing.idTypeId;
                    idType = Data.Utils.getValueOrDefault("", existing.idType);
                    idNumber = existing.idNumber;
                    fatherName = existing.fatherName;
                    motherName = existing.motherName;
                    hometownAddress = existing.homeTownAddress;
                    hometown = existing.homeTown;
                    hometownDistrictId = existing.homeTownDistrictId;
                    hometownRegionId = existing.homeTownRegionId;
                    hometownDistrict = existing.homeTownDistrict;
                    hometownRegion = existing.hometownRegion;
                    isVisuallyImpaired = existing.isVisuallyImpaired;
                    isDisabled = existing.isDisabled;
                    hearingImpaired = existing.hearingImpaired;
                    leper = existing.leper;
                    missingFingersL = existing.missingFingersL;
                    missingFingersR = existing.missingFingersR;
                    amputatedHandsL = existing.amputatedHandsL;
                    amputatedHandsR = existing.amputatedHandsR;
                    photo = existing.photo;
                }
            }
            catch
            {

            }
        }

        private static bool isPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return true; //accept empty
            if (phoneNumber.Any(x => !char.IsDigit(x))) return false;
            if (phoneNumber.Length == 13 && phoneNumber.StartsWith("+233")) return true;
            if (phoneNumber.Length == 12 && phoneNumber.StartsWith("233")) return true;
            if (phoneNumber.Length == 10 && phoneNumber.StartsWith("0")) return true;
            return false;
        }
    }
}
