﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
//using System.Printing;

namespace MRegistrar.ViewModels
{
    public class DataEntryClerksViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Data Entry Clerks";

        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        public ReactiveCommand<Unit, Unit> add { get; }
        [Reactive] public List<RegistrationUserViewModel> officers { get; set; }

        public DataEntryClerksViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            loadData();
            add = ReactiveCommand.Create(() =>
            {
                //todo: get the registration period from the appsettings table
                var vm = new AddDataEntryClerkViewModel();
                vm.officers = loadNotSelectedData();
                SharedViewModel.singleton.activeViewModel = vm;
            });
        }

        private void loadData()
        {
            var result = Data.Users.findByTypeAndSelected("Operator", true);
            if (result.Any())
            {
                var cnt = 1;
                officers = new List<RegistrationUserViewModel>();
                foreach (var r in result)
                {
                    officers.Add(new RegistrationUserViewModel
                    {
                        num = cnt,
                        id = r.id,
                        name = r.name,
                        username = r.username,
                        isActive = r.isActive
                    });
                    cnt++;
                }
            }
            else
            {
                //errorMessage = "Could not find any selected registration officers";
            }
        }

        private new List<RegistrationUserViewModel> loadNotSelectedData()
        {
            var recs = new List<RegistrationUserViewModel>();
            var result = Data.Users.findByTypeAndSelected("Operator", false);
            if (result.Any())
            {
                var cnt = 1;
                foreach (var r in result)
                {
                    recs.Add(new RegistrationUserViewModel
                    {
                        num = cnt,
                        id = r.id,
                        name = r.name,
                        username = r.username,
                        isActive = r.isActive
                    });
                    cnt++;
                }
            }
            else
            {
                //errorMessage = "Could not find any selected registration officers";
            }
            return recs;
        }
    }
}
