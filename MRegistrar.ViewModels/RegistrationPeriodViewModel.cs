﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
  public class RegistrationPeriodViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
  {
    public string UrlPathSegment => "Registration Settings";
    public IScreen HostScreen { get; }

    [Reactive] public string startTime { get; set; }
    [Reactive] public string endTime { get; set; }
    [Reactive] public DateTime startDate { get; set; }
    [Reactive] public DateTime endDate { get; set; }
    public ReactiveCommand<Unit, Unit> update { get; }
    [Reactive] public string message { get; set; }
    [Reactive] public long? pollingStationId { get; set; }
    [Reactive] public DropDownItemViewModel pollingStation { get; set; }

    bool IViewSettings.showTray => true;
    bool IViewSettings.showStatusSummaries => true;
    Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
    Func<Task> IViewSettings.onBack => null;
    bool IViewSettings.enableLogOut => true;
    bool IViewSettings.enableShutDown => true;
    bool IViewSettings.enableTransfer => false;
    bool IViewSettings.enableBackup => false;

    public RegistrationPeriodViewModel(IScreen screen = null)
    {
      HostScreen = screen ?? Locator.Current.GetService<IScreen>();
      update = ReactiveCommand.Create(() => updatePeriod(startDate, endDate, startTime, endTime));

      this.WhenAnyValue(x => x.pollingStationId)
          .SelectMany(x => SharedViewModel.singleton.pollingStations)
          .Select(xs => xs.FirstOrDefault(x => x.id == pollingStationId))
          .BindTo(this, x => x.pollingStation);
    }

    private void updatePeriod(DateTime startDate, DateTime endDate, string startTime, string endTime)
    {
      startTime = string.IsNullOrWhiteSpace(startTime) ? null : startTime.Trim();
      endTime = string.IsNullOrWhiteSpace(endTime) ? null : endTime.Trim();
      var obj = new List<Tuple<string, string>>
            {
                new Tuple<string, string>("start_date", startDate.Date.ToString()),
                new Tuple<string, string>("end_date", endDate.Date.ToString()),
                new Tuple<string, string>("start_time", startTime),
                new Tuple<string, string>("end_time", endTime),
                new Tuple<string, string>("ps_code", pollingStation.code),
                new Tuple<string, string>("ps_name", pollingStation.name)
            };

      var result = App.updateSettings(obj);
      if (result.IsOk)
      {
        message = "";
        App.init();
        SharedViewModel.singleton.pollingStationName = App.config.name;
        SharedViewModel.singleton.activeViewModel = new RegistrationOfficersViewModel();
      }
      else
      {
        message = "Could not update the registration period";
      }
    }
  }
}
