﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive;

namespace MRegistrar.ViewModels
{
    public class RegistrationUserViewModel : ReactiveObject
    {
        [Reactive] public long id { get; set; }
        [Reactive] public int num { get; set; }
        [Reactive] public string name { get; set; }
        [Reactive] public string username { get; set; }
        [Reactive] public bool isActive { get; set; }
        [Reactive] public bool selected { get; set; }
        public ReactiveCommand<Unit, Unit> changeStatus { get; }
        public ReactiveCommand<Unit, Unit> remove { get; }

        public RegistrationUserViewModel()
        {
            changeStatus = ReactiveCommand.Create(() => changeOfficerStatus(id));

            remove = ReactiveCommand.Create(() => removeOfficer(id));
        }
        private void changeOfficerStatus(long id)
        {
            bool state = !isActive;
            var res = Data.Users.toggleActivateUser(id, state);
            if (res.IsOk)
            {
                isActive = state;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
        
        private void removeOfficer(long id)
        {
            var result = Data.Users.toggleSelectUser(id, false);
            if (result.IsOk)
            {
                selected = false;
                var res = Data.Users.toggleActivateUser(id, false);
                if (res.IsOk)
                {
                    isActive = false;
                }
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
    }
}
