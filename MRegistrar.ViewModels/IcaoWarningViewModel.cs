﻿using Neurotec.Biometrics;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MRegistrar.ViewModels
{
    public class IcaoWarningViewModel: ReactiveObject
    {
        public NIcaoWarnings warning { get; }
        public string name { get; }
        public bool isEssential { get; }
        [Reactive] public WarningStatus status { get; set; }
        [Reactive] public string display { get; set; }

        public IcaoWarningViewModel(NIcaoWarnings warning, WarningStatus status)
        {
            this.warning = warning;
            this.status = status;
            name = NameFromEnum(warning);
            display = name;
        }

        private static string NameFromEnum(NIcaoWarnings x)
        {
            switch (x)
            {
                case NIcaoWarnings.PitchDown:
                case NIcaoWarnings.PitchUp: return "Pitch";
                case NIcaoWarnings.YawRight:
                case NIcaoWarnings.YawLeft: return "Yaw";
                case NIcaoWarnings.RollRight:
                case NIcaoWarnings.RollLeft: return "Roll";
            }
            var name = x.ToString();
            return Regex.Replace(name, "(.*?)([A-Z])", "$1 $2").Trim();
        }

        public void update(NIcaoWarnings warnings, NLAttributes attributes)
        {
            //checks
            if (attributes == null) return;
            if ((warnings & warning) != warning)
            {
                status = WarningStatus.Ok;
                return;
            }
            void setStatus (byte confidence)
            {
                status = confidence <= 100 ? WarningStatus.Error : WarningStatus.NotChecked;
            }
            void setStatusFromFlags(params NIcaoWarnings[] flags)
            {
                status = flags.Any(x => (x & warnings) == x) ? WarningStatus.Error : WarningStatus.Ok;
            }
            void setDisplay(byte value)
            {
                var v = value <= 100 ? value.ToString() : "N/A";
                display = $"{name}: {v}";
            }
            switch (warning)
            {
                case NIcaoWarnings.FaceNotDetected:
                    status = WarningStatus.Ok;
                    break;
                case NIcaoWarnings.RollLeft:
                    setStatusFromFlags(NIcaoWarnings.RollLeft, NIcaoWarnings.RollRight);
                    break;
                case NIcaoWarnings.RollRight:
                    setStatusFromFlags(NIcaoWarnings.RollLeft, NIcaoWarnings.RollRight);
                    break;
                case NIcaoWarnings.YawLeft:
                    setStatusFromFlags(NIcaoWarnings.YawLeft, NIcaoWarnings.YawRight);
                    break;
                case NIcaoWarnings.YawRight:
                    setStatusFromFlags(NIcaoWarnings.YawLeft, NIcaoWarnings.YawRight);
                    break;
                case NIcaoWarnings.PitchUp:
                    setStatusFromFlags(NIcaoWarnings.PitchUp, NIcaoWarnings.PitchDown);
                    break;
                case NIcaoWarnings.PitchDown:
                    setStatusFromFlags(NIcaoWarnings.PitchUp, NIcaoWarnings.PitchDown);
                    break;
                case NIcaoWarnings.TooNear:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.TooFar:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.TooNorth:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.TooSouth:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.TooEast:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.TooWest:
                    setStatusFromFlags(warning);
                    break;
                case NIcaoWarnings.Sharpness:
                    setStatusFromFlags(warning);
                    setDisplay(attributes.Sharpness);
                    break;
                case NIcaoWarnings.BackgroundUniformity:
                    setStatusFromFlags(warning);
                    setDisplay(attributes.BackgroundUniformity);
                    break;
                case NIcaoWarnings.GrayscaleDensity:
                    setStatusFromFlags(warning);
                    setDisplay(attributes.GrayscaleDensity);
                    break;
                case NIcaoWarnings.Saturation:
                    setStatusFromFlags(warning);
                    setDisplay(attributes.Saturation);
                    break;
                case NIcaoWarnings.Expression:
                    setStatus(attributes.ExpressionConfidence);
                    break;
                case NIcaoWarnings.DarkGlasses:
                    setStatus(attributes.DarkGlassesConfidence);
                    break;
                case NIcaoWarnings.Blink:
                    setStatus(attributes.BlinkConfidence);
                    break;
                case NIcaoWarnings.MouthOpen:
                    setStatus(attributes.MouthOpenConfidence);
                    break;
                case NIcaoWarnings.LookingAway:
                    setStatus(attributes.LookingAwayConfidence);
                    break;
                case NIcaoWarnings.RedEye:
                    setStatus(attributes.RedEyeConfidence);
                    break;
                case NIcaoWarnings.FaceDarkness:
                    setStatus(attributes.FaceDarknessConfidence);
                    break;
                case NIcaoWarnings.UnnaturalSkinTone:
                    setStatus(attributes.UnnaturalSkinToneConfidence);
                    break;
                case NIcaoWarnings.WashedOut:
                    setStatus(attributes.WashedOutConfidence);
                    break;
                case NIcaoWarnings.Pixelation:
                    setStatus(attributes.PixelationConfidence);
                    break;
                case NIcaoWarnings.SkinReflection:
                    setStatus(attributes.SkinReflectionConfidence);
                    break;
                case NIcaoWarnings.GlassesReflection:
                    setStatus(attributes.GlassesReflectionConfidence);
                    break;
                default:
                    break;
            }

        }
    }

    public enum WarningStatus { Ok, Error, NotChecked }
}
