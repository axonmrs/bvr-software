﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class PresidingOfficerHomeViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => null;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "Registration Officer";

        public IScreen HostScreen { get; }
        public ReactiveCommand<Unit, Unit> openDataEntryClerks { get; }
        public ReactiveCommand<Unit, Unit> openReports { get; }
        public ReactiveCommand<Unit, Unit> openRegistrationSettings { get; }
        public ReactiveCommand<Unit, Unit> openRegistrationOfficers { get; }

        public PresidingOfficerHomeViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            openRegistrationSettings = ReactiveCommand.Create(() =>
            {
                //todo: get the registration period from the appsettings table
                var rpVm = new RegistrationPeriodViewModel
                {
                    startDate = App.config.startDate,
                    endDate = App.config.endDate,
                    startTime = App.config.startTime.ToString(),
                    endTime = App.config.endTime.ToString()
                };
                SharedViewModel.singleton.activeViewModel = rpVm;
            });
            openDataEntryClerks = ReactiveCommand.Create(() =>
            {
                //SharedViewModel.singleton.activeViewModel = new DataEntryClerksViewModel();
                SharedViewModel.singleton.activeViewModel = new ActivateDataEntryClerksViewModel();

            });
            openRegistrationOfficers = ReactiveCommand.Create(() =>
            {
                SharedViewModel.singleton.activeViewModel = new RegistrationOfficersViewModel();
            });
            openReports = ReactiveCommand.Create(() =>
            {
                SharedViewModel.singleton.activeViewModel = new ReportListViewModel();
            });
        }
    }
}
