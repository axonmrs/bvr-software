﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using System.Windows.Controls;
//using System.Printing;
using System.Windows.Documents;

namespace MRegistrar.ViewModels
{
    public class FindRegistrationViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Find Voter";

        public IScreen HostScreen { get; }

        [Reactive] public string voterId { get; set; }
        [Reactive] public string voterName { get; set; }
        [Reactive] public string errorMessage { get; set; }
        public ReactiveCommand<Unit, Unit> search { get; }
        [Reactive] public List<RegistrationEntryViewModel> registrations { get; set; }

        public FindRegistrationViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            var canSearch = this.WhenAnyValue(x => x.voterId, x => x.voterName, (voterId, voterName) =>
                !string.IsNullOrWhiteSpace(voterId) || !string.IsNullOrWhiteSpace(voterName));
            search = ReactiveCommand.Create(() => runSearch(voterId, voterName), canSearch);
        }
        private void runSearch(string voterId, string voterName)
        {
            voterId = string.IsNullOrWhiteSpace(voterId) ? null : voterId.Trim();
            voterName = string.IsNullOrWhiteSpace(voterName) ? null : voterName.Trim();
            var result = Data.Registrations.findVoters(voterId, voterName);
            if (result.Any())
            {
                var cnt = 1;
                registrations = new List<RegistrationEntryViewModel>();
                errorMessage = "";
                //set the list datasource and show the list view
                foreach (var r in result)
                {
                    registrations.Add(new RegistrationEntryViewModel
                    {
                        num = cnt,
                        voterId = r.voterId,
                        surname = r.surname,
                        otherNames = r.otherNames,
                        age = r.estimatedAge,
                        photo = r.photo
                    }) ;
                    cnt++;
                }
                //registrations = result.Select(x => cnt++,  new RegistrationEntryViewModel
                //{
                //    voterId = x.voterId,
                //    surname = x.surname,
                //    otherNames = x.otherNames,
                //    age = x.estimatedAge,
                //}).ToList();
            }
            else
            {
                errorMessage = "Could not find any registered voters";
            }
        }

        

    }
}
