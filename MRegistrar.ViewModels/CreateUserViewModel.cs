﻿using MRegistrar.Data;
using MRegistrar.Services;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using MRegistrar.ViewModels.Helpers;

namespace MRegistrar.ViewModels
{
  public class CreateUserFormViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
  {

    public string UrlPathSegment => "Create User";
    public IScreen HostScreen { get; }

    [Reactive] public long registrationOfficerId { get; set; }
    [Reactive] public string name { get; set; }

    [Reactive] public string username { get; set; }
    [Reactive] public List<RegistrationUserViewModel> officers { get; set; }
    public ReactiveCommand<Unit, Unit> add { get; }
    public ReactiveCommand<Unit, Unit> roNext { get; }
    [Reactive] public string message { get; set; }

    bool IViewSettings.showTray => true;
    bool IViewSettings.showStatusSummaries => false;
    Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
    Func<Task> IViewSettings.onBack => null;
    bool IViewSettings.enableLogOut => true;
    bool IViewSettings.enableShutDown => true;
    bool IViewSettings.enableTransfer => true;

    bool IViewSettings.enableBackup => false;

    public CreateUserFormViewModel(IScreen screen = null, string role = "")
    {
      HostScreen = screen ?? Locator.Current.GetService<IScreen>();
      roNext = ReactiveCommand.CreateFromTask(async () =>
      {
        var vm = new StaffData
        {
          Name = name,
          UserName = username,
          Role = role
        };

        var result = Users.findByUsername(username);
        if (result.IsOk)
        {
          await DialogService.alert($"User with username {username} already exist. Please choose another one");
          return;
        }

        SharedViewModel.singleton.activeViewModel = new CaptureUserFingersViewModel();
        SharedViewModel.singleton.staffData = vm;
        SharedViewModel.singleton.userRole = vm.Role;
      });
    }
    private void addOfficer(long id)
    {
      var result = Data.Users.toggleSelectUser(id, true);
      if (result.IsOk)
      {
        var vm = new RegistrationOfficersViewModel();
        SharedViewModel.singleton.activeViewModel = vm;
      }
      else
      {
        //errorMessage = result.ErrorValue.GetBaseException().Message;
      }
    }
  }
}
