﻿using DynamicData;
using MRegistrar.Data;
using MRegistrar.Services;
using MRegistrar.ViewModels.Helpers;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class SharedViewModel : ReactiveObject
    {
        [Reactive] public string activePageTitle { get; set; }
        [Reactive] public Model.User user { get; set; }
        /// <summary>
        /// Loaded at startup.
        /// Increased after each successful registration
        /// </summary>
        [Reactive] public int registrationsSoFar { get; set; }
        /// <summary>
        /// Loaded at startup.
        /// Increased after each successful registration
        /// </summary>
        [Reactive] public int registrationsToday { get; set; }
        /// <summary>
        /// Loaded at startup.
        /// Increased after every successful registration
        /// Set to 0 after each backup
        /// </summary>
        [Reactive] public int registrationsSinceBackup { get; set; }
        [Reactive] public bool activated { get; set; }
        [Reactive] public bool clientsReady { get; set; }
        [Reactive] public string printerName { get; set; }
        [Reactive] public bool printerOnline { get; set; }
        [Reactive] public IRoutableViewModel activeViewModel { get; set; }
        public RegistrationData registrationData { get; private set; }
        
        public StaffData staffData { get; set; }

        public string userRole { get; set; }
        [Reactive] public string voterId { get; set; }
        [Reactive] public DateTime startTime { get; private set; }
        [Reactive] public DateTime endTime { get; private set; }
        [Reactive] public bool inDemoMode { get; set; }
        [Reactive] public string pollingStationName { get; set; }
        /// <summary>
        /// Controls whether registration should be permitted or not.
        /// It will likely be false if the application is launched outside the range
        /// </summary>
        [Reactive] public bool canRegister { get; set; }

        private readonly ObservableListX<DiagnosisResultViewModel, string> _diagnosisResults = new ObservableListX<DiagnosisResultViewModel, string>(x => x.name);
        public IObservable<List<DiagnosisResultViewModel>> diagnosisResults => _diagnosisResults.observable;
        public bool diagnosisOk { [ObservableAsProperty]get; }

        //Lookups
        private readonly ObservableListX<DropDownItemViewModel, long> _idTypes = new ObservableListX<DropDownItemViewModel, long>(x => x.id);
        public IObservable<List<DropDownItemViewModel>> idTypes => _idTypes.observable;
        private readonly ObservableListX<DropDownItemViewModel, long> _regions = new ObservableListX<DropDownItemViewModel, long>(x => x.id);
        public IObservable<List<DropDownItemViewModel>> regions => _regions.observable;
        private readonly ObservableListX<DropDownItemViewModel, long> _districts = new ObservableListX<DropDownItemViewModel, long>(x => x.id);
        public IObservable<List<DropDownItemViewModel>> districts => _districts.observable;
        private readonly ObservableListX<DropDownItemViewModel, long> _reasons = new ObservableListX<DropDownItemViewModel, long>(x => x.id);
        public IObservable<List<DropDownItemViewModel>> reasons => _reasons.observable;
    private readonly ObservableListX<DropDownItemViewModel, long> _pollingStations = new ObservableListX<DropDownItemViewModel, long>(x => x.id);
    public IObservable<List<DropDownItemViewModel>> pollingStations => _pollingStations.observable;

    //private DiagnosisSource 
    private List<DiagnosisResultViewModel> resultSource = new List<DiagnosisResultViewModel>
            {
                new DiagnosisResultViewModel(DiagnosisSource.Battery, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.ReportPrinter, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.SDCard, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.FingerprintReader, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.Licenses, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.DataIntegrity, "", DiagnosisStatus.NotChecked),
                new DiagnosisResultViewModel(DiagnosisSource.Camera, "", DiagnosisStatus.NotChecked)
            };

        private BehaviorSubject<DiagnosisSource> _diagnosisTriggers = new BehaviorSubject<DiagnosisSource>(DiagnosisSource.Battery);

        public SharedViewModel()
        {
            registrationData = RegistrationData.empty();
            _diagnosisResults.replaceWith(resultSource);
            _diagnosisResults.observable
                .Select(xs => !xs.Any(x => x.isEssential && x.status != DiagnosisStatus.Ok))
                .ToPropertyEx(this, x => x.diagnosisOk);

            _diagnosisTriggers
                .Subscribe(x =>
                {
                    Task.Run(() => runService(x));
                });
            //send all
            resultSource
                .Skip(1) //remove Battery since it was already added to the list
                .ToList()
                .ForEach(x => _diagnosisTriggers.OnNext(x.type));
            //check activation status
            var activationResult = App.isActivated();
            if (activationResult.IsOk) activated = activationResult.ResultValue;
            //dropdown data
            _idTypes.replaceWith(IdTypes.readAll().Select(x => new DropDownItemViewModel { id = x.id, name = x.name, code = x.code }).ToList());
            _regions.replaceWith(Regions.readAll().Select(x => new DropDownItemViewModel { id = x.id, name = x.name, code = x.code }).ToList());
            _districts.replaceWith(Districts.readAll().Select(x => new DropDownItemViewModel { id = x.id, name = x.name, parentId = x.regionId, code = x.code }).ToList());
            _reasons.replaceWith(Reasons.readAll().Select(x => new DropDownItemViewModel { id = x.id, name = x.name, code = x.code }).ToList());
      _pollingStations.replaceWith(PollingStations.readAll().Select(x => new DropDownItemViewModel { id = x.id, name = x.name, code = x.code }).OrderBy(x => x.name).ToList());
      //handler for demo
      this.WhenAnyValue(x => x.inDemoMode)
                .Subscribe(x =>
                {
                    if (!x) return; //ignore if false
                    switch (activeViewModel)
                    {
                        case null: return;
                        case StartViewModel _: break;
                        case LoginViewModel _: break;
                        case BioVerificationViewModel _: break;
                        default: return;
                    }
                    App.demoMode(x);
                });
            this.WhenAnyValue(x => x.printerName)
                .Subscribe(x => ReportPrinterService.printerName = x);

            Task.Run(initialize);
        }
        private async Task<Unit> runService(DiagnosisSource source)
        {
            (bool active, string message) f = (false, "Not processed");
            switch (source)
            {
                case DiagnosisSource.Battery:
                    f = await BatteryService.getStatus();
                    break;
                case DiagnosisSource.DataIntegrity:
                    f = await DataIntegrityService.getStatus();
                    break;
                case DiagnosisSource.FingerprintReader:
                    f = await FingerPrintService.getStatus();
                    break;
                case DiagnosisSource.Licenses:
                    f = await LicenseService.getStatus();
                    break;
                case DiagnosisSource.ReportPrinter:
                    f = await ReportPrinterService.getStatus();
                    break;
                case DiagnosisSource.SDCard:
                    f = await SDCardService.getStatus();
                    break;
                case DiagnosisSource.Camera:
                    f = await CameraService.getStatus();
                    break;
            }
            var result = resultSource.FirstOrDefault(x => x.type == source);
            if (result == null) return Unit.Default;
            if (f.active)
            {
                result.status = DiagnosisStatus.Ok;
            }
            else
            {
                result.status = DiagnosisStatus.Error;
            }
            result.notes = f.message ?? "";
            _diagnosisResults.addOrReplace(result);
            //sleep and try again later. Make it shorter if it is a failure
            await Task.Delay(f.active ? 10000 : 7500);
            _diagnosisTriggers.OnNext(source);
            //trigger the next call
            return Unit.Default;
        }

        private async Task initialize()
        {
            //todo: is this the best place for this?
            //todo: see how to log failure
            App.init();
            pollingStationName = App.config.name;
            startTime = DateTime.Now.Date.Add(App.config.startTime);
            endTime = DateTime.Now.Date.Add(App.config.endTime);
            canRegister = startTime >= App.config.startDate && endTime.Date <= App.config.endDate;
            Data.Registrations.init();
            var countsResult = Data.Registrations.registrationCount();
            if (countsResult.IsOk)
            {
                registrationsSoFar = (int)countsResult.ResultValue.all;
                registrationsToday = (int)countsResult.ResultValue.today;
                registrationsSinceBackup = registrationsToday % 100; //todo: use settings
            }
            //initialize the biometric clients
            await BioService.State.singleton.initVoterClientAsync();
            await BioService.State.singleton.initStaffClientAsync();
            clientsReady = true;
        }

        public enum Activity
        {
            Login, Logout, CardPrinting, ReportPrinting, Backup, Registration, Shutdown, Activation
        }

        public static SharedViewModel singleton { get; } = new SharedViewModel();
    }

}
