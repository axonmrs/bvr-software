﻿using MRegistrar.Config;
using MRegistrar.ViewModels.Interfaces;
using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class DeviceDetailsViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Connected Devices Information";
        public IScreen HostScreen { get; }

        [Reactive] public List<string> devices { get; set; }
        [Reactive] public byte[] qrCode { get; set; }
        [Reactive] public string qrCodeData { get; set; }
        [Reactive] public string message { get; set; }
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => false;
        Func<Task> IViewSettings.onCancel => () => null;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public ReactiveCommand<Unit, Unit> clearError { get; }

        public DeviceDetailsViewModel(IScreen screen = null)
        {
            try
            {
                HostScreen = screen ?? Locator.Current.GetService<IScreen>();
                clearError = ReactiveCommand.Create(() =>
                {
                    //errorMessage = "";
                });
                this.qrCodeData = GetDevices();
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            
        }
        private string GetDevices()
        {
            var res = new List<DeviceInfo>();
            var machineSerial = DeviceUtils.GetDeviceSerial();
            res.Add(new DeviceInfo
            {
                Type ="Computer",
                DeviceID = "",
                SerialNumber = machineSerial
            });
           return JsonConvert.SerializeObject(res);
        }
    }

    public class DeviceInfo
    {
        public string Type { get; set; }
        public string DeviceID { get; set; }
        public string SerialNumber { get; set; }
    }
}
