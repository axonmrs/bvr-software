﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRegistrar.ViewModels
{
    public class DropDownItemViewModel: ReactiveObject
    {
        [Reactive] public long id { get; set; }
        [Reactive] public long parentId { get; set; }
        [Reactive] public string name { get; set; }
        [Reactive] public string code { get; set; }
    }
}
