﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class CaptureFaceViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        Func<Task> IViewSettings.onCancel => async () =>
        {
            try
            {
                if (onCleanup != null)
                    await onCleanup();
            }
            catch { }
        };
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;

        public string UrlPathSegment => "Capture Face";
        public IScreen HostScreen { get; }

        [Reactive] public bool capturing { get; set; }
        [Reactive] public bool hasImage { get; set; }
        [Reactive] public bool isImageValid { get; set; }

        public ReactiveCommand<Unit, Unit> openConfirmView { get; }
        public ReactiveCommand<Unit, Unit> beginCapture { get; }
        public ReactiveCommand<Unit, Unit> endCapture { get; }

        public Func<Task> onStart { get; set; }
        public Func<Task> onCapture { get; set; }
        public Func<Task> onCleanup { get; set; }

        //icao
        public IcaoWarningsViewModel icao { get; }

        public CaptureFaceViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            var canContinue =
                this.WhenAny(x => x.hasImage, x => x.Value);
            icao = new IcaoWarningsViewModel();
            openConfirmView = ReactiveCommand.CreateFromTask(async () =>
            {
                capturing = false;
                try
                {
                    if (onCleanup != null) await onCleanup();
                }
                catch { }
                SharedViewModel.singleton.activeViewModel = new NewRegistrationViewModel();
            }, canContinue);
            beginCapture = ReactiveCommand.CreateFromTask(async () => {
                capturing = true;
                await onStart();
                }, this.WhenAnyValue(x => x.capturing).Select(x => !x));
            beginCapture.ThrownExceptions
                .Subscribe(x =>
                {
                    //todo: needed?
                });
            endCapture = ReactiveCommand.CreateFromTask(async () =>
            {
                capturing = false;
                await onCapture();
            }, this.WhenAnyValue(x => x.capturing));
        }
    }
}
