﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class QueueManagementViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => null;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Queue Cards";

        [Reactive] public string reportTitle { get; set; }
        [Reactive] public long generateRequests { get; set; }
        [Reactive] public long printRequests { get; set; }
        [Reactive] public bool busy { get; set; }
        [Reactive] public bool hasFile { get; set; }
        public ReactiveCommand<Unit, Unit> generateReport { get; }
        public ReactiveCommand<Unit, Unit> printReport { get; }

        [Reactive] public int startNumber { get; set; }
        [Reactive] public int count { get; set; }
        [Reactive] public DateTime date { get; set; }

        public IScreen HostScreen { get; }

        public QueueManagementViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            reportTitle = "";
            generateReport = ReactiveCommand.Create(() => {
                if (busy) return;
                generateRequests++;
            });
            printReport = ReactiveCommand.Create(() =>
            {
                if (busy) return;
                printRequests++;
            }, this.WhenAnyValue(x => x.hasFile));
            date = DateTime.Now.Date.AddDays(1);
            startNumber = 1;
            count = 1;
        }
    }
}
