﻿using MRegistrar.ViewModels.Helpers;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using Neurotec.Biometrics;
using System.Linq;
using System.ComponentModel;
using System.Collections.Specialized;

namespace MRegistrar.ViewModels
{
    public class IcaoWarningsViewModel : ReactiveObject
    {
        private readonly ObservableListX<IcaoWarningViewModel, string> _warningResults = new ObservableListX<IcaoWarningViewModel, string>(x => x.name);
        public IObservable<List<IcaoWarningViewModel>> warningResults => _warningResults.observable;
        public bool icaoOk { [ObservableAsProperty]get; }

        private List<IcaoWarningViewModel> warnings
        {
            get { return _warningResults.current; }
            set
            {
                _warningResults.replaceWith(value);
            }
        }

        private NLAttributes _attributes = null;
        private NFace _face = null;
        public NFace face
        {
            get { return _face; }
            set
            {
                cleanUp();
                _face = value;
                setup();
                updateResults();
            }
        }

        public IcaoWarningsViewModel()
        {
            warnings = new List<IcaoWarningViewModel>
            {
                //new IcaoWarningViewModel(NIcaoWarnings.FaceNotDetected, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.RollLeft, WarningStatus.NotChecked),
                //new IcaoWarningViewModel(NIcaoWarnings.RollRight, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.YawLeft, WarningStatus.NotChecked),
                //new IcaoWarningViewModel(NIcaoWarnings.YawRight, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.PitchUp, WarningStatus.NotChecked),
                //new IcaoWarningViewModel(NIcaoWarnings.PitchDown, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooNear, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooFar, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooNorth, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooSouth, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooEast, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.TooWest, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.Sharpness, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.BackgroundUniformity, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.GrayscaleDensity, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.Saturation, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.Expression, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.DarkGlasses, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.Blink, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.MouthOpen, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.LookingAway, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.RedEye, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.FaceDarkness, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.UnnaturalSkinTone, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.WashedOut, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.Pixelation, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.SkinReflection, WarningStatus.NotChecked),
                new IcaoWarningViewModel(NIcaoWarnings.GlassesReflection, WarningStatus.NotChecked)
            };
        }
        public void cleanUp()
        {
            if (_face != null) _face.Objects.CollectionChanged -= OnObjectsCollectionChanged;
            if (_attributes != null) _attributes.PropertyChanged -= OnAttributesPropertyChanged;
        }
        private void setup()
        {
            if (_face != null)
            {
                _face.Objects.CollectionChanged += OnObjectsCollectionChanged;
                _attributes = _face.Objects.ToArray().FirstOrDefault();
                if (_attributes != null) _attributes.PropertyChanged += OnAttributesPropertyChanged;
            }
        }
        private void updateResults()
        {
            if (_attributes != null)
            {
                var icaoWarnings = _attributes.IcaoWarnings;
                if ((icaoWarnings & NIcaoWarnings.FaceNotDetected) == NIcaoWarnings.FaceNotDetected)
                {
                    warnings = warnings.Select(x =>
                    {
                        x.status = WarningStatus.NotChecked;
                        return x;
                    }).ToList();
                }
                else
                {
                    warnings = warnings.Select(x =>
                    {
                        x.update(icaoWarnings, _attributes);
                        return x;
                    }).ToList();
                }
            }
        }

        private void OnAttributesPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IcaoWarnings") updateResults();
        }

        private void OnObjectsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (((NFace.ObjectCollection)sender).Owner != _face) return;
                    if (_attributes != null) _attributes.PropertyChanged -= OnAttributesPropertyChanged;
                    _attributes = (NLAttributes)e.NewItems[0];
                    _attributes.PropertyChanged += OnAttributesPropertyChanged;
                    break;
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (((NFace.ObjectCollection)sender).Owner == _face)
                    {
                        if (_attributes != null) _attributes.PropertyChanged -= OnAttributesPropertyChanged;
                        _attributes = null;
                    }
                    break;
            }
        }
    }
}
