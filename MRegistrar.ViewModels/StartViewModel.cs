﻿using DynamicData;
using DynamicData.Binding;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class StartViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Diagnostics";
        public IScreen HostScreen { get; }

        bool IViewSettings.showTray => false;
        bool IViewSettings.showStatusSummaries => false;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        Func<Task> IViewSettings.onCancel => null;
        Func<Task> IViewSettings.onBack => null;

        public StartViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
        }
    }
}
