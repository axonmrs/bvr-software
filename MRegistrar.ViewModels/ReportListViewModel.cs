﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class ReportListViewModel: ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Reports";

        public IScreen HostScreen { get; }
        public ReactiveCommand<ReportViewModel.ReportTypes, Unit> openReport { get; }

        public ReportListViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            openReport = ReactiveCommand.Create<ReportViewModel.ReportTypes, Unit>(runOpenReport);
        }

        private Unit runOpenReport(ReportViewModel.ReportTypes reportType)
        {
            var vm = new ReportViewModel { reportType = reportType };
            SharedViewModel.singleton.activeViewModel = vm;
            return Unit.Default;
        }
    }
}
