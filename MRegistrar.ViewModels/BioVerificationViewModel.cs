﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class BioVerificationViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Biometric Verification";

        public IScreen HostScreen { get; }
        public bool showTray => true;
        public bool showStatusSummaries => true;
        public bool enableLogOut
        {
            get { return identifyVoter; }
        }
        public Func<Task> onCancel => () => Task.CompletedTask;
        public Func<Task> onBack
        {
            get
            {
                if (getPreviousViewModel != null)
                    return async () => { SharedViewModel.singleton.activeViewModel = await getPreviousViewModel(); };
                return null;
            }
        }
        public bool enableShutDown => true;
        public bool enableTransfer => false;
        public bool enableBackup => false;

        public long userId { get; set; }
        [Reactive] public string title { get; set; }
        /// <summary>
        /// True to capture face, False to capture finger
        /// </summary>
        [Reactive] public bool useFace { get; set; }
        [Reactive] public bool capturing { get; set; }
        /// <summary>
        /// True to identify a voter, False to identify a user
        /// </summary>
        [Reactive] public bool identifyVoter { get; set; }
        [Reactive] public string lastMessage { get; set; }
        [Reactive] public string notes { get; set; }
        public Func<string, Task<IRoutableViewModel>> getNextViewModel { get; set; }
        public Func<Task<IRoutableViewModel>> getPreviousViewModel { get; set; }
        public ReactiveCommand<Unit, Unit> beginFinderCapture { get; }

        public ReactiveCommand<Unit, Unit> processBioLogin { get; set; }
        public ReactiveCommand<Unit, Unit> useFaceIdentification { get; private set; }
        public ReactiveCommand<Unit, Unit> useFingerIdentification { get; private set; }

        public Func<Task> onStart { get; set; } 


        public BioVerificationViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            useFaceIdentification = ReactiveCommand.Create(() => { useFace = true; }, this.WhenAnyValue(x => x.useFace, x => !x));
            useFingerIdentification = ReactiveCommand.Create(() => { useFace = false; }, this.WhenAnyValue(x => x.useFace));
            beginFinderCapture = ReactiveCommand.CreateFromTask(async () => {
                capturing = true;
                await onStart();
            }, this.WhenAnyValue(x => x.capturing, capturing => !capturing));
            beginFinderCapture.ThrownExceptions
                .Subscribe(x =>
                {
                    //todo: needed?
                    capturing = false;
                });


           

        }

       
    }
}
