﻿using MRegistrar.ViewModels.Helpers;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using ReactiveUI.Validation.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class NewRegistrationViewModel : ReactiveObject, IRoutableViewModel, IViewSettings, IValidatableViewModel
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "New Registration";

        public IScreen HostScreen { get; }

        [Reactive] public string otherNames { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public DateTime dateOfBirth { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public bool isEstimatedAge { get; set; }
        [Reactive] public string sex { get; set; }
        [Reactive] public string phoneNumber { get; set; }
        [Reactive] public string residentialAddress { get; set; }
        [Reactive] public string residentialTown { get; set; }
        [Reactive] public long? residentialDistrictId { get; set; }
        [Reactive] public long? residentialRegionId { get; set; }
        [Reactive] public long? idTypeId { get; set; }
        [Reactive] public string idNumber { get; set; }
        [Reactive] public string fatherName { get; set; }
        [Reactive] public string motherName { get; set; }
        [Reactive] public string hometownAddress { get; set; }
        [Reactive] public string hometown { get; set; }
        [Reactive] public long? hometownDistrictId { get; set; }
        [Reactive] public long? hometownRegionId { get; set; }
        [Reactive] public bool isVisuallyImpaired { get; set; }
        [Reactive] public bool isDisabled { get; set; }
        [Reactive] public string guarantor1VoterNumber { get; set; }
        [Reactive] public string guarantor2VoterNumber { get; set; }
        [Reactive] public string guarantor1Name { get; set; }
        [Reactive] public string guarantor2Name { get; set; }
        [Reactive] public string challengeNotes { get; set; }
        [Reactive] public Forms activeForm { get; set; }
        [Reactive] public bool noIdCard { get; set; }
        [Reactive] public bool noChallenge { get; set; }
        //[Reactive] public bool challenged { get; set; }
        [Reactive] public string formTitle { get; set; }
        [Reactive] public bool hearingImpaired { get; set; }
        [Reactive] public bool leper { get; set; }
        [Reactive] public bool missingFingersL { get; set; }
        [Reactive] public bool missingFingersR { get; set; }
        [Reactive] public bool amputatedHandsL { get; set; }
        [Reactive] public bool amputatedHandsR { get; set; }

        //for extra validations
        [Reactive] public string idTypeErrorMessage { get; set; }
        [Reactive] public string idNumberErrorMessage { get; set; }
        [Reactive] public string guarantor1VoterErrorMessage { get; set; }
        [Reactive] public string guarantor2VoterErrorMessage { get; set; }
        [Reactive] public string challengeErrorMessage { get; set; }

        public Func<List<DropDownItemViewModel>> getChallengeReasons { get; set; } 
        public ReactiveCommand<Unit, Unit> gotoNext { get; }
        public ReactiveCommand<Unit, Unit> gotoPrevious { get; }

        public ValidationContext ValidationContext { get; } = new ValidationContext();
        public ValidationHelper ageRule { get; }
        public ValidationHelper phoneNumberRule { get; }
        public ValidationHelper residentialRegionRule { get; }
        public ValidationHelper residentialDistrictRule { get; }
        public ValidationHelper hometownRegionRule { get; }
        public ValidationHelper hometownDistrictRule { get; }

        public bool showBioForm { get; set; } = true;
        public bool showIdVerificationForm { get; set; }

        private BehaviorSubject<bool> _otherFieldValidation = new BehaviorSubject<bool>(false);

        public NewRegistrationViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            activeForm = Forms.BioForm;
            this.WhenAnyValue(x => x.dateOfBirth)
                .Select(x => computeAge(x))
                .Where(x => x != age)
                .BindTo(this, x => x.age);
            this.WhenAnyValue(x => x.age)
                .Select(x => new DateTime(DateTime.Now.Year - x, 1, 1))
                .Where(x => x != dateOfBirth)
                .BindTo(this, x => x.dateOfBirth);
            age = 18;
            populateExistingData();
            //set the region ids
            this.WhenAnyValue(x => x.hometownDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == hometownDistrictId))
                .Select(x => x?.parentId)
                .BindTo(this, x => x.hometownRegionId);
            this.WhenAnyValue(x => x.residentialDistrictId)
                .SelectMany(x => SharedViewModel.singleton.districts)
                .Select(xs => xs.FirstOrDefault(x => x.id == residentialDistrictId))
                .Select(x => x?.parentId)
                .BindTo(this, x => x.residentialRegionId);

            // validations
            this.ValidationRule(vm => vm.surname, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.otherNames, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.residentialAddress, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.residentialTown, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.hometownAddress, x => !string.IsNullOrWhiteSpace(x), "Required");
            this.ValidationRule(vm => vm.hometown, x => !string.IsNullOrWhiteSpace(x), "Required");
            ageRule = this.ValidationRule(vm => vm.age, age => age >= 18, "Must be 18 years and above");
           
            phoneNumberRule = this.ValidationRule(vm => vm.phoneNumber, isPhoneNumber, "Invalid Phone Number.");
            residentialRegionRule = this.ValidationRule(vm => vm.residentialRegionId, x => x != null, "Required");
            residentialDistrictRule = this.ValidationRule(vm => vm.residentialDistrictId, x => x != null, "Required");
            hometownRegionRule = this.ValidationRule(vm => vm.hometownRegionId, x => x != null, "Required");
            hometownDistrictRule = this.ValidationRule(vm => vm.hometownDistrictId, x => x != null, "Required");

            //validations
            var formIsValid = this.IsValid().CombineLatest(_otherFieldValidation, (f, o) => f && o);
            this.WhenAnyValue(x => x.idTypeId, x => x.idNumber, x => x.guarantor1VoterNumber, x => x.guarantor2VoterNumber, x => x.noIdCard)
                .Subscribe(_ => validateOtherFields());

            gotoNext = ReactiveCommand.Create(() =>
            {
                switch (activeForm)
                {
                    case Forms.BioForm:
                        if (SharedViewModel.singleton.registrationData.kind == RegistrationData.Kind.Existing)
                            commitAndMoveToConfirmationPage();
                        else
                            activeForm = Forms.IdVerificationForm;
                        //hack to force validation
                        var temp = idNumber; idNumber = "blah"; idNumber = temp;
                        var temp1 = idTypeId; idTypeId = -1; idTypeId = temp1;
                        temp = guarantor1VoterNumber; guarantor1VoterNumber = "blah"; guarantor1VoterNumber = temp;
                        temp = guarantor2VoterNumber; guarantor2VoterNumber = "blah"; guarantor2VoterNumber = temp;
                        break;
                    case Forms.IdVerificationForm:
                        var formData = new RegistrationFormData
                        {
                            otherNames = otherNames,
                            surname = surname,
                            dateOfBirth = dateOfBirth,
                            age = age,
                            isEstimatedAge = isEstimatedAge,
                            sex = sex,
                            phoneNumber = phoneNumber,
                            residentialAddress = residentialAddress,
                            residentialTown = residentialTown,
                            residentialDistrictId = residentialDistrictId ?? 0,
                            residentialRegionId = residentialRegionId ?? 0,
                            idTypeId = idTypeId ?? 0,
                            idNumber = idNumber,
                            //idExpiry = idExpiry,
                            fatherName = fatherName,
                            motherName = motherName,
                            hometownAddress = hometownAddress,
                            hometown = hometown,
                            hometownDistrictId = hometownDistrictId ?? 0,
                            hometownRegionId = hometownRegionId ?? 0,
                            isVisuallyImpaired = isVisuallyImpaired,
                            isDisabled = isDisabled,
                            amputatedHandsL = amputatedHandsL,
                            amputatedHandsR = amputatedHandsR,
                            missingFingersL = missingFingersL,
                            missingFingersR = missingFingersR,
                            leper = leper,
                            hearingImpaired = hearingImpaired,
                            guarantor1Name = guarantor1Name,
                            guarantor1VoterNumber = guarantor1VoterNumber,
                            guarantor2Name = guarantor2Name,
                            guarantor2VoterNumber = guarantor2VoterNumber
                        };
                        SharedViewModel.singleton.registrationData.form = formData;
                        activeForm = Forms.ChallengeForm;
                        break;
                    case Forms.ChallengeForm:
                        var data = SharedViewModel.singleton.registrationData.form;
                        data.challengeReasons = getChallengeReasons() ?? new List<DropDownItemViewModel>();
                        data.challengeNotes = challengeNotes;
                        SharedViewModel.singleton.registrationData.form = data;
                        SharedViewModel.singleton.activeViewModel = new ConfirmRegistrationViewModel();
                        break;
                }
            }, formIsValid);

            gotoPrevious = ReactiveCommand.Create(() =>
            {
                switch (activeForm)
                {
                    case Forms.BioForm:
                        //activeForm = Forms.IdVerificationForm;
                        break;
                    case Forms.IdVerificationForm:
                        activeForm = Forms.BioForm;
                        break;
                    case Forms.ChallengeForm:
                        activeForm = Forms.IdVerificationForm;
                        break;
                }
                validateOtherFields();
            });
        }

        private static bool matchesIdFormat(long? idType, string idNumber)
        {
            if (string.IsNullOrWhiteSpace(idNumber)) return false;
            if (idType == Config.Configuration.Settings.PassportId) return idNumber.Length == 8;
            if (idType == Config.Configuration.Settings.NationalIDId) return idNumber.Length == 9;
            return false;
        }

        private static bool isVoterNumber(string voterId)
        {
            if (string.IsNullOrWhiteSpace(voterId)) return false;
            //todo: validate
            return true;
        }

        private void validateOtherFields()
        {
            //reset
            _otherFieldValidation.OnNext(true);
            guarantor1VoterErrorMessage = guarantor2VoterErrorMessage = idNumberErrorMessage = idTypeErrorMessage = challengeErrorMessage = "";
            var isValid = true;
            //perform the validations
            switch (activeForm)
            {
                case Forms.IdVerificationForm:
                    switch (noIdCard)
                    {
                        case true: //validate guarantor ids
                            if (string.IsNullOrWhiteSpace(guarantor1VoterNumber))
                            {
                                isValid = false;
                                guarantor1VoterErrorMessage = "Required";
                            }
                            else if (!Data.Registrations.isValidVoterNumber(guarantor1VoterNumber))
                            {
                                isValid = false;
                                guarantor1VoterErrorMessage = "Invalid format";
                            }
                            if (string.IsNullOrWhiteSpace(guarantor2VoterNumber))
                            {
                                isValid = false;
                                guarantor2VoterErrorMessage = "Required";
                            }
                            else if (!Data.Registrations.isValidVoterNumber(guarantor2VoterNumber))
                            {
                                isValid = false;
                                guarantor2VoterErrorMessage = "Invalid format";
                            } else if (guarantor1VoterNumber == guarantor2VoterNumber)
                            {
                                isValid = false;
                                guarantor2VoterErrorMessage = "Must be different";
                            }
                            break;
                        case false: //validate id
                            if (!idTypeId.HasValue)
                            {
                                isValid = false;
                                idTypeErrorMessage = "Required";
                            }
                            if (!matchesIdFormat(idTypeId, idNumber))
                            {
                                isValid = false;
                                idNumberErrorMessage = "Required";
                            }
                            break;
                    }
                    break;
                case Forms.ChallengeForm:
                    switch (noChallenge)
                    {
                        case true: break;
                        case false:
                            //todo: is it necessary to ensure that at least 1 is selected?
                            break;
                    }
                    break;
            }
            _otherFieldValidation.OnNext(isValid);
        }

        private void commitAndMoveToConfirmationPage()
        {
            var formData = new RegistrationFormData
            {
                otherNames = otherNames,
                surname = surname,
                dateOfBirth = dateOfBirth,
                age = age,
                isEstimatedAge = isEstimatedAge,
                sex = sex,
                phoneNumber = phoneNumber,
                residentialAddress = residentialAddress,
                residentialTown = residentialTown,
                residentialDistrictId = residentialDistrictId ?? 0,
                residentialRegionId = residentialRegionId ?? 0,
                idTypeId = noIdCard ? 0 : idTypeId ?? 0,
                idNumber = noIdCard ? "" : idNumber,
                fatherName = fatherName,
                motherName = motherName,
                hometownAddress = hometownAddress,
                hometown = hometown,
                hometownDistrictId = hometownDistrictId ?? 0,
                hometownRegionId = hometownRegionId ?? 0,
                isVisuallyImpaired = isVisuallyImpaired,
                isDisabled = isDisabled,
                amputatedHandsL = amputatedHandsL,
                amputatedHandsR = amputatedHandsR,
                missingFingersL = missingFingersL,
                missingFingersR = missingFingersR,
                leper = leper,
                hearingImpaired = hearingImpaired,
                guarantor1Name = noIdCard ? guarantor1Name : "",
                guarantor1VoterNumber = noIdCard ? guarantor1VoterNumber:  "",
                guarantor2Name = noIdCard ? guarantor2Name : "",
                guarantor2VoterNumber = noIdCard ? guarantor2VoterNumber : ""
            };
            SharedViewModel.singleton.registrationData.form = formData;
            SharedViewModel.singleton.activeViewModel = new ConfirmRegistrationViewModel();
        }
        private void populateExistingData()
        {
            if (SharedViewModel.singleton.registrationData.kind != RegistrationData.Kind.Existing) return;
            var existing = SharedViewModel.singleton.registrationData.form;
            if (existing == null) return;
            otherNames = existing.otherNames;
            surname = existing.surname;
            dateOfBirth = existing.dateOfBirth;
            age = existing.age;
            isEstimatedAge = existing.isEstimatedAge;
            sex = existing.sex;
            phoneNumber = existing.phoneNumber;
            residentialAddress = existing.residentialAddress;
            residentialTown = existing.residentialTown;
            residentialDistrictId = existing.residentialDistrictId;
            residentialRegionId = existing.residentialRegionId;
            idTypeId = existing.idTypeId;
            idNumber = existing.idNumber;
            fatherName = existing.fatherName;
            motherName = existing.motherName;
            hometownAddress = existing.hometownAddress;
            hometown = existing.hometown;
            hometownDistrictId = existing.hometownDistrictId;
            hometownRegionId = existing.hometownRegionId;
            isVisuallyImpaired = existing.isVisuallyImpaired;
            isDisabled = existing.isDisabled;
        }

        private int computeAge(DateTime dob)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - dob.Year;
            if (today < dob.AddYears(age)) age--;
            return age;
        }

        public enum Forms
        {
            BioForm, IdVerificationForm, ChallengeForm
        }

        private static bool isPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return true; //accept empty
            if (phoneNumber.Any(x => !char.IsDigit(x))) return false;
            if (phoneNumber.Length == 13 && phoneNumber.StartsWith("+233")) return true;
            if (phoneNumber.Length == 12 && phoneNumber.StartsWith("233")) return true;
            if (phoneNumber.Length == 10 && phoneNumber.StartsWith("0")) return true;
            return false;
        }
    }
}
