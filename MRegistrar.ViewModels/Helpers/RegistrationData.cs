﻿using Neurotec.Biometrics;
using Neurotec.Images;
using System;
using System.Collections.Generic;
using System.Text;

namespace MRegistrar.ViewModels.Helpers
{
    public class RegistrationData
    {
        public Kind kind { get; set; }
        public byte[] picture { get; set; }
        public byte[] j2kPhoto { get; set; }
        public RegistrationFormData form { get; set; }
        public byte[] pictureTemplate { get; set; }
        public Dictionary<NFPosition, (byte[] tpl, byte[] wsq)> fingers { get; set; }

        public Dictionary<NFPosition,NImage> fingerImages { get; set; }
        public NImage faceImage { get; set; }

        public byte[] compositeTemplate { get; set; }

        public static RegistrationData empty ()
        {
            return new RegistrationData
            {
                kind = Kind.Unknown,
                picture = new byte[0],
                form = new RegistrationFormData()
            };
        }

        public void reset()
        {
            kind = Kind.Unknown;
            picture = new byte[0];
            form = new RegistrationFormData();
            picture = new byte[0];
            j2kPhoto = new byte[0];
            pictureTemplate = new byte[0];
            fingers = new Dictionary<NFPosition, (byte[] tpl, byte[] wsq)>();
            fingerImages = new Dictionary<NFPosition, NImage>();
            faceImage = null;
            compositeTemplate = null;
        }


        public enum Kind
        {
            New, Existing, Unknown
        }
    }

    public class StaffData
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }

        public Dictionary<NFPosition, NImage> Fingers { get; set; }
    }
    public class BiometricUser
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public bool Selected { get; set; }
        public string UserName { get; set; }
    }
}
