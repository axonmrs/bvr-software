﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRegistrar.ViewModels.Helpers
{
    public class RegistrationFormData
    {
        public string otherNames { get; set; }
        public string surname { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int age { get; set; }
        public bool isEstimatedAge { get; set; }
        public string sex { get; set; }
        public string phoneNumber { get; set; }
        public string residentialAddress { get; set; }
        public string residentialTown { get; set; }
        public long residentialDistrictId { get; set; }
        public string residentialDistrict { get; set; }
        public long residentialRegionId { get; set; }
        public string residentialRegion { get; set; }
        public long idTypeId { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
        public DateTime idExpiry { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public string hometownAddress { get; set; }
        public string hometown { get; set; }
        public long hometownDistrictId { get; set; }
        public string hometownDistrict { get; set; }
        public long hometownRegionId { get; set; }
        public string hometownRegion { get; set; }
        public bool isVisuallyImpaired { get; set; }
        public bool isDisabled { get; set; }
        public bool hearingImpaired { get; set; }
        public bool leper { get; set; }
        public bool missingFingersL { get; set; }
        public bool missingFingersR { get; set; }
        public bool amputatedHandsL { get; set; }
        public bool amputatedHandsR { get; set; }
        public List<DropDownItemViewModel> challengeReasons { get; set; }
        public string challengeNotes { get; set; }
        public string guarantor1VoterNumber { get; set; }
        public string guarantor2VoterNumber { get; set; }
        public string guarantor1Name { get; set; }
        public string guarantor2Name { get; set; }

    }
}
