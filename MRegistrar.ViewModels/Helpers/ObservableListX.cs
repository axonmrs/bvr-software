﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;

namespace MRegistrar.ViewModels.Helpers
{
    public class ObservableListX<T, TKey>
    {
        private BehaviorSubject<List<T>> _store;
        private Func<T, TKey> getId;
        private Comparer<TKey> comparer = Comparer<TKey>.Default;

        public IObservable<List<T>> observable => _store;
        public List<T> current => _store.Value;

        public ObservableListX(Func<T, TKey> getId)
        {
            _store = new BehaviorSubject<List<T>>(new List<T>());
            this.getId = getId;
        }

        public void add(T newItem)
        {
            var xs = _store.Value.ToList();
            xs.Add(newItem);
            //_store.Value.Add(newItem);
            _store.OnNext(xs);
        }

        public void addOrReplace(T item)
        {
            var id = getId(item);
            var existing = _store.Value.FirstOrDefault(x => comparer.Compare(getId(x), id) == 0);
            if (existing == null)
                add(item);
            else
            {
                var index = _store.Value.IndexOf(existing);
                var xs = _store.Value.ToList();
                xs[index] = item;
                _store.OnNext(xs);
            }            
        }

        public void replaceWith(List<T> xs)
        {
            _store.OnNext(xs);
        }
    }
}
