﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class AddDataEntryClerkViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public string UrlPathSegment => "Add Data Entry Clerk";
        public IScreen HostScreen { get; }

        [Reactive] public long clerkId { get; set; }
        [Reactive] public List<RegistrationUserViewModel> officers { get; set; }
        public ReactiveCommand<Unit, Unit> add { get; }

        public ReactiveCommand<Unit, Unit> addNewDEC { get; }
        [Reactive] public string message { get; set; }

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => false;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => null;
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        bool IViewSettings.enableBackup => true;

        public AddDataEntryClerkViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            
            addNewDEC = ReactiveCommand.Create(() =>
            {
                SharedViewModel.singleton.activeViewModel = new CreateUserFormViewModel(role:"DataEntryClerk");
            });
        }
        private void addOfficer(long id)
        {
            var result = Data.Users.toggleSelectUser(id, true);
            if (result.IsOk)
            {
                var vm = new DataEntryClerksViewModel();
                SharedViewModel.singleton.activeViewModel = vm;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
    }
}
