﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Text;
using MRegistrar.Data;
using System.Reactive;
using System.Linq;
using Neurotec.Biometrics;
using System.Reactive.Linq;
using System.Threading.Tasks;
using MRegistrar.BioService;
using NLog;

namespace MRegistrar.ViewModels
{
    public class ConfirmRegistrationViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => true;
        public string UrlPathSegment => "Confirm Registration";
        public IScreen HostScreen { get; }

        [Reactive] public string voterId { get; set; }
        [Reactive] public string otherNames { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public DateTime dateOfBirth { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public bool isEstimatedAge { get; set; }
        [Reactive] public string sex { get; set; }
        [Reactive] public string phoneNumber { get; set; }
        [Reactive] public string residentialAddress { get; set; }
        [Reactive] public string residentialTown { get; set; }
        [Reactive] public long residentialDistrictId { get; set; }
        [Reactive] public string residentialDistrict { get; set; }
        [Reactive] public long residentialRegionId { get; set; }
        [Reactive] public string residentialRegion { get; set; }
        [Reactive] public long idTypeId { get; set; }
        [Reactive] public string idType { get; set; }
        [Reactive] public string idNumber { get; set; }
        [Reactive] public DateTime idExpiry { get; set; }
        [Reactive] public string fatherName { get; set; }
        [Reactive] public string motherName { get; set; }
        [Reactive] public string hometownAddress { get; set; }
        [Reactive] public string hometown { get; set; }
        [Reactive] public long hometownDistrictId { get; set; }
        [Reactive] public string hometownDistrict { get; set; }
        [Reactive] public long hometownRegionId { get; set; }
        [Reactive] public string hometownRegion { get; set; }
        [Reactive] public bool isVisuallyImpaired { get; set; }
        [Reactive] public bool isDisabled { get; set; }
        [Reactive] public string guarantor1VoterNumber { get; set; }
        [Reactive] public string guarantor2VoterNumber { get; set; }
        [Reactive] public string guarantor1Name { get; set; }
        [Reactive] public string guarantor2Name { get; set; }
        /// <summary>
        /// The face iamge as jpeg
        /// </summary>
        [Reactive] public byte[] photo { get; set; }
        /// <summary>
        /// The face image as j2k
        /// </summary>
        [Reactive] public byte[] j2kPhoto { get; set; }
        [Reactive] public byte[] faceTemplate { get; set; }
        [Reactive] public byte[] leftThumb { get; set; }
        [Reactive] public byte[] leftIndex { get; set; }
        [Reactive] public byte[] leftMiddle { get; set; }
        [Reactive] public byte[] leftRing { get; set; }
        [Reactive] public byte[] leftPinky { get; set; }
        [Reactive] public byte[] rightThumb { get; set; }
        [Reactive] public byte[] rightIndex { get; set; }
        [Reactive] public byte[] rightMiddle { get; set; }
        [Reactive] public byte[] rightRing { get; set; }
        [Reactive] public byte[] rightPinky { get; set; }
        [Reactive] public byte[] leftThumbWsq { get; set; }
        [Reactive] public byte[] leftIndexWsq { get; set; }
        [Reactive] public byte[] leftMiddleWsq { get; set; }
        [Reactive] public byte[] leftRingWsq { get; set; }
        [Reactive] public byte[] leftPinkyWsq { get; set; }
        [Reactive] public byte[] rightThumbWsq { get; set; }
        [Reactive] public byte[] rightIndexWsq { get; set; }
        [Reactive] public byte[] rightMiddleWsq { get; set; }
        [Reactive] public byte[] rightRingWsq { get; set; }
        [Reactive] public byte[] rightPinkyWsq { get; set; }
        [Reactive] public DateTime dateOfRegistration { get; set; }
        [Reactive] public string name { get; set; }
        [Reactive] public string pollingStationCode { get; set; }

        [Reactive] public string errorMessage { get; set; }
        [Reactive] public bool canPrint { get; set; }

        public ReactiveCommand<Unit, bool> save { get; }
        public Func<string, Task> printCard { get; set; }
        public byte[] qrCode { get; set; }

        public ConfirmRegistrationViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            canPrint = true;

            save = ReactiveCommand.CreateFromTask(async () =>
            {
                try
                {
                    var d = SharedViewModel.singleton.registrationData;
                    var challengeReasons = d.form.challengeReasons?.Select(u => u.code)?.ToList() ?? new List<string>();
                    var challengeReasonsText = string.Join(",", challengeReasons);
                    var x = d.form;
                    var dataToSave = new Data.Registrations.Registration(surname, otherNames, dateOfBirth, age, sex, phoneNumber, residentialAddress, residentialTown, residentialDistrictId, idTypeId, idNumber, idExpiry, fatherName, motherName, hometownAddress, hometown, hometownDistrictId, isVisuallyImpaired, isDisabled, guarantor1VoterNumber, guarantor2VoterNumber, guarantor1Name, guarantor2Name, isEstimatedAge, photo, j2kPhoto, faceTemplate, leftThumb, leftIndex, leftMiddle, leftRing, leftPinky, rightThumb, rightIndex, rightMiddle, rightRing, rightPinky, leftThumbWsq, leftIndexWsq, leftMiddleWsq, leftRingWsq, leftPinkyWsq, rightThumbWsq, rightIndexWsq, rightMiddleWsq, rightRingWsq, rightPinkyWsq, d.compositeTemplate,  x.hearingImpaired, x.leper, x.missingFingersL, x.missingFingersR, x.amputatedHandsL, x.amputatedHandsR, challengeReasonsText);
                    return await saveRegistration(SharedViewModel.singleton.user?.name ?? "", dataToSave);
                }
                catch (Exception e)
                {
                    logger.Error(e, $"Error saving registration: {e.GetBaseException().Message}");
                    return false;
                }
            }, this.WhenAnyValue(x => x.canPrint));

            //other data
            var data = SharedViewModel.singleton.registrationData;
            photo = data.picture;
            j2kPhoto = data.j2kPhoto;
            faceTemplate = data.pictureTemplate;
            var form = data.form;
            otherNames = form.otherNames;
            surname = form.surname;
            dateOfBirth = form.dateOfBirth;
            age = form.age;
            sex = form.sex;
            phoneNumber = form.phoneNumber;
            residentialAddress = form.residentialAddress;
            residentialDistrictId = form.residentialDistrictId;
            residentialRegionId = form.residentialRegionId;
            residentialTown = form.residentialTown;
            idTypeId = form.idTypeId;
            idNumber = form.idNumber;
            idExpiry = form.idExpiry;
            fatherName = form.fatherName;
            motherName = form.motherName;
            hometownAddress = form.hometownAddress;
            hometown = form.hometown;
            hometownDistrictId = form.hometownDistrictId;
            hometownRegionId = form.hometownRegionId;
            isVisuallyImpaired = form.isVisuallyImpaired;
            isDisabled = form.isDisabled;
            //set the fingers
            if (data.fingers != null)
            {
                foreach (var kvp in data.fingers)
                {
                    var template = kvp.Value.Item1;
                    var wsq = kvp.Value.Item2;
                    switch (kvp.Key)
                    {
                        case NFPosition.LeftThumb:
                            leftThumb = template;
                            leftThumbWsq = wsq;
                            break;
                        case NFPosition.LeftIndex:
                            leftIndex = template;
                            leftIndexWsq = wsq;
                            break;
                        case NFPosition.LeftMiddle:
                            leftMiddle = template;
                            leftMiddleWsq = wsq;
                            break;
                        case NFPosition.LeftRing:
                            leftRing = template;
                            leftRingWsq = wsq;
                            break;
                        case NFPosition.LeftLittle:
                            leftPinky = template;
                            leftPinkyWsq = wsq;
                            break;
                        case NFPosition.RightThumb:
                            rightThumb = template;
                            rightThumbWsq = wsq;
                            break;
                        case NFPosition.RightIndex:
                            rightIndex = template;
                            rightIndexWsq = wsq;
                            break;
                        case NFPosition.RightMiddle:
                            rightMiddle = template;
                            rightMiddleWsq = wsq;
                            break;
                        case NFPosition.RightRing:
                            rightRing = template;
                            rightRingWsq = wsq;
                            break;
                        case NFPosition.RightLittle:
                            rightPinky = template;
                            rightPinkyWsq = wsq;
                            break;
                    }
                }
            }
            //get the lookup values
            SharedViewModel.singleton.idTypes.Take(1)
               .Subscribe(xs =>
               {
                   idType = xs.FirstOrDefault(x => x.id == idTypeId)?.name ?? "";
               });
            SharedViewModel.singleton.regions
                .Take(1)
                .Subscribe(xs =>
                {
                    hometownRegion = xs.FirstOrDefault(x => x.id == hometownRegionId)?.name ?? "";
                    residentialRegion = xs.FirstOrDefault(x => x.id == residentialRegionId)?.name ?? "";
                });
            SharedViewModel.singleton.districts
                .Take(1)
                .Subscribe(xs =>
                {
                    hometownDistrict = xs.FirstOrDefault(x => x.id == hometownDistrictId)?.name ?? "";
                    residentialDistrict = xs.FirstOrDefault(x => x.id == residentialDistrictId)?.name ?? "";
                });
        }

        private async Task<bool> saveRegistration(string username, Data.Registrations.Registration data)
        {
            try
            {
                var bioTask = new BiometricTask();
                var subjectId = Data.Registrations.getNextVoterIdNumber();
                var bioEnrollResult = await bioTask.EnrollSubjectAsync(SharedViewModel.singleton.registrationData.fingerImages,
                        SharedViewModel.singleton.registrationData.faceImage, subjectId);

                if (bioEnrollResult.success)
                {
                    var result = Data.Registrations.save(username, data);
                    if (result.IsOk)
                    {
                        voterId = result.ResultValue.idNumber;
                        SharedViewModel.singleton.registrationsSoFar++;
                        SharedViewModel.singleton.registrationsToday++;
                        SharedViewModel.singleton.registrationsSinceBackup++;
                        errorMessage = "";

                        //todo: save the wsqs to file 

                        //print
                        await printCard?.Invoke(voterId);
                        //attempt creating 
                        canPrint = false;
                        //todo: what is next?
                        return true;
                    }
                    else
                    {
                        errorMessage = result.ErrorValue.GetBaseException().Message; 
                        //rollback registered biometric record
                        await bioTask.DeleteSubjectAsync(SharedViewModel.singleton.registrationData.fingerImages, voterId);
                        return false;
                    }
                }
                else
                {
                    errorMessage = bioEnrollResult.message;
                    return false;
                }


            }
            catch (Exception e)
            {
                errorMessage = e.GetBaseException().Message;
                return false;
            }
        }
    }
}
