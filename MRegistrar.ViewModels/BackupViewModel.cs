﻿using MRegistrar.Config;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
//using System.Printing;

namespace MRegistrar.ViewModels
{
    public class BackupViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        public RoutingState Router { get; }
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => () => null;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "System Backup";

        public IScreen HostScreen { get; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public bool backupDriveReady { get; set; }
        public ReactiveCommand<Unit, Unit> clearError { get; }
        public ReactiveCommand<Unit, Unit> retry { get; }


        public BackupViewModel(IScreen screen = null)
        {
            try
            {
                HostScreen = screen ?? Locator.Current.GetService<IScreen>();                
                retry = ReactiveCommand.Create(() => processBackup());
                clearError = ReactiveCommand.Create(() =>
                {
                    //errorMessage = "";
                });
                processBackup();
            }
            catch(Exception e)
            {
                errorMessage = e.Message;
            }
        }

        private void processBackup()
        {
            errorMessage = "Backup process started ...";
            string fileName = SharedViewModel.singleton.inDemoMode? "demo_registration.db" : "registration_db.db";
            string sourcePath = @".\Data";
            string sourceFile = Path.Combine(sourcePath, fileName);

            var sysBackupDrive = USBUtils.GetUSBDevices().Where(x => x.Root.Contains(Configuration.Settings.SystemBackUpDriveLetter)).FirstOrDefault();
            if (sysBackupDrive != null) File.Copy(sourceFile, Path.Combine(sysBackupDrive.Root, fileName), true);

            //todo: check that the setup drive is connected
            var usbDevices = USBUtils.GetUSBDevices().ToList();
            var backupDrive = usbDevices.Where(x => x.Label.ToLower().Contains(Configuration.Settings.BackUpDriveLabelIdentifier.ToLower())).FirstOrDefault();
            if (backupDrive != null)
            {
                //check the size of the registration db
                FileInfo f = new FileInfo(sourceFile);
                var dbSize = f.Length;

                //check the available space on the backup drive
                if (dbSize > backupDrive.AvailableSpace) throw new Exception("Backup Drive Space Error:: Please check the available space on the backup drive.");

                //backup the database onto the drive
                string destFile = Path.Combine(backupDrive.Root, fileName);
                File.Copy(sourceFile, destFile, true);
            }     

            //make a log into the database
            var user = SharedViewModel.singleton.user;
            var now = DateTime.UtcNow;
            var bkLog = new Data.BackupLogs.BackupLog(now, user?.username ?? "System", 1);
            Data.BackupLogs.save(bkLog);

            //return to the previous page
            GotoPreviousPage();
        }

        public void GotoPreviousPage()
        {
            Router.NavigateBack.Execute();
        }
    }
}
