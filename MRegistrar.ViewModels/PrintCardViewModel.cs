﻿using MRegistrar.Data;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Reactive;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
    public class PrintCardViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {
        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onBack
        {
            get
            {
                if (canGoBack) return (() => Task.CompletedTask);
                return null;
            }
        }
        Func<Task> IViewSettings.onCancel => () => Task.CompletedTask;
        bool IViewSettings.enableLogOut => false;
        bool IViewSettings.enableShutDown => false;
        bool IViewSettings.enableTransfer => false;
        bool IViewSettings.enableBackup => false;
        public string UrlPathSegment => "Print Card";
        public IScreen HostScreen { get; }

        [Reactive] public string otherNames { get; set; }
        [Reactive] public string surname { get; set; }
        [Reactive] public DateTime dateOfBirth { get; set; }
        [Reactive] public int age { get; set; }
        [Reactive] public string sex { get; set; }
        [Reactive] public string phoneNumber { get; set; }
        [Reactive] public string residentialAddress { get; set; }
        [Reactive] public string residentialTown { get; set; }
        [Reactive] public string residentialDistrict { get; set; }
        [Reactive] public string residentialRegion { get; set; }
        [Reactive] public string idType { get; set; }
        [Reactive] public string idNumber { get; set; }
        [Reactive] public DateTime idExpiry { get; set; }
        [Reactive] public string fatherName { get; set; }
        [Reactive] public string motherName { get; set; }
        [Reactive] public string hometownAddress { get; set; }
        [Reactive] public string hometown { get; set; }
        [Reactive] public string hometownDistrict { get; set; }
        [Reactive] public string hometownRegion { get; set; }
        [Reactive] public bool isVisuallyImpaired { get; set; }
        [Reactive] public bool isDisabled { get; set; }
        [Reactive] public byte[] photo { get; set; }
        [Reactive] public byte[] qrCode { get; set; }
        [Reactive] public string voterId { get; set; }
        [Reactive] public string pollingStationCode { get; set; }
        [Reactive] public string name { get; set; }
        [Reactive] public DateTime dateOfRegistration { get; set; }
        [Reactive] public string errorMessage { get; set; }
        [Reactive] public bool canPrint { get; set; }
        [Reactive] public int cardPrintCount { get; set; }
        [Reactive] public bool busy { get; set; }
        public ReactiveCommand<Unit, Unit> print { get; }
        public ReactiveCommand<Unit, Unit> voterCard { get; }
        public bool canGoBack { get; set; }
        public Action onPrintComplete { get; set; }
        public Action onPrintFailed { get; set; }
        /// <summary>
        /// References the actual function to call to do the printing
        /// </summary>
        public Action printCard { get; set; }
        [Reactive] public int generateRequests { get; set; }


        public PrintCardViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            pollingStationCode = App.config.code;

            print = ReactiveCommand.Create(() => printCard(), this.WhenAnyValue(x => x.canPrint));
        }
    }
}
