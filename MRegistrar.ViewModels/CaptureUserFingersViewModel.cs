﻿using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MRegistrar.ViewModels
{
   public  class CaptureUserFingersViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
    {

        bool IViewSettings.showTray => true;
        bool IViewSettings.showStatusSummaries => true;
        Func<Task> IViewSettings.onCancel => async () =>
        {
            try
            {
                if (onCleanup != null)
                    await onCleanup();
            }
            catch { }
        };
        bool IViewSettings.enableLogOut => true;
        bool IViewSettings.enableShutDown => true;
        bool IViewSettings.enableTransfer => true;
        Func<Task> IViewSettings.onBack => () => Task.CompletedTask;
        public string UrlPathSegment => "Capture Fingers";
        public IScreen HostScreen { get; }

        public ReactiveCommand<Unit, Unit> openRegisterROView { get; }
        public ReactiveCommand<Unit, Unit> openCaptureFaceView { get; }
        public ReactiveCommand<Unit, Unit> beginCapture { get; }
        public ReactiveCommand<Unit, Unit> endCapture { get; }
        public ReactiveCommand<Unit, Unit> restart { get; }

        public Func<Task> onStart { get; set; }
        public Func<Task> onCapture { get; set; }
        [Reactive] public bool capturing { get; set; }
        [Reactive] public bool canContinue { get; set; }
        [Reactive] public bool hasMoreFingers { get; set; }
        [Reactive] public int currentFingerPosition { get; set; }
        [Reactive] public int missingFingerCount { get; set; }
        public Func<Task> onCleanup { get; set; }

        bool IViewSettings.enableBackup => false;

        public CaptureUserFingersViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();

            openRegisterROView = ReactiveCommand.CreateFromTask(async () =>
            {
                if (onCleanup != null)
                    try { await onCleanup(); }
                    catch { }

                var source = SharedViewModel.singleton.userRole;
                if (source.Contains("RegistrationOfficer"))
                    SharedViewModel.singleton.activeViewModel = new RegisterRegistrationOfficerViewModel();
                else if (source.Contains("DataEntryClerk")) SharedViewModel.singleton.activeViewModel = new ActivateDataEntryClerksViewModel();

            }, this.WhenAnyValue(x => x.canContinue, x => x.missingFingerCount, (canContinue, missingFingerCount) => canContinue || missingFingerCount == 10));

            openCaptureFaceView = ReactiveCommand.CreateFromTask(async () =>
            {
                if (onCleanup != null)
                    try { await onCleanup(); }
                    catch { }
                SharedViewModel.singleton.activeViewModel = new CaptureUserFaceViewModel();
            }, this.WhenAnyValue(x => x.canContinue, x => x.missingFingerCount, (canContinue, missingFingerCount) => canContinue || missingFingerCount == 10));
            beginCapture = ReactiveCommand.CreateFromTask(async () => {
                capturing = true;
                await onStart();
            }, this.WhenAnyValue(x => x.capturing, x => x.missingFingerCount, (capturing, missingFingerCount) => !capturing && missingFingerCount < 10));
            beginCapture.ThrownExceptions
                .Subscribe(x =>
                {
                    //todo: needed?
                    capturing = false;
                });
            endCapture = ReactiveCommand.CreateFromTask(async () =>
            {
                capturing = false;
                await onCapture();
            }, this.WhenAnyValue(x => x.capturing, x => x.hasMoreFingers, (c, h) => c && h)); // !x.capturing && 
            restart = ReactiveCommand.Create(() =>
            {
                capturing = false;
                currentFingerPosition = 0;
            }, this.WhenAnyValue(x => x.capturing, x => x.currentFingerPosition, x => x.missingFingerCount, (c, p, mf) => !c && p > 0 && mf < 10));
        }
    }
}
