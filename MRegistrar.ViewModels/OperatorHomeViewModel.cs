﻿using MRegistrar.Data;
using MRegistrar.Services;
using MRegistrar.ViewModels.Interfaces;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.ViewModels
{
  public class OperatorHomeViewModel : ReactiveObject, IRoutableViewModel, IViewSettings
  {
    private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
    bool IViewSettings.showTray => true;
    bool IViewSettings.showStatusSummaries => true;
    Func<Task> IViewSettings.onCancel => null;
    Func<Task> IViewSettings.onBack => null;
    bool IViewSettings.enableLogOut => true;
    bool IViewSettings.enableShutDown => true;
    bool IViewSettings.enableTransfer => true;
    bool IViewSettings.enableBackup => true;
    public string UrlPathSegment => "Data Entry Clerk";

    public IScreen HostScreen { get; }
    public ReactiveCommand<Unit, Unit> registerNew { get; }
    public ReactiveCommand<Unit, Unit> registerExisting { get; }
    public ReactiveCommand<Unit, Unit> openReports { get; }
    public ReactiveCommand<Unit, Unit> openQueue { get; }
    public ReactiveCommand<Unit, Unit> openFindVoter { get; }
    public ReactiveCommand<Unit, Unit> openUpdateVoterDetails { get; }

    public OperatorHomeViewModel(IScreen screen = null)
    {
      var noPsMsg = "No polling station is set. Registration cannot happen. Kindly contact the Registration Officer";
      HostScreen = screen ?? Locator.Current.GetService<IScreen>();
      registerNew = ReactiveCommand.CreateFromTask(async () =>
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        SharedViewModel.singleton.registrationData.kind = Helpers.RegistrationData.Kind.New;
        SharedViewModel.singleton.activeViewModel = new CaptureFingersViewModel(); // NewRegistrationViewModel();//
            });
      registerExisting = ReactiveCommand.CreateFromTask(async () =>
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        SharedViewModel.singleton.registrationData.kind = Helpers.RegistrationData.Kind.Existing;
        SharedViewModel.singleton.activeViewModel = new FindByVoterIdViewModel();
      });
      openReports = ReactiveCommand.CreateFromTask(async () =>
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        SharedViewModel.singleton.activeViewModel = new ReportListViewModel();
      });
      openQueue = ReactiveCommand.CreateFromTask(async () =>
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        SharedViewModel.singleton.activeViewModel = new QueueManagementViewModel();
      });
      openFindVoter = ReactiveCommand.CreateFromTask(async () =>
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        PrintCardViewModel displayCard(string voterId)
        {
          var result = Data.Registrations.find(voterId);
          if (result.IsOk)
          {
            var x = result.ResultValue;
            return new PrintCardViewModel { age = x.estimatedAge, dateOfBirth = x.dateOfBirth, fatherName = x.fatherName, motherName = x.motherName, hometown = x.homeTown, hometownAddress = x.homeTownAddress, hometownDistrict = x.homeTownDistrict, hometownRegion = x.hometownRegion, idNumber = x.idNumber, idType = Data.Utils.getValueOrDefault("", x.idType), isDisabled = x.isDisabled, isVisuallyImpaired = x.isVisuallyImpaired, otherNames = x.otherNames, phoneNumber = x.phoneNumber, residentialAddress = x.residentialAddress, residentialDistrict = x.residentialDistrict, residentialTown = x.residentialTown, residentialRegion = x.residentialRegion, sex = x.sex, surname = x.surname, voterId = x.voterId, photo = x.photo, pollingStationCode = Data.App.config.code, name = $"{x.otherNames} {x.surname}", dateOfRegistration = x.dateRegistered, cardPrintCount = x.cardPrintCount };
          }
          else
          {
            logger.Error(result.ErrorValue, $"Error getting voter data: {voterId}");
            return new PrintCardViewModel(null);
          }
        }
        SharedViewModel.singleton.activeViewModel = new BioVerificationViewModel { identifyVoter = true, useFace = false, getNextViewModel = async voterId => displayCard(voterId), title = "Identify Voter" };
      });
      openUpdateVoterDetails = ReactiveCommand.CreateFromTask(async () => //todo: include the staff identification
      {
        if (string.IsNullOrWhiteSpace(App.config.code) || string.IsNullOrWhiteSpace(App.config.name))
        {
          await DialogService.alert(noPsMsg);
          return;
        }

        SharedViewModel.singleton.activeViewModel = new BioVerificationViewModel { identifyVoter = true, useFace = false, getNextViewModel = async id => new UpdateVoterViewModel(id), title = "Identify Voter" };
      });
    }
  }
}
