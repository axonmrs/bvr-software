﻿using MRegistrar.Config;
using MRegistrar.Data;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Text;

namespace MRegistrar.ViewModels
{
    public class ActivationViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment => "Activation";
        public IScreen HostScreen { get; }

        [Reactive] public string code { get; set; }
        [Reactive] public string passCode { get; set; }
        [Reactive] public string errorMessage { get; set; }
        public ReactiveCommand<Unit, Unit> attemptActivation { get; }

        public ActivationViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
            //setup the activation call
            attemptActivation = ReactiveCommand.Create(() => processActivation(code, passCode),
                this.WhenAnyValue(x => x.passCode, passCode => !string.IsNullOrWhiteSpace(passCode)));
        }

        private void processActivation(string code, string passCode)
        {
            //get the hash of the code and passcode
            var hash = Security.Hash.ComputeSha256Hash(code + passCode);
            //todo: get the serial number
            var serial = DeviceUtils.GetDeviceSerial();
            var device = App.getDevice(serial);
            if (device.ResultValue == null)
            {
                errorMessage = "This device cannot be setup with the data on the setup disk.";
                //delete the db
                string destFile = Path.Combine(@".\Data", "registration_db.db");
                string demoDestFile = Path.Combine(@".\Data", "demo_registration.db");
                if (File.Exists(destFile)) File.Delete(destFile);
                if (File.Exists(demoDestFile)) File.Delete(demoDestFile);
                return;
            }

            if(device.ResultValue.activation_code_hash != hash)
            {
                errorMessage = "Please check the Activation Code and Pass Code entered.";
                return;
            }

            //todo: set the device details in the appsettings table
            var obj = new List<Tuple<string, string>>
            {
                new Tuple<string, string>("activated", "true"),
                new Tuple<string, string>("device_sequence", device.ResultValue.sequence),
                new Tuple<string, string>("device_serial_number", serial)
            };
            var result = App.updateSettings(obj);
            if (result.IsOk)
            {
                errorMessage = "";
                SharedViewModel.singleton.activated = true;
                ActivityLogs.save(new ActivityLogs.ActivityLog(SharedViewModel.Activity.Activation.ToString(), $"System activation for {serial}", ""));
            }
        }
    }
}
