﻿using MRegistrar.ViewModels;
using NLog;
using RawPrint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Helpers
{
    public static class PrintHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static bool printFile(string filename)
        {
            try
            {
                var printer = new Printer();
                printer.PrintRawFile(SharedViewModel.singleton.printerName, filename, false);
                return true;
            }
            catch(Exception e)
            {
                logger.Error(e, $"Error printing [{filename}] to [{SharedViewModel.singleton.printerName}] => {e.GetBaseException().Message}");
                return false;
            }
        }
    }
}
