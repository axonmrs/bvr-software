﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Helpers
{
    /// <summary>
    /// Not working as expected
    /// </summary>
    public class RawPrinterHelper
    {
        public static bool sendFileToPrinter(string pdfFilename)
        {
            try
            {
                if (File.Exists(pdfFilename)) return sendBytesToPrinter(File.ReadAllBytes(pdfFilename));
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool sendBytesToPrinter(byte[] data)
        {
            try
            {
                var pd = new PrintDocument();
                var dp = new StringBuilder();
                int size = dp.Capacity;
                //get the printer name
                if (GetDefaultPrinter(dp, ref size))
                {
                    pd.PrinterSettings.PrinterName = dp.ToString().Trim();
                }
                var ptrUnmanagedBytes = Marshal.AllocCoTaskMem(data.Length);
                Marshal.Copy(data, 0, ptrUnmanagedBytes, data.Length);
                var success = sendBytesToPrinter(pd.PrinterSettings.PrinterName, ptrUnmanagedBytes, data.Length);
                Marshal.FreeCoTaskMem(ptrUnmanagedBytes);
                return success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static bool sendBytesToPrinter(string printerName, IntPtr ptrBytes, int length)
        {
            try
            {
                int error = 0;
                int written = 0;
                var printer = new IntPtr(0);
                var di = new DOCINFOA { pDocName = "PDF Document", pDataType = "RAW" };
                var success = false;
                if (OpenPrinter(printerName.Normalize(), out printer, IntPtr.Zero))
                {
                    if (StartDocPrinter(printer, 1, di))
                    {
                        if (StartPagePrinter(printer))
                        {
                            success = WritePrinter(printer, ptrBytes, length, out written);
                            EndPagePrinter(printer);
                        }
                        EndDocPrinter(printer);
                    }
                    ClosePrinter(printer);
                }
                if (success == false)
                {
                    error = Marshal.GetLastWin32Error();
                }
                return success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        private class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool GetDefaultPrinter(StringBuilder pszBuffer, ref int size);

    }
}
