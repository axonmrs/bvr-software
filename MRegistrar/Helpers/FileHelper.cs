﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Helpers
{
    public static class FileHelper
    {
        public static string saveReport(string name, byte[] data)
        {
            var filename = $"./temp/{name}";
            File.WriteAllBytes(filename, data);
            return filename;
        }
    }
}
