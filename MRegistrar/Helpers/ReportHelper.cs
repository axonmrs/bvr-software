﻿using Microsoft.Reporting.WinForms;
using MRegistrar.Data.Reporting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Text;

namespace MRegistrar.Helpers
{
    public static class ReportHelper
    {
        public static (List<ReportDataSource> data, string reportFile) getAllRegistrationsData()
        {
            var data = ReportData.getAllRegistrations();
            var dataSource = new ReportDataSource { Name = "Data", Value = data };
            return (new List<ReportDataSource> { dataSource }, "AllRegistrations");
        }

        public static (List<ReportDataSource> data, string reportFile) getStartOfDayStatisticsData()
        {
            var data = ReportData.getStartOfDayStatistics();
            var dataSource = new ReportDataSource { Name = "Data", Value = data };
            return (new List<ReportDataSource> { dataSource }, "StartOfDayStatistics");
        }

        public static (List<ReportDataSource> data, string reportFile) getEndOfDayStatisticsData()
        {
            var data = ReportData.getEndOfDayStatistics();
            var dataSource = new ReportDataSource { Name = "Data", Value = data };
            return (new List<ReportDataSource> { dataSource }, "EndOfDayStatistics");
        }

        public static byte[] generateReport(Func<(List<ReportDataSource> data, string reportFile)> getData)
        {
            var ret = getData();
            return generateReport(ret.reportFile, ret.data);
        }

        public static byte[] generateReport(string rdlcFile, object data, string srcName = "Data")
        {
            var ds = new ReportDataSource { Name = srcName, Value = data };
            return generateReport(rdlcFile, new List<ReportDataSource> { ds });
        }

        public static byte[] generateReport(string rdlcFile, List<ReportDataSource> data)
        {
            var file = Path.Combine("Reports", $"{rdlcFile}.rdlc");
            Warning[] warnings;
            string[] streams;
            var report = new LocalReport
            {
                ReportPath = Path.GetFullPath(file),
                EnableExternalImages = true
            };
            if (data != null)
                foreach (var entry in data)
                    report.DataSources.Add(entry);
            string mimeType;
            string encoding;
            string extension;
            var bytes = report.Render("PDF",
                @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                out mimeType, out encoding, out extension, out streams, out warnings);
            return bytes;
        }
        
        public static (List<ReportDataSource> data, string reportFile) getQueueData(int rowCount, int count, int startNumber, DateTime date)
        {
            var data =
                Enumerable.Range(0, count)
                .Select(x => {
                    var column = 1 + x % rowCount;
                    var row = 1 + x / rowCount;
                    var index = startNumber + x;
                    var title = $"ECR-{date:yyyy-MM-dd}";
                    return new { column, row, index, title };
                })
                .ToArray();
            var dataSource = new ReportDataSource { Name = "Data", Value = data };
            return (new List<ReportDataSource> { dataSource }, "Queue");
        }
    }
}
