﻿using MRegistrar.Config;
using MRegistrar.ViewModels;
using Newtonsoft.Json;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace MRegistrar
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            if(Config.Configuration.Settings.UseSdCard)InitialSetup();

            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
            Locator.CurrentMutable.RegisterViewsForViewModels(typeof(StartViewModel).Assembly);
        }

        //copy the database to the file system and set it up
        public void InitialSetup()
        {
            var sdCard = new USBDevice(0,"","",0,"","");
            string fileName = "registration_db.db";
            string demoFileName = "demo_registration.db";
            string targetPath = @".\Data";
            string destFile = Path.Combine(targetPath, fileName);
            string demoDestFile = Path.Combine(targetPath, demoFileName);

            //check that the file exists
            if (File.Exists(destFile)) return;


            //todo: check that the setup drive is connected
            var usbDevices = USBUtils.GetUSBDevices().ToList();
            usbDevices = usbDevices.Where(x => x.Label.ToLower().Contains(Config.Configuration.Settings.SetUpDriveLabelIdentifier.ToLower())).ToList();
            if (!usbDevices.Any()) throw new Exception("Please insert your setup drive and start again.");
            if(usbDevices.Count() > 1) throw new Exception("Please ensure that you have only one setup drive inserted");
            sdCard = usbDevices.First();

            //check that the setup drive contains the registration database
            string sourceFile = Path.Combine(sdCard.Root, fileName);
            if (!File.Exists(sourceFile)) throw new Exception("The setup drive does not contain a registration db file.");

            //copy the database to the local file system, replace any existing database file
            Directory.CreateDirectory(targetPath);
            File.Copy(sourceFile, destFile, true);
            File.Copy(sourceFile, demoDestFile, true);
        }

    }
}
