﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace MRegistrar
{
    public class IdleDetector
    {
        private readonly DispatcherTimer _activityTimer;
        //private Point _inactiveMousePosition = new Point(0, 0);

        //private IInputElement _inputElement;
        //private int _idleTime = 10;

        public event EventHandler IsIdle;

        public IdleDetector(int idleTime)
        {
            //_inputElement = inputElement;
            InputManager.Current.PreProcessInput += OnActivity;
            _activityTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(idleTime), IsEnabled = true };
            _activityTimer.Tick += OnInactivity;
        }

        public void ChangeIdleTime(int newIdleTime)
        {
            //_idleTime = newIdleTime;

            _activityTimer.Stop();
            _activityTimer.Interval = TimeSpan.FromSeconds(newIdleTime);
            _activityTimer.Start();
        }

        void OnInactivity(object sender, EventArgs e)
        {
            _activityTimer.Stop();
            IsIdle?.Invoke(this, new EventArgs());
        }

        void OnActivity(object sender, PreProcessInputEventArgs e)
        {
            switch (e.StagingItem.Input)
            {
                case MouseEventArgs _:
                case KeyboardEventArgs _:
                    _activityTimer.Stop();
                    _activityTimer.Start();
                    break;
            }
        }
    }
}
