﻿using MRegistrar.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Dialogs
{
    public class AlertDialogViewModel : INotifyPropertyChanged
    {
        private string _message;
        public string message
        {
            get { return _message; }
            set { this.MutateVerbose(ref _message, value, raisePropertyChanged()); }
        }
        private string _okLabel = "OK";
        public string okLabel
        {
            get { return _okLabel; }
            set { this.MutateVerbose(ref _okLabel, value, raisePropertyChanged()); }
        }
        private string _cancelLabel = "Cancel";
        public string cancelLabel
        {
            get { return _cancelLabel; }
            set { this.MutateVerbose(ref _cancelLabel, value, raisePropertyChanged()); }
        }
        private bool _showCancel = true;
        public bool showCancel
        {
            get { return _showCancel; }
            set { this.MutateVerbose(ref _showCancel, value, raisePropertyChanged()); }
        }
        private DialogService.DialogKind _kind = DialogService.DialogKind.Info;
        public DialogService.DialogKind kind
        {
            get { return _kind; }
            set
            {
                this.MutateVerbose(ref _kind, value, raisePropertyChanged());
                switch (_kind)
                {
                    case DialogService.DialogKind.Info:
                        icon = "\uf05a";
                        iconColor = "#3085d6";
                        break;
                    case DialogService.DialogKind.Warning:
                        icon = "\uf06a";
                        iconColor = "#f8bb86";
                        break;
                    case DialogService.DialogKind.Error:
                        icon = "\uf057";
                        iconColor = "#f27474";
                        break;
                    default:
                        icon = "\uf06a";
                        iconColor = "#3085d6";
                        break;
                }
            }
        }

        private string _icon;
        public string icon
        {
            get { return _icon; }
            set { this.MutateVerbose(ref _icon, value, raisePropertyChanged()); }
        }

        private string _iconColor;
        public string iconColor
        {
            get { return _iconColor; }
            set { this.MutateVerbose(ref _iconColor, value, raisePropertyChanged()); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private Action<PropertyChangedEventArgs> raisePropertyChanged() => args => PropertyChanged?.Invoke(this, args);
    }
}
