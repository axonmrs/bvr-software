﻿using MaterialDesignThemes.Wpf;
using MRegistrar.Controls;
using MRegistrar.Dialogs;
using MRegistrar.Services;
using MRegistrar.ViewModels;
using NLog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MRegistrar
{
    public class MainViewModel : ReactiveObject, IScreen
    {
        public RoutingState Router { get; }
        public AltRouter altRouter { get; }
        [Reactive] public bool devicesOk { get; set; }
        [Reactive] public Func<Task> onCancel { get; set; }
        [Reactive] public Func<Task> onBack { get; set; }
        [Reactive] public bool showTray { get; set; }
        [Reactive] public bool showStatus { get; set; }
        [Reactive] public int viewStackCount { get; internal set; }
        /// <summary>
        /// Updated when the goBack action is initiated
        /// </summary>
        //[Reactive] public long backRequestCount { get; private set; }
        [Reactive] public bool dialogVisible { get; private set; }
        public ReactiveCommand<Unit, Unit> goBack { get; }
        public ReactiveCommand<Unit, Unit> cancel { get; }
        public ReactiveCommand<Unit, Unit> showDialog { get; }
        public ReactiveCommand<Unit, Unit> closeDialog { get; }
        public ReactiveCommand<Unit, Unit> logOut { get; set; }
        public ReactiveCommand<Unit, Unit> shutDown { get; set; }
        public ReactiveCommand<Unit, Unit> transfer { get; set; }
        public ReactiveCommand<Unit, Unit> backup { get; set; }
        [Reactive] public TimeSpan timeLeft { get; private set; }

        [Reactive] public bool enableLogOut { get; set; }
        [Reactive] public bool enableShutdown { get; set; }
        [Reactive] public bool enableTransfer { get; set; }
        [Reactive] public bool enableBackup { get; set; }
        public string username { [ObservableAsProperty]get; }
        public Action close { get; set; }
        private Logger logger = LogManager.GetCurrentClassLogger();

        public MainViewModel()
        {
            Router = new RoutingState();
            altRouter = new AltRouter();
            //dialogs
            DialogService.confirmFn = async (string message, DialogService.DialogKind kind, string okLabel, string cancelLabel) =>
            {
                var viewModel = new AlertDialogViewModel { message = message, cancelLabel = cancelLabel, okLabel = okLabel, showCancel = true, kind = kind };
                var view = new AlertDialog { DataContext = viewModel };
                return (bool)await DialogHost.Show(view);
            };
            DialogService.alertFn = async (string message, DialogService.DialogKind kind, string okLabel) =>
            {
                var viewModel = new AlertDialogViewModel { message = message, okLabel = okLabel, showCancel = false, kind = kind };
                var view = new AlertDialog { DataContext = viewModel };
                return (bool)await DialogHost.Show(view);
            };
            goBack = ReactiveCommand.CreateFromTask(runGoBack, this.WhenAnyValue(x => x.onBack).Select(x => x != null));
            cancel = ReactiveCommand.CreateFromTask(runCancel, this.WhenAnyValue(x => x.onCancel).Select(x => x != null));
            showDialog = ReactiveCommand.Create(() => { dialogVisible = true; });
            closeDialog = ReactiveCommand.Create(() => { dialogVisible = false; });
            logOut = ReactiveCommand.Create(() =>
            {
                dialogVisible = false;
                if (SharedViewModel.singleton.user != null)
                    Data.ActivityLogs.save(new Data.ActivityLogs.ActivityLog(SharedViewModel.Activity.Logout.ToString(), $"{SharedViewModel.singleton.user.name} logged out of the system", SharedViewModel.singleton.user.username));
              SharedViewModel.singleton.user = new Data.Model.User("", "", "", "", true, true);
            });
            createHomeView();

            //add mapping for username
            SharedViewModel.singleton.WhenAnyValue(x => x.user)
                .Select(x => x?.name ?? "")
                .ToPropertyEx(this, x => x.username);
            Observable.Interval(TimeSpan.FromSeconds(1))
                .Select(x => SharedViewModel.singleton.endTime - DateTime.Now)
                .Select(x => //hack to not create another observable
                {
                    if (SharedViewModel.singleton.canRegister && x.TotalSeconds < 0)
                    {
                        SharedViewModel.singleton.canRegister = false;
                    }
                    return x;
                })
                .Subscribe(x => timeLeft = x);

            shutDown = ReactiveCommand.CreateFromTask(async () =>
            {
                var proceed = await DialogService.confirm("Do you want to shut down?", DialogService.DialogKind.Error);
                if (!proceed) return;
                processSystemShutdown();
            });

            transfer = ReactiveCommand.Create(() => processTransfer());

            backup = ReactiveCommand.Create(() => processBackup());


        }

        private void createHomeView()
        {
            SharedViewModel.singleton.activeViewModel = new StartViewModel();
        }

        public async Task runCancel()
        {
            if (onCancel != null) await onCancel();
            IDisposable d = null;
            d = SharedViewModel.singleton.WhenAnyValue(x => x.user)
                .Take(1)
                .Subscribe(x =>
                {
                    if (x == null)
                    {
                        //todo: go to the right page. For now, do nothing
                        return;
                    }
                    switch (x.type)
                    {
                        case "DataEntryClerk":
                        case "Operator":
                            SharedViewModel.singleton.activeViewModel = new OperatorHomeViewModel();
                            break;
                        case "RegistrationOfficer":
                        case "PresidingOfficer":
                            SharedViewModel.singleton.activeViewModel = new PresidingOfficerHomeViewModel();
                            break;
                        case "DistrictOfficer":
                            SharedViewModel.singleton.activeViewModel = new DistrictOfficerHomeViewModel();
                            break;
                        default:
                            break;
                    }
                    if (d != null) d.Dispose();
                });
        }
        public async Task runGoBack()
        {
            if (onBack != null) await onBack();
            var viewModel = altRouter.Back();
            logger.Trace($"Going back to {viewModel?.GetType()?.Name}");
            SharedViewModel.singleton.activeViewModel = viewModel;
        }

        private void processSystemShutdown()
        {
            switch (SharedViewModel.singleton.user?.type)
            {
                case null:
                case "DistrictOfficer":
                case "RegistrationOfficer":
                case "PresidingOfficer":
                    close();
                    return;
            }
            var vm = new ConfirmShutdownViewModel();
            var errors = string.Empty;
            //check if all data has been backed up
            vm.noPendingBackups = Data.Registrations.hasPendingBackups();
            if (!vm.noPendingBackups)
            {
                errors += "There are pending backups. \n";
            }

            //then check if all registrations have been transmitted
            vm.noPendingTransmissions = Data.Registrations.hasPendingTransfers();
            if (!vm.noPendingTransmissions)
            {
                errors += "There are pending registrations that haven't been transmitted yet. \n";
            }

            var now = DateTime.Now;
            //then check if start of day report has been printed
            var sodRes = Data.Registrations.hasPrintedReport(ReportViewModel.ReportTypes.StartOfDayStatistics.ToString(), now.Date);
            vm.printedStartOfDayReport = sodRes.ResultValue;
            if (!vm.printedStartOfDayReport)
            {
                errors += "You have to print the start of day report. \n";
            }

            //then check if end of day report has been printed
            var eodRes = Data.Registrations.hasPrintedReport(ReportViewModel.ReportTypes.EndOfDayStatistics.ToString(), now.Date);
            vm.printedEndOfDayReport = eodRes.ResultValue;
            if (!vm.printedEndOfDayReport)
            {
                errors += "You have to print the end of day report. \n";
            }

            if (errors != string.Empty)
            {
                vm.errorMessage = errors;
                vm.error = errors;
            }
            SharedViewModel.singleton.activeViewModel = vm;
        }

        private void processTransfer()
        {
            SharedViewModel.singleton.activeViewModel = new TransferViewModel();
        }

        private void processBackup()
        {
            SharedViewModel.singleton.activeViewModel = new BackupViewModel();
        }
    }
}
