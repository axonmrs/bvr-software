﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for StartView.xaml
    /// </summary>
    public partial class StartView : ReactiveUserControl<StartViewModel>
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public StartView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                SharedViewModel.singleton.WhenAnyObservable(x => x.diagnosisResults)
                    .BindTo(this, view => view.results.ItemsSource)
                    .DisposeWith(d);
            });
        }
    }
}
