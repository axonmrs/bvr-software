﻿using MRegistrar.BioService;
using MRegistrar.BioService.Laxton;
using MRegistrar.Services;
using MRegistrar.ViewModels;
using Neurotec.Biometrics;
using NLog;
using ReactiveUI;
using System;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for CaptureFaceView.xaml
    /// </summary>
    public partial class CaptureFaceView : ReactiveUserControl<CaptureFaceViewModel>
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        FaceState bioState;
        public CaptureFaceView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    Laxton.cameraFlashOff();
                    bioState?.cleanUp();
                    //can do cleanup here
                    return;
                }

                capture.Visibility = Config.Configuration.Settings.UseICAO ? Visibility.Collapsed : Visibility.Visible;
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                //picture display
                this.OneWayBind(ViewModel, x => x.capturing, x => x.faceViewContainer.Visibility, x => x ? Visibility.Visible : Visibility.Collapsed).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.capturing, x => x.captured.Visibility, x => !x ? Visibility.Visible : Visibility.Collapsed).DisposeWith(d);

                //icao
                ViewModel.WhenAnyObservable(x => x.icao.warningResults)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .BindTo(this, v => v.warnings.ItemsSource)
                    .DisposeWith(d);

                this.BindCommand(ViewModel, vm => vm.openConfirmView, v => v.next).DisposeWith(d);

                bioState = new FaceState { client = State.singleton.client };
                bioState.init();
                Observable.FromEventPattern<FaceStatePropertyChangedEventArgs>(bioState, "onPropertyChange")
                    .Select(x => x.EventArgs)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(async args =>
                    {
                        switch (args.property)
                        {
                            case FaceProperty.BeginCapture:
                                faceView.Face = ViewModel.icao.face = null;
                                infoLabel.Content = "Begin Capture...";
                                infoLabel.Foreground = Brushes.Blue;
                                break;
                            case FaceProperty.NewFaceForCapture:
                                faceView.Face = ViewModel.icao.face = (NFace)args.value;
                                break;
                            case FaceProperty.Capturing:
                                var value = (bool)args.value;
                                if (value)
                                {
                                    infoLabel.Content = "Capturing...";
                                    infoLabel.Foreground = Brushes.Blue;
                                }
                                break;
                            case FaceProperty.CapturingFailed:
                                infoLabel.Content = (args.value as Exception)?.GetBaseException()?.Message;
                                infoLabel.Foreground = Brushes.Red; //clean up?
                                break;
                            case FaceProperty.CaptureComplete:
                                await ViewModel.endCapture.Execute();
                                break; //clean up?
                            default:
                                break;
                        }
                    })
                    .DisposeWith(d);

                this.BindCommand(ViewModel, x => x.beginCapture, x => x.start).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.endCapture, x => x.capture).DisposeWith(d);

                ViewModel.onStart = async () =>
                {
                    Laxton.cameraFlashOn();
                    //bioState.active = true;
                    await bioState.beginCapture();
                };
                ViewModel.onCapture = async () =>
                {
                    Laxton.cameraFlashOff();
                    var bytes = bioState.faceJpegAsBytes();
                    var template = bioState.faceTemplate();
                    var faceNimage = bioState.faceAsNimage();

                    //create the stream
                    var stream = new MemoryStream(bytes);
                    stream.Seek(0, SeekOrigin.Begin);
                    //create the image
                    var image = new BitmapImage();
                    image.BeginInit();
                    image.StreamSource = stream;
                    image.EndInit();
                    //set the new image
                    captured.Source = image;
                    //store the image
                    var face = bioState.capturedFace();
                    if (face.Status == NBiometricStatus.Ok)
                    {
                        infoLabel.Content = "Processing";

                        SharedViewModel.singleton.registrationData.picture = bytes;
                        SharedViewModel.singleton.registrationData.j2kPhoto = bioState.faceJ2KAsBytes();
                        SharedViewModel.singleton.registrationData.pictureTemplate = template;
                        SharedViewModel.singleton.registrationData.faceImage = faceNimage;
                        ViewModel.hasImage = true;

                        await VerifyFingerAndFaceAsync();

                        infoLabel.Content = "Press next to continue";
                        infoLabel.Foreground = Brushes.Green;
                    }
                    else
                    {
                        ViewModel.hasImage = false;
                        infoLabel.Content = face.Status.ToString();
                        infoLabel.Foreground = Brushes.DarkRed;
                    }
                };
                ViewModel.onCleanup = async () =>
                {
                    try
                    {
                        Laxton.cameraFlashOff();
                        bioState.cleanUp();
                        await Task.CompletedTask;
                    }
                    catch(Exception e)
                    {
                        logger.Error(e, $"Cleanup Error: {e.GetBaseException().Message}");
                    }
                };
                ViewModel.beginCapture.Execute();
            });
        }

        public async Task VerifyFingerAndFaceAsync()
        {
            try
            {
                logger.Trace("Verifying Finger and Face");
                var bioTask = new BiometricTask();
                var task = await bioTask.IdentifySubjectAsync(SharedViewModel.singleton.registrationData.fingerImages,
                  SharedViewModel.singleton.registrationData.faceImage);
                SharedViewModel.singleton.registrationData.compositeTemplate = task.createdSubject.GetTemplate().Save().ToArray();

                if (task.success)
                {
                    logger.Trace($"Finger/Face found: {task.voterId}");
                    //MessageBox.Show($"Voter already registered with Voter Number: {task.voterId}", "Identification Result", MessageBoxButton.OK, MessageBoxImage.Error);

                    await DialogService.alert($"Voter already registered with Voter Number: {task.voterId}", DialogService.DialogKind.Error);

          if (!Config.Configuration.Settings.AllowDuplicateOverride)
                        SharedViewModel.singleton.activeViewModel = new OperatorHomeViewModel();
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error,ex,"VerifyFingerAndFaceAsync");
            }



        }
    }
}
