﻿using MRegistrar.BioService;
using MRegistrar.ViewModels;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Gui;
using NLog;
using ReactiveUI;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using MRegistrar.Data;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for BioVerificationView.xaml
    /// </summary>
    public partial class BioVerificationView : ReactiveUserControl<BioVerificationViewModel>
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        FingerState fingerState;

        public BioVerificationView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.OneWayBind(ViewModel, x => x.title, v => v.headerText.Text).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.useFaceIdentification, v => v.useFace).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.useFingerIdentification, v => v.useFinger).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.useFace)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(useFace =>
                    {
                        var faceBrush = useFace ? Brushes.Green : Brushes.Gray;
                        var fingerBrush = useFace ? Brushes.Gray : Brushes.Green;
                        useFaceIcon.Foreground = useFaceText.Foreground = faceBrush;
                        useFingerIcon.Foreground = useFingerText.Foreground = fingerBrush;
                    })
                    .DisposeWith(d);
                //Setup.recreateClient();
                //State.singleton.increaseClientUsage();
                fingerState = new FingerState { client = State.singleton.client };
                _ = Observable.FromEventPattern<FingerStatePropertyChangedEventArgs>(fingerState, "onPropertyChange")
                    .Select(x => x.EventArgs)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(async args =>
                    {
                        switch (args.property)
                        {
                            case FingerProperty.BeginCapture:
                                infoLabel.Content = "Begin Capture...";
                                infoLabel.Foreground = Brushes.Blue;
                                break;
                            case FingerProperty.SubjectFingerInitialized:
                                var finger = (NFinger)args.value;
                                fingerView.ShownImage = ShownImage.Original;
                                fingerView.Finger = finger;
                                break;
                            case FingerProperty.Capturing:
                                infoLabel.Content = "Capturing...";
                                infoLabel.Foreground = Brushes.Blue;
                                break;
                            case FingerProperty.CaptureStatus:
                                var status = (NBiometricStatus)args.value;
                                switch (status)
                                {
                                    case NBiometricStatus.Ok:
                                        infoLabel.Content = "Finger Captured";
                                        break;
                                    case NBiometricStatus.TooFewObjects:
                                    case NBiometricStatus.ObjectNotFound:
                                        infoLabel.Content = "Waiting for Finger...";
                                        break;
                                    case NBiometricStatus.Canceled:
                                    case NBiometricStatus.None:
                                        infoLabel.Content = status.ToString();
                                        break;
                                    case NBiometricStatus.BadObject:
                                        infoLabel.Content = "Try again";
                                        await ViewModel.beginFinderCapture.Execute();
                                        break;
                                    default:
                                        logger.Trace($"Skipped Status: {status}");
                                        infoLabel.Content = status.ToString();
                                        break;
                                }
                                infoLabel.Foreground = Brushes.Blue;
                                break;
                            case FingerProperty.CaptureComplete:
                                ViewModel.capturing = false;
                                fingerState.active = false;
                                var result = await (ViewModel.identifyVoter ? identifyVoter(fingerState.subject) : identifyStaff(fingerState.subject));
                                if (result.successful)
                                {
                                    //todo: clean up
                                    SharedViewModel.singleton.activeViewModel = await ViewModel.getNextViewModel(result.id);
                                    cleanUp();
                                }
                                else
                                {
                                    errorLabel.Content = "No Match Found"; //message
                                    ViewModel.beginFinderCapture.Execute();
                                }
                                break;
                            case FingerProperty.CaptureFailed:
                                logger.Error($"Capture Failed => {(args.value as Exception)?.ToString() }");
                                infoLabel.Content = (args.value as Exception)?.GetBaseException()?.Message;
                                infoLabel.Foreground = Brushes.DarkRed;
                                ViewModel.capturing = false;
                                fingerState.active = false;
                                break;
                            case FingerProperty.CaptureFailedStatus:
                                break;
                        }
                    })
                    .DisposeWith(d);
                ViewModel.onStart = async () =>
                {
                    fingerState.active = true;
                    //errorLabel.Content = "";
                    await fingerState.beginCapture(NFPosition.UnknownFourFingers, null);
                };

                //attempt capture
                ViewModel.beginFinderCapture.Execute(); //todo: depends on capture type
            });
        }

        private async Task<(bool successful, bool retry, string id)> identifyStaff(NSubject subject)
        {
            try
            {
                    var client = State.singleton.staffClient;
                    var status = await client.IdentifyAsync(subject);
                    switch (status)
                    {
                        case NBiometricStatus.Ok:
                            foreach(var result in subject.MatchingResults)
                            {
                                var res = Users.findById(long.Parse(result.Id));
                                if (res.IsOk)
                                {
                                    var details = res.ResultValue;
                                    SharedViewModel.singleton.user = new Model.User(details.name, details.password_hash, details.type, details.username,details.is_active, details.selected);
                                    ActivityLogs.save(new ActivityLogs.ActivityLog(SharedViewModel.Activity.Login.ToString(), $"{details.name} logged in as {details.type}", details.username));
                                    return (true, false, "Match Found");
                                }
                                else
                                {
                                    infoLabel.Foreground = Brushes.Red;
                                    infoLabel.Content = "Login falied.No User found with given ID";
                                    return (false, false, "Login falied.No User found with given ID");
                                }
                            }
                        return (false, false, "Login falied.No User found with given ID");
                        case NBiometricStatus.MatchNotFound:
                            infoLabel.Content = "No Match Found";
                            return (false, false, "No Match Found");
                        default:
                            return (false, false, $"Matching failed with status {status}");
                    }
                
            }
            catch (Exception e)
            {
                return (false, true, $"Error: {e.GetBaseException().Message}"); //todo: change
            }
        }

        private async Task<(bool successful, bool retry, string id)> identifyVoter(NSubject subject)
        {
            try
            {
                var bioTask = new BiometricTask();
                var task = await bioTask.IdentifySubjectAsync(subject);
                if (task.success) return (true, false, task.voterId);
                return (false, true, $"No voter found");
            }
            catch (Exception e)
            {
                logger.Error(e, $"Error identifying voter: {e.GetBaseException().Message}");
                return (false, true, $"Error verifying: {e.GetBaseException().Message}");
            }
        }

        private void cleanUp()
        {
            try
            {
                if (fingerState != null) {
                    fingerState.cleanUp();
                    fingerState = null;
                }
                //todo: clean up finger state
            } 
            catch(Exception e)
            {
                logger.Error(e, $"Error cleaning up: {e.GetBaseException().Message}");
            }
        }
    }
    
}
