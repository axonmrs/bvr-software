﻿using MRegistrar.Helpers;
using MRegistrar.ViewModels;
using MRegistrar.ViewModels.Helpers;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static MRegistrar.Helpers.PrintHelper;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ConfirmRegistrationView.xaml
    /// </summary>
    public partial class PrintCardView : ReactiveUserControl<PrintCardViewModel>
    {
        public PrintCardView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                //qr code
                var qrCodeData = $@"
                    {{
                    kind:""VoterCard"",
                    id:{ViewModel.voterId},
                    name:""{ViewModel.surname} {ViewModel.otherNames}"",
                    sex:""{ViewModel.sex}"",
                    dob:{ViewModel.dateOfBirth:yyyyMMdd},
                    issueDate:{ViewModel.dateOfRegistration.ToString("yyyyMMdd")},
                    }}
                    ".Trim();
                ViewModel.qrCode = Utilities.createQRCode(qrCodeData, 15);

                this.BindCommand(ViewModel, x => x.print, x => x.print).DisposeWith(d);


                ViewModel.WhenAnyValue(x => x.generateRequests)
                    .Where(x => x > 0) //skip the first time it is set so we don't generate the report
                    .Subscribe(async _ => await generate())
                    .DisposeWith(d);

                ViewModel.canPrint = true;
                ViewModel.generateRequests++;
                ViewModel.printCard = () =>
                {
                    try
                    {
                        ViewModel.busy = true;
                        var printOk = printFile(pdfViewer.pdfPath.Split('?')[0]);
                        //assume printing worked and move on
                        var result = Data.Registrations.updateCardPrintCount(ViewModel.voterId, ViewModel.cardPrintCount + 1);
                        ViewModel.onPrintComplete?.Invoke();
                    }
                    catch (Exception e)
                    {
                        ViewModel.onPrintFailed?.Invoke();
                    }
                    finally
                    {
                        ViewModel.busy = false;
                    }
                    //Utilities.printCard(voterCard);
                    ViewModel.canPrint = false;
                };
            });
        }

        private async Task generate()
        {
            try
            {
                ViewModel.busy = true;
                var bytes = ReportHelper.generateReport("CardPage", new List<PrintCardViewModel> { ViewModel });
                var filename = FileHelper.saveReport("card.pdf", bytes);
                pdfViewer.pdfPath = $"{filename}?{DateTime.Now.Ticks}";
                ViewModel.canPrint = true;
            }
            catch (Exception e)
            {

            }
            finally
            {
                ViewModel.busy = false;
            }
        }
    }
}

