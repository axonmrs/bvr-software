﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ActivationView.xaml
    /// </summary>
    public partial class ActivationView : ReactiveUserControl<ActivationViewModel>
    {
        public ActivationView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                //local
                this.Bind(ViewModel, x => x.code, x => x.activationCode.Text).DisposeWith(d);
                this.Bind(ViewModel, x => x.passCode, x => x.passCode.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.IsActive,
                    x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.Message.Content).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.attemptActivation, x => x.activate).DisposeWith(d);
                Observable.FromEventPattern(toast.Message, "ActionClick")
                    .Select(_ => "")
                    .BindTo(ViewModel, x => x.errorMessage)
                    .DisposeWith(d);
            });
        }
    }
}
