﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ReportListView.xaml
    /// </summary>
    public partial class ReportListView : ReactiveUserControl<ReportListViewModel>
    {
        public ReactiveCommand<ReportViewModel.ReportTypes, Unit> openReport { get; }
        public ReportListView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.BindCommand(ViewModel, x => x.openReport, view => view.openAllRegistrationsReport, Observable.Return(ReportViewModel.ReportTypes.AllRegistrations))
                    .DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openReport, view => view.openStartOfDayStatisticsReport, Observable.Return(ReportViewModel.ReportTypes.StartOfDayStatistics))
                    .DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openReport, view => view.openEndOfDayStatisicsReport, Observable.Return(ReportViewModel.ReportTypes.EndOfDayStatistics))
                    .DisposeWith(d);
            });
        }
    }
}
