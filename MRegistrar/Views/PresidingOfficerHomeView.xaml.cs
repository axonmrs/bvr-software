﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for PresidingOfficerHomeView.xaml
    /// </summary>
    public partial class PresidingOfficerHomeView : ReactiveUserControl<PresidingOfficerHomeViewModel>
    {
        public PresidingOfficerHomeView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                
                this.BindCommand(ViewModel, x => x.openDataEntryClerks, x => x.openDataEntryClerksView).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openReports, x => x.reports).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openRegistrationOfficers, x => x.openRegistrationOfficersView).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openRegistrationSettings, x => x.openRegistrationSettingsView).DisposeWith(d);
            });
        }
    }
}
