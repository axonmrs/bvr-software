﻿using MRegistrar.BioService;
using MRegistrar.BioService.Controls;
using MRegistrar.ViewModels;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Biometrics.Gui;
using Neurotec.Images;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;


namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for CaptureUserFingersView.xaml
    /// </summary>
    public partial class CaptureUserFingersView : ReactiveUserControl<CaptureUserFingersViewModel>
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        FingerState bioState;
        //int currentFingerPosition = 0;
        NFPosition[] fingers; //todo: see how this ties in with the subject
        Dictionary<NFPosition, (byte[], byte[])> extracts;
        Dictionary<NFPosition, NImage> fingerImgLst;
        //NBiometricClient thisClient;
        public CaptureUserFingersView()
        {
            InitializeComponent();
            //thisClient = CreateNewClient();
            //bioState = new FingerState { client = State.singleton.client };
            fingerImgLst = new Dictionary<NFPosition, NImage>() { };
            extracts = new Dictionary<NFPosition, (byte[], byte[])>();
            this.WhenActivated(async d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.BindCommand(ViewModel, vm => vm.openCaptureFaceView, v => v.next).DisposeWith(d);

                ////??? temp. Reconsider after fix
                //while (State.singleton.client == null)
                //{
                //Setup.recreateClient();
                //    await Task.Delay(1000);
                //}
                fingers = State.singleton.canCaptureSlaps ? Common.slaps : Common.fingers;
                //temp
                bioState = new FingerState { client = State.singleton.client };
                //bioState.init();
                _ = Observable.FromEventPattern<FingerStatePropertyChangedEventArgs>(bioState, "onPropertyChange")
                    .Select(x => x.EventArgs)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(async args =>
                    {
                    switch (args.property)
                    {
                        case FingerProperty.BeginCapture:
                            infoLabel.Content = "Begin Capture...";
                            infoLabel.Foreground = Brushes.Blue;
                            break;
                        case FingerProperty.SubjectFingerInitialized:
                            var finger = (NFinger)args.value;
                            fingerView.ShownImage = ShownImage.Original;
                            fingerView.Finger = finger;
                            break;
                        case FingerProperty.Capturing:
                            infoLabel.Content = "Capturing...";
                            infoLabel.Foreground = Brushes.Blue;
                            break;
                        case FingerProperty.CaptureStatus:
                            var status = (NBiometricStatus)args.value;
                            switch (status)
                            {
                                case NBiometricStatus.Ok:
                                    infoLabel.Content = "Processing... Please wait.";
                                    break;
                                case NBiometricStatus.TooFewObjects:
                                case NBiometricStatus.ObjectNotFound:
                                    infoLabel.Content = "Waiting for Finger...";
                                    break;
                                //case NBiometricStatus.BadObject: 
                                //    ViewModel.currentFingerPosition--;
                                //    infoLabel.Content = "Try again";
                                //    await ViewModel.beginCapture.Execute();
                                //    break;
                                case NBiometricStatus.Canceled:
                                case NBiometricStatus.None:
                                    infoLabel.Content = status.ToString();
                                    break;
                                case NBiometricStatus.BadObject:
                                    ViewModel.currentFingerPosition--;
                                    infoLabel.Content = "Try again";
                                    await ViewModel.beginCapture.Execute();
                                    break;
                                default:
                                    logger.Trace($"Skipped Status: {status}");
                                    infoLabel.Content = status.ToString();
                                    break;
                            }
                            infoLabel.Foreground = Brushes.Blue;
                            break;
                        case FingerProperty.CaptureComplete:
                            ViewModel.capturing = false;
                            //extract
                            var fingersAndTemplates = bioState.getFingerAndTemplates();
                            fingersAndTemplates
                                .ForEach(x =>
                                {
                                    extracts[x.position] = (x.template, x.wsq);
                                });
                            fingerImgLst.Merge(fingersAndTemplates.ToDictionary(x => x.position, x => x.fingerImage));
                            capturedFingers.MissingPositions = fingerImgLst.Keys.ToArray();

                            //setup
                            var hasMoreFingers = ViewModel.currentFingerPosition + 1 < fingers.Length;
                            infoLabel.Content = hasMoreFingers
                                ? "Finger Captured. Move to next finger(s)"
                                : "";
                            infoLabel.Foreground = Brushes.Green;
                            ViewModel.hasMoreFingers = hasMoreFingers;
                            ViewModel.canContinue = !hasMoreFingers;
                                if (!hasMoreFingers)
                                {
                                    bioState.active = false;
                                    SharedViewModel.singleton.staffData.Fingers = fingerImgLst;
                                }
                                else
                                    {
                                        //ViewModel.endCapture.Execute();
                                        await ViewModel.onCapture();
                                    }
                                break;
                            case FingerProperty.CaptureFailed:
                                logger.Error($"Capture Failed => {(args.value as Exception)?.ToString() }");
                                infoLabel.Content = (args.value as Exception)?.GetBaseException()?.Message;
                                infoLabel.Foreground = Brushes.DarkRed;
                                ViewModel.capturing = false;
                                break;
                            case FingerProperty.CaptureFailedStatus:
                                break;
                        }
                    })
                    .DisposeWith(d);
                this.BindCommand(ViewModel, x => x.beginCapture, x => x.start).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.endCapture, x => x.nextFinger).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.restart, x => x.restart).DisposeWith(d);
                //todo: restart

                ViewModel.onStart = async () =>
                {
                    bioState.active = true;
                    //update the fingers and remove only if
                    bool containsAll(NFPosition[] target, params NFPosition[] missingFingers)
                    {
                        if (missingFingers == null) return false;
                        foreach (var f in missingFingers)
                        {
                            if (!target.Contains(f)) return false;
                        }
                        return true;
                    }
                    NFPosition[] filteredFingers()
                    {
                        return Common.fingers
                            .Where(x => !fSelector.MissingPositions.Contains(x))
                            .ToArray();
                    }
                    NFPosition[] filteredSlaps()
                    {
                        var output = new List<NFPosition>();
                        if (!containsAll(fSelector.MissingPositions, NFPosition.LeftIndex, NFPosition.LeftLittle, NFPosition.LeftMiddle, NFPosition.LeftRing))
                            output.Add(NFPosition.PlainLeftFourFingers);
                        if (!containsAll(fSelector.MissingPositions, NFPosition.RightIndex, NFPosition.RightLittle, NFPosition.RightMiddle, NFPosition.RightRing))
                            output.Add(NFPosition.PlainRightFourFingers);
                        if (!containsAll(fSelector.MissingPositions, NFPosition.LeftThumb, NFPosition.RightThumb))
                            output.Add(NFPosition.PlainThumbs);
                        return output.ToArray();
                    }
                    //todo: handle when no fingers are selected. For now, let the user select a reason and move on
                    fingers =
                        State.singleton.canCaptureSlaps
                            ? filteredSlaps()
                            : filteredFingers();
                    if (ViewModel.currentFingerPosition >= fingers.Length) ViewModel.currentFingerPosition = 0;
                    if (ViewModel.currentFingerPosition < 0) ViewModel.currentFingerPosition = 0;
                    var position = fSelector.SelectedPosition = fingers[ViewModel.currentFingerPosition];
                    await bioState.beginCapture(position, fSelector.MissingPositions);
                };
                ViewModel.onCapture = async () =>
                {
                    //todo: extract the template and wsq and move to the next finger
                    ViewModel.currentFingerPosition++;
                    if (ViewModel.currentFingerPosition >= fingers.Length)
                    {
                        //can't continue
                        ViewModel.currentFingerPosition--;
                    }
                    else
                    {
                        //some cleanup
                        fingerView.Finger = null;
                        //process
                        await ViewModel.beginCapture.Execute();
                    }
                };
                ViewModel.onCleanup = async () =>
                {
                    try
                    {
                        bioState.cleanUp();
                    }
                    catch (Exception e)
                    {

                    }
                };

                //start
                ViewModel.currentFingerPosition = 0;
                //ViewModel.beginCapture.Execute();
                ViewModel.hasMoreFingers = ViewModel.currentFingerPosition > fingers.Length;
            });
        }


        private void fSelectorOnFingerClick(object sender, FingerSelector.FingerClickArgs e)
        {
            var selector = sender as FingerSelector;
            if (selector == null) return;
            var missing = selector.MissingPositions;
            var selectedFinger = e.Position;
            if (missing.Contains(selectedFinger))
                missing = missing.Where(x => x != selectedFinger).ToArray();
            else
                missing = missing.Concat(new[] { selectedFinger }).ToArray();
            selector.MissingPositions = missing;
            ViewModel.missingFingerCount = missing.Length;
        }

       

        
    }
}
