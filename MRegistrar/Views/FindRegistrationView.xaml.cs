﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for FindByVoterIdView.xaml
    /// </summary>
    public partial class FindRegistrationView : ReactiveUserControl<FindRegistrationViewModel>
    {
        public FindRegistrationView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.Bind(ViewModel, x => x.voterId, x => x.voterId.Text).DisposeWith(d);
                this.Bind(ViewModel, x => x.voterName, x => x.voterName.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.IsActive,
                    x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.Message.Content).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.search, x => x.search).DisposeWith(d);
                Observable.FromEventPattern(toast.Message, "ActionClick")
                    .Select(_ => "")
                    .BindTo(ViewModel, x => x.errorMessage)
                    .DisposeWith(d);
                //this.OneWayBind(ViewModel, x => x.registrations, view => view.results.ItemsSource).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.registrations)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .BindTo(this, x => x.results.ItemsSource)
                    .DisposeWith(d);
            });
        }

        void viewRegistration(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    var d = (RegistrationEntryViewModel) row.DataContext;
                    openViewRegistration(d.voterId);
                }
            }
        }

        private void openViewRegistration(string voterId)
        {
            var result = Data.Registrations.find(voterId);
            if (result.IsOk)
            {
                var x = result.ResultValue;
                var vm = new PrintCardViewModel { age = x.estimatedAge, dateOfBirth = x.dateOfBirth, fatherName = x.fatherName, motherName = x.motherName, hometown = x.homeTown, hometownAddress = x.homeTownAddress, hometownDistrict = x.homeTownDistrict, hometownRegion = x.hometownRegion, idNumber = x.idNumber, idType = Data.Utils.getValueOrDefault("", x.idType), isDisabled = x.isDisabled, isVisuallyImpaired = x.isVisuallyImpaired, otherNames = x.otherNames, phoneNumber = x.phoneNumber, residentialAddress = x.residentialAddress, residentialDistrict = x.residentialDistrict, residentialTown = x.residentialTown, residentialRegion = x.residentialRegion, sex = x.sex, surname = x.surname, voterId = x.voterId, photo = x.photo, pollingStationCode = Data.App.config.code, name = $"{x.surname} {x.otherNames}", dateOfRegistration = x.dateRegistered };
                SharedViewModel.singleton.activeViewModel = vm;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
            }
        }
    }
}
