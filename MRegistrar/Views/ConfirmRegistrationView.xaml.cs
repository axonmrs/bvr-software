﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ConfirmRegistrationView.xaml
    /// </summary>
    public partial class ConfirmRegistrationView : ReactiveUserControl<ConfirmRegistrationViewModel>
    {
        public ConfirmRegistrationView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.OneWayBind(ViewModel, x => x.otherNames, x => x.otherNames.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.surname, x => x.surname.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.dateOfBirth, x => x.dateOfBirth.Text, x => x.ToString("yyyy-MM-dd")).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.age, x => x.age.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.sex, x => x.sex.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.phoneNumber, x => x.phoneNumber.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialAddress, x => x.residentialAddress.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialTown, x => x.residentialTown.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialRegion, x => x.residentialRegion.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialDistrict, x => x.residentialDistrict.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.idType, x => x.idType.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.idNumber, x => x.idNumber.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.fatherName, x => x.fathersName.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.motherName, x => x.mothersName.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometown, x => x.hometown.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownAddress, x => x.hometownAddress.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownRegion, x => x.hometownRegion.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownDistrict, x => x.hometownDistrict.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isVisuallyImpaired, x => x.isVisuallyImpaired.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isDisabled, x => x.isPhysicallyDisabled.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.photo, x => x.photo.Source, xs => Utilities.toBitmapSource(xs)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.voterId, x => x.voterId.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.voterId, x => x.voterIdContainer.Visibility, x => string.IsNullOrWhiteSpace(x) ? Visibility.Collapsed : Visibility.Visible).DisposeWith(d);

                this.OneWayBind(ViewModel, x => x.dateOfRegistration, x => x.dateOfRegistration.Text, x => x.ToString("dd/MM/yyyy")).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.pollingStationCode, x => x.pollingStationCode.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.photo, x => x.cardPhoto.Source, xs => Utilities.toBitmapSource(xs)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.name, x => x.name.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.dateOfBirth, x => x.cardDateOfBirth.Text, x => x.ToString("yyyy-MM-dd")).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.sex, x => x.cardSex.Text).DisposeWith(d);

                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.IsActive, x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.Message.Content).DisposeWith(d);

                this.BindCommand(ViewModel, x => x.save, x => x.save).DisposeWith(d);

                ViewModel.pollingStationCode = Data.App.config.code;
                ViewModel.dateOfRegistration = DateTime.Now;
                this.OneWayBind(ViewModel, x => x.voterId, x => x.cardVoterId.Text).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.otherNames, x => x.surname, (otherNames, surname) => $"{surname}, {otherNames}")
                    .BindTo(ViewModel, x => x.name)
                    .DisposeWith(d);
                ViewModel.printCard = async voterId =>
                {
                    Utilities.printCard(voterCard);
                    //? do we know if print worked
                    await Task.Delay(3000);
                    //move to home
                    SharedViewModel.singleton.activeViewModel = new OperatorHomeViewModel();
                };
                ViewModel.voterId = Data.Registrations.getNextVoterIdNumber();
                createQRCode();
                this.OneWayBind(ViewModel, x => x.qrCode, x => x.barcode.Source, xs => Utilities.toBitmapSource(xs)).DisposeWith(d);

                Observable.FromEventPattern(toast.Message, "ActionClick")
                    .Select(_ => "")
                    .BindTo(ViewModel, x => x.errorMessage)
                    .DisposeWith(d);
            });
        }

        private void createQRCode()
        {
            //qr code
            var qrCodeData = $@"
{{
kind:""VoterCard"",
id:{ViewModel.voterId},
name:""{ViewModel.surname} {ViewModel.otherNames}"",
sex:""{ViewModel.sex}"",
dob:{ViewModel.dateOfBirth:yyyyMMdd},
issueDate:{ViewModel.dateOfRegistration:yyyyMMdd}
}}
                    ".Trim();
            ViewModel.qrCode = Utilities.createQRCode(qrCodeData, 15);
        }
    }
}
