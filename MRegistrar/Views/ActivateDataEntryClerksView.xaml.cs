﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for FindByVoterIdView.xaml
    /// </summary>
    public partial class ActivateDataEntryClerksView : ReactiveUserControl<ActivateDataEntryClerksViewModel>
    {
        public ActivateDataEntryClerksView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.BindCommand(ViewModel, x => x.addDec, x => x.addDec).DisposeWith(d);

                ViewModel.WhenAnyValue(x => x.officers)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .BindTo(this, x => x.results.ItemsSource)
                    .DisposeWith(d);
            });
        }

        void changeStatus(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    var d = (RegistrationUserViewModel)row.DataContext;
                    d.isActive = changeOfficerStatus(d.id, d.isActive);
                }
            }
        }

        private bool changeOfficerStatus(long id, bool isActive)
        {
            bool state = !isActive;
            var res = Data.Users.toggleActivateUser(id, state);
            if (res.IsOk)
            {
                return state;
            }
            else
            {
                //errorMessage = result.ErrorValue.GetBaseException().Message;
                return isActive;
            }
        }

    }
}
