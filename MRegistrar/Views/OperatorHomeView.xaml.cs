﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System.Reactive.Disposables;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for OperatorHome.xaml
    /// </summary>
    public partial class OperatorHomeView : ReactiveUserControl<OperatorHomeViewModel>
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public OperatorHomeView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //cleanup
                SharedViewModel.singleton.registrationData?.reset();

                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.BindCommand(ViewModel, x => x.registerNew, x => x.registerNew).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.registerExisting, x => x.registerExisting).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openReports, view => view.reports).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openQueue, view => view.queue).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openFindVoter, x => x.openFindVoterView).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.openUpdateVoterDetails, x => x.openUpdateVoter).DisposeWith(d);
            });
        }
    }
}
