﻿using MRegistrar.ViewModels;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class AddRegistrationOfficerView : ReactiveUserControl<AddRegistrationOfficerViewModel>
    {
        public AddRegistrationOfficerView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.OneWayBind(ViewModel, x => x.message, x => x.toast.IsActive,
                    x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.message, x => x.toast.Message.Content).DisposeWith(d);
                this.Bind(ViewModel, x => x.registrationOfficerId, x => x.registrationOfficer.SelectedValue).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.officers, view => view.registrationOfficer.ItemsSource)
            .DisposeWith(d);
                this.BindCommand(ViewModel, vm => vm.add, v => v.add).DisposeWith(d);
            });
        }
    }
}
