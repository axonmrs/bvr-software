﻿using MRegistrar.ViewModels;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for UpdateVoterView.xaml
    /// </summary>
    public partial class UpdateVoterView : ReactiveUserControl<UpdateVoterViewModel>
    {
        public UpdateVoterView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                //locals
                this.Bind(ViewModel, x => x.otherNames, x => x.otherNames.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.otherNames, v => v.otherNamesValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.surname, x => x.surname.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.surname, v => v.surnameValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.dateOfBirth, x => x.dateOfBirth.SelectedDate).DisposeWith(d);

                this.Bind(ViewModel, x => x.age, x => x.age.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.ageRule, v => v.ageValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, vm => vm.isEstimatedAge, v => v.isEstimatedAge.IsChecked).DisposeWith(d);

                this.Bind(ViewModel, x => x.phoneNumber, x => x.phoneNumber.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.phoneNumber, v => v.phoneNumberValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.residentialAddress, x => x.residentialAddress.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.residentialAddress, v => v.residentialAddressValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.residentialTown, x => x.residentialTown.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.residentialTown, v => v.residentialTownValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.residentialRegionId, x => x.residentialRegion.SelectedValue).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.residentialRegionRule, v => v.residentialRegionValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.residentialDistrictId, x => x.residentialDistrict.SelectedValue).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.residentialDistrictRule, v => v.residentialDistrictValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.fatherName, x => x.fathersName.Text).DisposeWith(d);
                this.Bind(ViewModel, x => x.motherName, x => x.mothersName.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.hometown, x => x.hometown.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.hometown, v => v.hometownValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.hometownAddress, x => x.hometownAddress.Text).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.hometownAddress, v => v.hometownAddressValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.hometownRegionId, x => x.hometownRegion.SelectedValue).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.hometownRegionRule, v => v.hometownRegionValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.hometownDistrictId, x => x.hometownDistrict.SelectedValue).DisposeWith(d);
                this.BindValidation(ViewModel, vm => vm.hometownDistrictRule, v => v.hometownDistrictValidation.Text).DisposeWith(d);

                this.Bind(ViewModel, x => x.isVisuallyImpaired, x => x.visuallyImpaired.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.isDisabled, x => x.isPhysicallyDisabled.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.hearingImpaired, x => x.hearingImpaired.IsChecked).DisposeWith(d);

                this.Bind(ViewModel, x => x.leper, x => x.leper.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.missingFingersL, x => x.missingFingersL.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.missingFingersR, x => x.missingFingersR.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.amputatedHandsL, x => x.amputatedHandL.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, x => x.amputatedHandsR, x => x.amputatedHandR.IsChecked).DisposeWith(d);
                //gender
                this.Bind(ViewModel, x => x.sex, view => view.male.IsChecked, x => x == "Male", x => x == null ? "" : x.Value ? "Male" : "Female").DisposeWith(d);
                this.Bind(ViewModel, x => x.sex, view => view.female.IsChecked, x => x == "Female", x => x == null ? "" : x.Value ? "Female" : "Male").DisposeWith(d);


                this.OneWayBind(ViewModel, vm => vm.formTitle, v => v.formTitle.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.photo, x => x.photo.Source, xs => Utilities.toBitmapSource(xs)).DisposeWith(d);

                //dropdowns
                SharedViewModel.singleton.WhenAnyObservable(x => x.regions)
                  .BindTo(this, view => view.hometownRegion.ItemsSource)
                  .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyObservable(x => x.districts)
                    .BindTo(this, view => view.hometownDistrict.ItemsSource)
                    .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyObservable(x => x.regions)
                  .BindTo(this, view => view.residentialRegion.ItemsSource)
                  .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyObservable(x => x.districts)
                    .BindTo(this, view => view.residentialDistrict.ItemsSource)
                    .DisposeWith(d);

                this.BindCommand(ViewModel, vm => vm.gotoNext, v => v.next).DisposeWith(d);

                ViewModel.WhenAnyValue(x => x.isEstimatedAge)
                  .Subscribe(disableDateOfBirthFields)
                  .DisposeWith(d);

                surname.Focus();
            });
        }
        private void disableDateOfBirthFields(bool isChecked)
        {
            dateOfBirth.IsEnabled = !isChecked;
            age.IsEnabled = isChecked;
        }
    }
}
