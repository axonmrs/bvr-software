﻿using MRegistrar.ViewModels;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class RegistrationPeriodView : ReactiveUserControl<RegistrationPeriodViewModel>
    {
        public RegistrationPeriodView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                //shared
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.OneWayBind(ViewModel, x => x.message, x => x.toast.IsActive,
                    x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.message, x => x.toast.Message.Content).DisposeWith(d);
                this.Bind(ViewModel, x => x.startDate, x => x.startDate.SelectedDate).DisposeWith(d);
                this.Bind(ViewModel, x => x.endDate, x => x.endDate.SelectedDate).DisposeWith(d);
                this.Bind(ViewModel, x => x.startTime, x => x.startTime.Text).DisposeWith(d);
                this.Bind(ViewModel, x => x.endTime, x => x.endTime.Text).DisposeWith(d);
              this.Bind(ViewModel, x => x.pollingStationId, x => x.pollingStation.SelectedValue).DisposeWith(d);
                this.BindCommand(ViewModel, vm => vm.update, v => v.update).DisposeWith(d);

              SharedViewModel.singleton.WhenAnyObservable(x => x.pollingStations)
                  .BindTo(this, view => view.pollingStation.ItemsSource)
                  .DisposeWith(d);
            });
        }
    }
}
