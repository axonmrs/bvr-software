﻿using Microsoft.Reporting.WinForms;
using MRegistrar.Data.Reporting;
using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ListReportsView.xaml
    /// </summary>
    public partial class ListReportsView : ReactiveUserControl<ReportViewModel>
    {
        public ListReportsView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.BindCommand(ViewModel, x => x.generateReport, view => view.generateReport).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.reportTitle, view => view.reportTitle.Text).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.generateRequests)
                    .Where(x => x > 0) //skip the first time it is set so we don't generate the report
                    .Subscribe(_ => generate(reportViewer, ViewModel.reportType))
                    .DisposeWith(d);
                var busy = Observable.FromEventPattern(reportViewer, "RenderingBegin").Select(_ => true);
                var notBusy = Observable.FromEventPattern(reportViewer, "RenderingComplete").Select(_ => false);
                busy.Merge(notBusy)
                    .BindTo(ViewModel, x => x.busy)
                    .DisposeWith(d);
                ViewModel.busy = false; //init

                reportViewer.ShowRefreshButton = false;
                reportViewer.ShowExportButton = false;
                reportViewer.ZoomMode = ZoomMode.FullPage;
                reportViewer.ShowFindControls = false;

                //reportViewer.SetDisplayMode(DisplayMode.PrintLayout);
            });
        }

        private void generate(ReportViewer reportViewer, ReportViewModel.ReportTypes reportType)
        {
            var localReport = reportViewer.LocalReport;
            localReport.DataSources.Clear();
            var (dataSources, reportFile) = getData(reportType);
            dataSources.ForEach(localReport.DataSources.Add);
            localReport.ReportPath = $"./Reports/{reportFile}.rdlc";
            reportViewer.RefreshReport();
            reportViewer.SetDisplayMode(DisplayMode.PrintLayout);
        }

        private (List<ReportDataSource> data, string reportFile) getData(ReportViewModel.ReportTypes type)
        {
            var dataSource = new ReportDataSource { Name = "Data", Value = new object[] { } };
            //todo: how is the querying applied
            var user = SharedViewModel.singleton.user;
            switch (type)
            {
                case ReportViewModel.ReportTypes.AllRegistrations:
                    dataSource.Value = ReportData.getAllRegistrations();
                    Data.ReportLogs.save(new Data.ReportLogs.ReportLog(ReportViewModel.ReportTypes.AllRegistrations.ToString(), user.name));
                    return (new List<ReportDataSource> { dataSource }, "AllRegistrations");
                case ReportViewModel.ReportTypes.StartOfDayStatistics:
                    dataSource.Value = ReportData.getStartOfDayStatistics();
                    Data.ReportLogs.save(new Data.ReportLogs.ReportLog(ReportViewModel.ReportTypes.StartOfDayStatistics.ToString(), user.name));
                    return (new List<ReportDataSource> { dataSource }, "StartOfDayStatistics");
                case ReportViewModel.ReportTypes.EndOfDayStatistics:
                    dataSource.Value = ReportData.getEndOfDayStatistics();
                    Data.ReportLogs.save(new Data.ReportLogs.ReportLog(ReportViewModel.ReportTypes.EndOfDayStatistics.ToString(), user.name));
                    return (new List<ReportDataSource> { dataSource }, "EndOfDayStatistics");
                default:
                    return (new List<ReportDataSource> { dataSource }, "404");
            }
        }
    }
}
