﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    public partial class ConfirmShutdownView : ReactiveUserControl<ConfirmShutdownViewModel>
    {
        public ConfirmShutdownView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;

                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.IsActive,
                    x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.Message.Content).DisposeWith(d);
                this.Bind(ViewModel, vm => vm.noPendingBackups, v => v.noPendingBackups.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, vm => vm.noPendingTransmissions, v => v.noPendingTransmissions.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, vm => vm.printedStartOfDayReport, v => v.printedStartOfDayReport.IsChecked).DisposeWith(d);
                this.Bind(ViewModel, vm => vm.printedEndOfDayReport, v => v.printedEndOfDayReport.IsChecked).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.error)
       .Subscribe(enableShutDownButton)
       .DisposeWith(d);
                this.BindCommand(ViewModel, x => x.shutDown, x => x.shutDown).DisposeWith(d);

                Observable.FromEventPattern(toast.Message, "ActionClick")
                    .Select(_ => "")
                    .BindTo(ViewModel, x => x.errorMessage)
                    .DisposeWith(d);
            });
        }

        private void enableShutDownButton(string msg)
        {
            if(string.IsNullOrEmpty(msg))
                shutDown.IsEnabled = true;
        }
    }
}
