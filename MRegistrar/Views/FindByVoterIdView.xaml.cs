﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for FindByVoterIdView.xaml
    /// </summary>
    public partial class FindByVoterIdView : ReactiveUserControl<FindByVoterIdViewModel>
    {
        public FindByVoterIdView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.Bind(ViewModel, x => x.voterId, x => x.voterId.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.IsActive,
                      x => !string.IsNullOrWhiteSpace(x)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.errorMessage, x => x.toast.Message.Content).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.search, x => x.search).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.next, x => x.next).DisposeWith(d);
                Observable.FromEventPattern(toast.Message, "ActionClick")
                    .Select(_ => "")
                    .BindTo(ViewModel, x => x.errorMessage)
                    .DisposeWith(d);

                //bind the data
                this.OneWayBind(ViewModel, x => x.otherNames, x => x.otherNames.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.surname, x => x.surname.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.dateOfBirth, x => x.dateOfBirth.Text, x => x.ToString("yyyy-MM-dd")).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.age, x => x.age.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.sex, x => x.sex.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.phoneNumber, x => x.phoneNumber.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialAddress, x => x.residentialAddress.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialTown, x => x.residentialTown.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialRegion, x => x.residentialRegion.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.residentialDistrict, x => x.residentialDistrict.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.idType, x => x.idType.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.idNumber, x => x.idNumber.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.fatherName, x => x.fathersName.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.motherName, x => x.mothersName.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometown, x => x.hometown.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownAddress, x => x.hometownAddress.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownRegion, x => x.hometownRegion.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.hometownDistrict, x => x.hometownDistrict.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isVisuallyImpaired, x => x.isVisuallyImpaired.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isDisabled, x => x.isPhysicallyDisabled.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.photo, x => x.photo.Source, xs => Utilities.toBitmapSource(xs)).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.canContinue, x => x.displayView.Visibility, x => x ? Visibility.Visible : Visibility.Hidden).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.canContinue, x => x.next.Visibility, x => x ? Visibility.Visible : Visibility.Hidden).DisposeWith(d);

                this.voterId.Focus();
            });
        }
    }
}
