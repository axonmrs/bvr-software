﻿using Microsoft.Reporting.WinForms;
using MRegistrar.Data.Reporting;
using MRegistrar.Helpers;
using MRegistrar.ViewModels;
using RawPrint;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static MRegistrar.Helpers.PrintHelper;

namespace MRegistrar.Views
{
    /// <summary>
    /// Interaction logic for ListReportsView.xaml
    /// </summary>
    public partial class QueueManagementView : ReactiveUserControl<QueueManagementViewModel>
    {
        public QueueManagementView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                if (!this.isCurrent())
                {
                    //can do cleanup here
                    return;
                }
                SharedViewModel.singleton.activePageTitle = ViewModel.UrlPathSegment;
                this.BindCommand(ViewModel, x => x.generateReport, view => view.generateReport).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.printReport, view => view.printReport).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.reportTitle, view => view.reportTitle.Text).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.generateRequests)
                    .Where(x => x > 0) //skip the first time it is set so we don't generate the report
                    .Subscribe(async _ => await generate())
                    .DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.printRequests)
                    .Where(x => x > 0)
                    .Subscribe(async _ => await print())
                    .DisposeWith(d);
                ViewModel.busy = false; //init

                //form
                this.Bind(ViewModel, x => x.date, x => x.date.SelectedDate).DisposeWith(d);
                this.Bind(ViewModel, x => x.startNumber, x => x.startNumber.Text).DisposeWith(d);
                this.Bind(ViewModel, x => x.count, x => x.count.Text).DisposeWith(d);
            });
        }

        private async Task generate()
        {
            try
            {
                ViewModel.busy = true;
                var bytes = ReportHelper.generateReport(() => ReportHelper.getQueueData(3, ViewModel.count, ViewModel.startNumber, ViewModel.date));
                var filename = FileHelper.saveReport("queue.pdf", bytes);
                pdfViewer.pdfPath = $"{filename}?{DateTime.Now.Ticks}";
                ViewModel.hasFile = true;
            }
            catch(Exception e)
            {

            }
            finally
            {
                ViewModel.busy = false;
            }
        }

        private async Task print()
        {
            try
            {
                ViewModel.busy = true;
                var printOk = printFile(pdfViewer.pdfPath.Split('?')[0]);
                //assume printing worked and move on
            }
            catch(Exception e)
            {

            }
            finally
            {
                ViewModel.busy = false;
            }
        }
    }
}
