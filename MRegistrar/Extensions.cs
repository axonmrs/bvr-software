﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MRegistrar
{
    public static class Extensions
    {
        public static bool isCurrent<T>(this ReactiveUserControl<T> src) where T:class
        {
            return src.ViewModel == SharedViewModel.singleton.activeViewModel;
        }

        public static void MutateVerbose<TField>(this INotifyPropertyChanged instance, ref TField field, TField newValue, Action<PropertyChangedEventArgs> raise, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<TField>.Default.Equals(field, newValue)) return;
            field = newValue;
            raise?.Invoke(new PropertyChangedEventArgs(propertyName));
        }
    }
    public static class ListBoxExtensions
    {
        public static IObservable<List<T>> SelectionChanged<T>(this ListBox listBox)
        {
            return Observable.FromEventPattern<SelectionChangedEventHandler, SelectionChangedEventArgs>(
                eh => listBox.SelectionChanged += eh,
                eh => listBox.SelectionChanged -= eh)
                .Select(_ => listBox.SelectedItems.Cast<T>().ToList());
        }
    }
}
