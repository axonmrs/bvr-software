﻿using MRegistrar.ViewModels;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using QRCoder;

namespace MRegistrar
{
    public static class Utilities
    {
        public static SolidColorBrush brushFromDiagnosisStatus(DiagnosisStatus x)
        {
            switch (x)
            {
                case DiagnosisStatus.Error:
                    return Brushes.Red;
                case DiagnosisStatus.Ok:
                    return Brushes.Green;
                case DiagnosisStatus.NotChecked:
                    return Brushes.Purple;
                default: return Brushes.Black;
            }
        }

        public static BitmapSource toBitmapSource(this byte[] bytes)
        {
            if (bytes == null) return null;
            //create the stream
            var stream = new MemoryStream(bytes);
            stream.Seek(0, SeekOrigin.Begin);
            //create the image
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        public static void printCard(Visual visual)
        {
            var dialog = new PrintDialog();
            dialog.PrintVisual(visual, "Printing Card.");
        }
        public static byte[] createQRCode(string data, int pixelsPerModule)
        {
            var generator = new QRCodeGenerator();
            var qrCodeData = generator.CreateQrCode(data, QRCodeGenerator.ECCLevel.H);
            var qrCode = new BitmapByteQRCode(qrCodeData);
            var byteArray = qrCode.GetGraphic(pixelsPerModule);
            return byteArray;
        }
    }
}
