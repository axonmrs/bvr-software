﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for DiagnosisResultView.xaml
    /// </summary>
    public partial class RegistrationOfficerView : ReactiveUserControl<RegistrationUserViewModel>
    {
        public RegistrationOfficerView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                //this.OneWayBind(ViewModel, x => x.id, x => x.id.Text)
                //    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.name, x => x.name.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isActive, x => x.isActive.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.username, x => x.username.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.num, x => x.num.Text)
                    .DisposeWith(d);

                this.BindCommand(ViewModel, x => x.remove, x => x.remove).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.changeStatus, x => x.changeStatus).DisposeWith(d);
            });
        }
    }
}
