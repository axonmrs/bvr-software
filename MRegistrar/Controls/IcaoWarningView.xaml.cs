﻿using MRegistrar.ViewModels;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for IcaoWarningView.xaml
    /// </summary>
    public partial class IcaoWarningView : ReactiveUserControl<IcaoWarningViewModel>
    {
        public IcaoWarningView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel, x => x.display, x => x.name.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.status, x => x.name.Foreground, x =>
                {
                    switch (x)
                    {
                        case WarningStatus.Ok: return Brushes.Green;
                        case WarningStatus.Error: return Brushes.Red;
                        case WarningStatus.NotChecked: return Brushes.Purple;
                        default: return Brushes.Black;
                    }
                })
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isEssential, x => x.name.FontWeight, x => x ? FontWeights.Bold : FontWeights.Normal)
                    .DisposeWith(d);
            });
        }
    }
}
