﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for DiagnosisResultView.xaml
    /// </summary>
    public partial class DiagnosisResultView : ReactiveUserControl<DiagnosisResultViewModel>
    {
        public DiagnosisResultView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel, x => x.name, x => x.name.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.icon, x => x.icon.Kind)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.status, x => x.icon.Foreground, x =>
                {
                    switch (x)
                    {
                        case DiagnosisStatus.Error:
                            return Brushes.Red;
                        case DiagnosisStatus.Ok:
                            return Brushes.Green;
                        case DiagnosisStatus.NotChecked:
                            return Brushes.Purple;
                        default: return Brushes.Black;
                    }
                }).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.status, x => x.name.Foreground, x =>
                {
                    switch (x)
                    {
                        case DiagnosisStatus.Error:
                            return Brushes.Red;
                        case DiagnosisStatus.Ok:
                            return Brushes.Green;
                        case DiagnosisStatus.NotChecked:
                            return Brushes.Purple;
                        default: return Brushes.Black;
                    }
                }).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.isEssential, x => x.name.FontWeight, x => x ? FontWeights.Bold : FontWeights.Normal).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.notes, x => x.name.ToolTip).DisposeWith(d);
            });
        }
    }
}
