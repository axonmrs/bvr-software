﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.Storage.Streams;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for PdfViewer.xaml
    /// </summary>
    public partial class PdfViewer : UserControl
    {
        public PdfViewer()
        {
            InitializeComponent();
        }

        public string pdfPath
        {
            get { return (string)GetValue(PdfPathProperty); }
            set { SetValue(PdfPathProperty, value); }
        }


        public static readonly DependencyProperty PdfPathProperty =
            DependencyProperty.Register("pdfPath", typeof(string), typeof(PdfViewer),
                new PropertyMetadata(null, onPdfPathChanged));

        private static void onPdfPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pdfDrawer = (PdfViewer)d;
            var pdfPath = pdfDrawer.pdfPath;
            if (!string.IsNullOrEmpty(pdfDrawer.pdfPath))
            {
                pdfPath = pdfPath.Contains("?") ? pdfPath.Split('?')[0] : pdfPath;
                var path = System.IO.Path.GetFullPath(pdfPath);
                StorageFile.GetFileFromPathAsync(path).AsTask()
                    .ContinueWith(x => PdfDocument.LoadFromFileAsync(x.Result).AsTask()).Unwrap()
                    .ContinueWith(x => pdfToImages(pdfDrawer, x.Result), TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private async static Task pdfToImages(PdfViewer viewer, PdfDocument doc)
        {
            var items = viewer.pageContainer.Items;
            items.Clear();

            if (doc == null) return;
            for(uint i = 0; i<doc.PageCount; i++)
            {
                using (var page = doc.GetPage(i))
                {
                    var bitmap = await pageToBitmapAsync(page);
                    var image = new Image
                    {
                        Source = bitmap,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(0, 4, 0, 4),
                        MaxWidth = 800
                    };
                    items.Add(image);
                }
            }
        }

        private static async Task<BitmapImage> pageToBitmapAsync(PdfPage page)
        {
            var image = new BitmapImage();
            using(var stream = new InMemoryRandomAccessStream())
            {
                await page.RenderToStreamAsync(stream);
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = stream.AsStream();
                image.EndInit();
            }
            return image;
        }
    }
}
