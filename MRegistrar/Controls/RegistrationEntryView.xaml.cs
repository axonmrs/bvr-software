﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for DiagnosisResultView.xaml
    /// </summary>
    public partial class RegistrationEntryView : ReactiveUserControl<RegistrationEntryViewModel>
    {
        public RegistrationEntryView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel, x => x.voterId, x => x.voterId.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.surname, x => x.surname.Text)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.otherNames, x => x.otherNames.Text).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.num, x => x.num.Text)
                    .DisposeWith(d);

                //this.OneWayBind(ViewModel, x => (Bitmap)(new ImageConverter()).ConvertFrom(x.photo), x => x.photo.Source).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.viewRegistration, x => x.viewRegistration).DisposeWith(d);
            });
        }
    }
}
