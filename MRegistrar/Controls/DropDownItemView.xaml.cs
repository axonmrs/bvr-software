﻿using MRegistrar.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MRegistrar.Controls
{
    /// <summary>
    /// Interaction logic for DropDownItemView.xaml
    /// </summary>
    public partial class DropDownItemView : ReactiveUserControl<DropDownItemViewModel>
    {
        public DropDownItemView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel, x => x.name, x => x.text.Text)
                    .DisposeWith(d);
            });
        }
    }
}
