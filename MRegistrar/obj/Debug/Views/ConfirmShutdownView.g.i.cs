﻿#pragma checksum "..\..\..\Views\ConfirmShutdownView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "819A944705C524707B30E3392F9EE4C9FDC233E30C8CA17A6E3888D653ABF6BF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MRegistrar.ViewModels;
using MRegistrar.Views;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using ReactiveUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MRegistrar.Views {
    
    
    /// <summary>
    /// ConfirmShutdownView
    /// </summary>
    public partial class ConfirmShutdownView : ReactiveUI.ReactiveUserControl<MRegistrar.ViewModels.ConfirmShutdownViewModel>, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Snackbar toast;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox noPendingBackups;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox noPendingTransmissions;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox printedStartOfDayReport;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox printedEndOfDayReport;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\ConfirmShutdownView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button shutDown;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MRegistrar;component/views/confirmshutdownview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\ConfirmShutdownView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.toast = ((MaterialDesignThemes.Wpf.Snackbar)(target));
            return;
            case 2:
            this.noPendingBackups = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 3:
            this.noPendingTransmissions = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 4:
            this.printedStartOfDayReport = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 5:
            this.printedEndOfDayReport = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 6:
            this.shutDown = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

