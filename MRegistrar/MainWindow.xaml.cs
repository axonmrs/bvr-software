﻿using ReactiveUI;
using MRegistrar.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using MaterialDesignThemes.Wpf;
using MRegistrar.ViewModels.Interfaces;
using MRegistrar.BioService;
using MRegistrar.Services;
using System.Windows.Input;
using System.Drawing;
using MRegistrar.Data;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.Printing;

namespace MRegistrar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<MainViewModel>
    {
        #region Unmanaged
        
        #endregion

        private IdleDetector idleDetector;
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Alternate Router. An attempt to fix the existing navigation issue (back and sometimes cancel)
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            KeyDown += onKeyDown;
            SourceInitialized += onInitialized;

            //hotkeys
            var demoCmd = new RoutedCommand();
            demoCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control | ModifierKeys.Alt));
            CommandBindings.Add(new CommandBinding(demoCmd, (sender, args) =>
            {
                if (SharedViewModel.singleton.inDemoMode) return;
                SharedViewModel.singleton.inDemoMode = !SharedViewModel.singleton.inDemoMode;
            }));
            var deviceInfoCommand = new RoutedCommand();
            deviceInfoCommand.InputGestures.Add(new KeyGesture(Key.I, ModifierKeys.Control | ModifierKeys.Alt));
            CommandBindings.Add(new CommandBinding(deviceInfoCommand, (sender, args) =>
            {
                SharedViewModel.singleton.activeViewModel = new DeviceDetailsViewModel();
            }));
            ViewModel = new MainViewModel();
            ViewModel.close = Close;
            //cleanup
            Dispatcher.ShutdownStarted += (_, e) =>
            {
                //todo: is this necessary. could be the source of the crash. Maybe do this if
                var bioClient = State.singleton?.client;
                if (bioClient != null)
                    try { bioClient.Cancel(); } catch { }
            };
            //idle management
            idleDetector = new IdleDetector(Config.Configuration.Settings.MaxIdleSeconds);
            idleDetector.IsIdle += (_, args) =>
            {
                //move to login page
                if (SharedViewModel.singleton.activeViewModel is LoginViewModel ||
                    SharedViewModel.singleton.activeViewModel is StartViewModel) return;
                Data.ActivityLogs.save(new Data.ActivityLogs.ActivityLog(SharedViewModel.Activity.Logout.ToString(), $"{SharedViewModel.singleton.user.name} logged out of the system due to idle time", SharedViewModel.singleton.user.username));
                SharedViewModel.singleton.user = new Data.Model.User("", "", "", "", true, true);
            };
            this.WhenActivated(d =>
            {
                //ui updates
                var config = Config.Configuration.Settings;
                appTitle.Text = config.AppTitle;
                ecName.Text = config.ECName; 

                //setup the router
                this.OneWayBind(ViewModel, x => x.Router, x => x.routedViewHost.Router)
                    .DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.dialogVisible, x => x.dialog.IsOpen).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.enableLogOut, x => x.logOut.IsEnabled).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.username, x => x.username.Text).DisposeWith(d);
                ViewModel.WhenAnyValue(x => x.timeLeft)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x =>
                    {
                        var text = x.TotalMinutes < 0 ? "Time's up" : $"{x:hh\\:mm\\:ss}";
                        closingCountDown.Text = text;
                    })
                    .DisposeWith(d);
                this.BindCommand(ViewModel, x => x.goBack, x => x.back).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.cancel, x => x.cancel).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.showDialog, x => x.logOut).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.closeDialog, x => x.logoutNo).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.logOut, x => x.logoutYes).DisposeWith(d);
                SharedViewModel.singleton.WhenAnyValue(x => x.inDemoMode)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Select(x => x ? System.Windows.Media.Brushes.Red : System.Windows.Media.Brushes.Beige)
                    .BindTo(this, x => x.topHeader.Background)
                    .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyValue(x => x.inDemoMode)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Select(x => x ? $"Demo: {config.AppTitle}" : config.AppTitle)
                    .BindTo(this, x => x.appTitle.Text)
                    .DisposeWith(d);
              SharedViewModel.singleton.WhenAnyValue(x => x.pollingStationName)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Select(x => string.IsNullOrEmpty(x) ? $"{config.AppTitle}" : $"{config.AppTitle} - {x}")
                    .BindTo(this, x => x.appTitle.Text)
                    .DisposeWith(d);
              //set the current page title
              SharedViewModel.singleton.WhenAnyValue(x => x.activePageTitle)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .BindTo(this, x => x.activePageTitle.Text)
                    .DisposeWith(d);
                //registrations counts
                SharedViewModel.singleton.WhenAnyValue(x => x.registrationsSoFar)
                    .BindTo(this, x => x.registrationsSoFar.Text)
                    .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyValue(x => x.registrationsToday)
                    .BindTo(this, x => x.registrationsToday.Text)
                    .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyValue(x => x.registrationsSinceBackup)
                    .Select(x => Config.Configuration.Settings.RegistrationsBeforeBackup - x)
                    .BindTo(this, x => x.registrationsSinceBackup.Text)
                    .DisposeWith(d);
                //notification icons
                SharedViewModel.singleton.WhenAnyObservable(x => x.diagnosisResults)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(xs =>
                    {
                        PackIcon icon = null;
                        //map the items to controls
                        foreach (var x in xs)
                        {
                            switch (x.type)
                            {
                                case DiagnosisSource.Battery:
                                    icon = battery;
                            batteryInfo.Text = x.notes;
                                    batteryInfo.Foreground = Utilities.brushFromDiagnosisStatus(x.status);
                                    break;
                                case DiagnosisSource.DataIntegrity:
                                    icon = dataIntegrity;
                                    break;
                                case DiagnosisSource.FingerprintReader:
                                    icon = fingerprint;
                                    break;
                                case DiagnosisSource.Licenses:
                                    icon = licenses;
                                    break;
                                case DiagnosisSource.ReportPrinter:
                                    icon = reportPrinter;
                                    break;
                                case DiagnosisSource.SDCard:
                                    icon = sdcard;
                                    break;
                                case DiagnosisSource.Camera:
                                    icon = camera;
                                    break;
                            }
                            if (icon == null) continue;
                            icon.Foreground = Utilities.brushFromDiagnosisStatus(x.status);
                            icon.ToolTip = string.IsNullOrWhiteSpace(x.notes) ? null : x.notes;
                        }
                    })
                    .DisposeWith(d);
                //navigation
                ViewModel.Router.CurrentViewModel
                    .Subscribe(x =>
                    {
                        var setting = x as IViewSettings;
                        ViewModel.onCancel = setting?.onCancel;
                        ViewModel.onBack = setting?.onBack;
                        ViewModel.enableLogOut = setting?.enableLogOut ?? false;
                        ViewModel.enableShutdown = setting?.enableShutDown ?? true;
                        ViewModel.enableTransfer = setting?.enableTransfer ?? true;
                        ViewModel.enableBackup = setting?.enableBackup ?? true;
                    })
                    .DisposeWith(d);
                SharedViewModel.singleton.WhenAnyValue(x => x.activeViewModel)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Where(x => x != null)
                    .Subscribe(x =>
                    {
                        ResourceService.singleton.logMemory($"New View: {x?.GetType()?.Name}");
                        //update alt router
                        var clearAll = x is LoginViewModel || x is ActivationViewModel;
                        x = ViewModel.altRouter.Push(x, clearAll);
                        //set the navigation settings
                        var settings = x as IViewSettings;
                        if (settings != null)
                        {
                            ViewModel.showTray = settings.showTray;
                            ViewModel.showStatus = settings.showStatusSummaries;
                        }
                        ViewModel.Router.NavigateAndReset.Execute(x);
                    })
                    .DisposeWith(d);

                //end of remove
                SharedViewModel.singleton.WhenAny(x => x.user, x => x.diagnosisOk, x => x.activated, x => x.clientsReady,
                    (user, diagnosisOk, activated, clientsReady) =>
                    {
                        return (user.Value, diagnosisOk.Value, activated.Value, clientsReady.Value);
                    })
                    .Subscribe(x =>
                    {
                        if (!x.Item2 || !x.Item4) //diagnosisOk && clientsReady
                        {
                            SharedViewModel.singleton.activeViewModel = new StartViewModel();
                            return;
                        }
                        if (!x.Item3) //activated
                        {
                            SharedViewModel.singleton.activeViewModel = new ActivationViewModel();
                            return;
                        }
                        if (x.Item3 && Config.USBUtils.isSetupDeviceConnection())
                        {
                            if (SharedViewModel.singleton.user == null)
                            {
                                SharedViewModel.singleton.user = new Model.User("District Officer", "", "DistrictOfficer", "districtOfficer", true, true);
                                return;
                            }
                        }
                        var type = x.Item1?.type ?? "";
                        //todo: this does not hold for all cases. See the additional checks to do
                        switch (type)
                        {
                            case "Operator":
                                SharedViewModel.singleton.activeViewModel = new OperatorHomeViewModel(null);
                                break;
                            case "RegistrationOfficer":
                            case "PresidingOfficer":
                                SharedViewModel.singleton.activeViewModel = new PresidingOfficerHomeViewModel();
                                break;
                            case "DistrictOfficer":
                                SharedViewModel.singleton.activeViewModel = new DistrictOfficerHomeViewModel();
                                break;
                            default:
                                //if (config.UseBiometricLogin) {
                                // SharedViewModel.singleton.activeViewModel = new BioVerificationViewModel { identifyVoter = false, useFace = false,userId = 0, getNextViewModel = async id => processBioLogin()};
                                //}
                               // SharedViewModel.singleton.activeViewModel = new CaptureUserFingersViewModel();
                                //else 
                                SharedViewModel.singleton.activeViewModel = new LoginViewModel();
                                //SharedViewModel.singleton.activeViewModel = new DistrictOfficerHomeViewModel();
                                break;
                        }
                    })
                    .DisposeWith(d);
                //apply the view settings
                this.OneWayBind(ViewModel, x => x.onBack, x => x.back.Visibility, x => x == null ? Visibility.Collapsed : Visibility.Visible).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.onCancel, x => x.cancel.Visibility, x => x == null ? Visibility.Collapsed : Visibility.Visible).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.showTray, x => x.trayArea.Visibility).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.showStatus, x => x.statusArea.Visibility).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.enableShutdown, x => x.shutDown.IsEnabled).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.shutDown, x => x.shutDown).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.enableTransfer, x => x.transfer.IsEnabled).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.transfer, x => x.transfer).DisposeWith(d);
                this.OneWayBind(ViewModel, x => x.enableBackup, x => x.backup.IsEnabled).DisposeWith(d);
                this.BindCommand(ViewModel, x => x.backup, x => x.backup).DisposeWith(d);

                SharedViewModel.singleton.WhenAnyValue(x => x.registrationsSinceBackup)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x =>
                    {
                        if (x == Config.Configuration.Settings.RegistrationsBeforeBackup)
                        {
                            SharedViewModel.singleton.activeViewModel = new BackupViewModel();
                        }

                    })
                    .DisposeWith(d);
            });
        }

        private void onInitialized(object sender, EventArgs e)
        {
            handle = new WindowInteropHelper(this).Handle;
            DisableSystemMenu();
            //DisableMaximizeButton();
            getDefaultPrinter();
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            //prevent shutdown with Alt F4
            if (e.Key == Key.System && e.SystemKey == Key.F4)
            {
                e.Handled = true;
            }
        }

        private IRoutableViewModel processBioLogin()
        {
            //todo: find way of hooking this up to the default handler.
            var bioUser = SharedViewModel.singleton.user;
            if(bioUser.type.Contains("DataEntryClerk"))
                return new OperatorHomeViewModel();
            else return new PresidingOfficerHomeViewModel();
        }

        private async Task getDefaultPrinter()
        {
            try
            {
                var printers = PrinterSettings.InstalledPrinters.OfType<string>().ToArray();
                if (!printers.Any()) {
                    SharedViewModel.singleton.printerName = "(none)";
                    return;
                }
                var permittedPrinters =
                    Config.Configuration.Settings.PermittedPrinters
                        ?.Split(',')
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToList();
                if (!permittedPrinters.Any()) {
                    SharedViewModel.singleton.printerName = printers.First();
                    return;
                }
                var printerFound = permittedPrinters
                    .FirstOrDefault(printers.Contains);
                SharedViewModel.singleton.printerName = printerFound ?? printers.First();
                ReportPrinterService.checkPrinter = () =>
                {
                    try
                    {
                        var ps = new PrintServer();
                        var queue = ps.GetPrintQueues().FirstOrDefault(x => x.Name == SharedViewModel.singleton.printerName);
                        if (queue == null) return false;
                        queue.Refresh();
                        SharedViewModel.singleton.printerOnline = !(queue.IsNotAvailable || queue.IsOffline);
                    }
                    catch
                    {
                        SharedViewModel.singleton.printerOnline = false;
                    }
                    return SharedViewModel.singleton.printerOnline;
                };
            }
            catch(Exception e)
            {
                logger.Error(e, $"Error getting list of printers: {e.GetBaseException().Message}");
                SharedViewModel.singleton.printerName = "(error)";
            }
        }
    }
}