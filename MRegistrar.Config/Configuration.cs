﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Config
{
    public class Configuration
    {
        public static Settings Settings { get; set; } = JsonConvert.DeserializeObject<Settings>
            (File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config.json")));
    }

    public class Settings
    {
        public string SetUpDriveLabelIdentifier { get; set; }
        public int BackUpDriveLabelLength { get; set; }
        public string BackUpDriveLabelIdentifier { get; set; }
        public bool UseSdCard { get; set; }
        public int RegistrationsBeforeBackup { get; set; }
        public string DeviceType { get; set; }
        public int TransferDriveLabelLength { get; set; }
        public string TransferDriveLabelIdentifier { get; set; }
        public bool EnableResourceUsageLogs { get; set; }
        public int MaxIdleSeconds { get; set; }
        public bool UseICAO { get; set; }

        public bool AllowDuplicateOverride { get; set; }
        public bool RequireFingerPrintDeviceConnected { get; set; }
        public string ECName { get; set; }
        public string AppTitle { get; set; }

        public bool UseBiometricLogin { get; set; }
        public long NationalIDId { get; set; }
        public long PassportId { get; set; }

        /// <summary>
        /// Not set by config. Must be updated from config
        /// </summary>
        public string PollingStationCode { get; set; } = "Unknown";
        public string SystemBackUpDriveLetter { get; set; }

        public string PermittedPrinters { get; set; }
        public bool RequirePrinterConnected { get; set; }
    }

    public static class USBUtils
    {
        public static List<USBDevice> GetUSBDevices()
        {
            List<USBDevice> devices = new List<USBDevice>();

            var drives = DriveInfo.GetDrives().Where(x=> x.DriveType == DriveType.Removable && x.IsReady);
            var cnt = 1;
            foreach (var device in drives)
            {
                devices.Add(new USBDevice(cnt, device.Name.Replace("\\", ""), 
                    device.RootDirectory.FullName, 
                    device.TotalSize, 
                    device.DriveType.ToString(), 
                    device.VolumeLabel, 
                    device.AvailableFreeSpace));
                cnt++;
            }

            return devices;
        }

        public static void CheckAuthenticityOfDrive(USBDevice drive)
        {
            ///original condition
            //var encryptedLabel = Securities.ComputeSha256Hash(drive.Label + "_" + drive.Size.ToString());
            //var files = Directory.GetFiles(drive.Root, "*.txt", SearchOption.AllDirectories);
            //if (files.FirstOrDefault(x => x.Contains(encryptedLabel)) == null) throw new Exception("The selected drive did not pass one of our verifications.\nPlease check and try again.");
            ///fail if the drive contains any files
            var hasFiles = Directory.GetFiles(drive.Root).Any();
            if (hasFiles) throw new Exception("Only Empty Drives can be used for Transfer. Format Drive and try again");
        }

        public static bool isSetupDeviceConnection()
        {
            var usbDevices = GetUSBDevices().ToList();
            return usbDevices.Any(x => x.Label.ToLower().Contains(Config.Configuration.Settings.SetUpDriveLabelIdentifier.ToLower()));
        }

        //public static void CopyDriveContents(USBDevice drive)
        //{
        //    var backupFolders = JsonConvert.DeserializeObject<List<BackupFolder>>
        //        (File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CopiedFiles", "folderslog.json")));

        //    var now = DateTime.Now;
        //    var folderName = drive.Label + "_" + now.ToFileTimeUtc();
        //    var filesFolder = Directory.GetCurrentDirectory();
        //    var newFolder = Path.Combine(filesFolder, "CopiedFiles", folderName);

        //    var files = Directory.GetFiles(drive.Root, "*.xml", SearchOption.AllDirectories);
        //    if (!files.Any()) throw new Exception("The selected drive does not have any registration data.\nPlease check and try again.");

        //    Directory.CreateDirectory(newFolder);
        //    var backupFiles = new List<BackupFile>();
        //    foreach (var f in files)
        //    {
        //        var fn = Path.GetFileName(f);
        //        if (fn.Length < 64) continue;
        //        File.Copy(f, Path.Combine(newFolder, fn), true);
        //        backupFiles.Add(new BackupFile
        //        {
        //            Copied = DateTime.Now,
        //            Name = fn,
        //            Status = BackupFileStatus.New
        //        });
        //    }

        //    backupFolders.Add(new BackupFolder
        //    {
        //        Name = folderName,
        //        Created = now,
        //        Status = BackupFolderStatus.New
        //    });
        //    string backupFoldersJson = JsonConvert.SerializeObject(backupFolders.ToArray());
        //    File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CopiedFiles", "folderslog.json"), backupFoldersJson);
        //    string backupFilesJson = JsonConvert.SerializeObject(backupFiles.ToArray());
        //    File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CopiedFiles", folderName, "fileslog.json"), backupFilesJson);
        //}


    }

    public class USBDeviceInfo
    {
        public USBDeviceInfo(int num, string deviceID, string pnpDeviceID, string description)
        {
            Number = num;
            DeviceID = deviceID;
            PnpDeviceID = pnpDeviceID;
            Description = description;
        }
        public int Number { get; private set; }
        public string DeviceID { get; private set; }
        public string PnpDeviceID { get; private set; }
        public string Description { get; private set; }
    }

    public class USBDevice
    {
        public USBDevice(int num, string name, string root, long size, string type, string label, long availableSpace = 0)
        {
            Number = num;
            Name = name;
            Root = root;
            Size = size;
            Type = type;
            Label = label;
            AvailableSpace = availableSpace;
        }
        public int Number { get; private set; }
        public string Name { get; private set; }
        public string Root { get; private set; }
        public long Size { get; private set; }
        public string Type { get; private set; }
        public string Label { get; private set; }
        public long AvailableSpace { get; private set; }
    }

    public static class DeviceUtils
    {
        public static string GetDeviceSerial()
        {
            var results = new List<string>();

            string query = "SELECT * FROM Win32_BaseBoard";
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher(query);
            foreach (ManagementObject info in searcher.Get())
            {
                results.Add(info.GetPropertyValue("SerialNumber").ToString());
            }
            if (!results.Any()) return "";
            return results.First();
        }



    }
}
