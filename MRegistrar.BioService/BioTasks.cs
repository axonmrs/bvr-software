﻿
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.IO;
using Neurotec.Images;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using MRegistrar.Data;


namespace MRegistrar.BioService
{
    public class BiometricTask
    {
        public NBiometricClient BiometricClient { get; set; }
        public NSubject Subject { get; set; }

        public BiometricTask()
        {
            BiometricClient = State.singleton.client;
        }

        public BiometricTask(NBiometricClient client)
        {
            BiometricClient = client;
        }

        private NSubject CreateSubject(Dictionary<NFPosition, NImage> nImages, string subjectId)
        {
            NSubject subject = new NSubject();
            try
            {
                var nF = new NFinger();
                foreach (var item in nImages)
                {
                    var finger = new NFinger { Image = item.Value, Position = item.Key };
                    subject.Fingers.Add(finger);
                }
                subject.Id = subjectId;
                return subject;
            }
            catch (Exception e)
            {
                return subject;
            }
        }
        private NSubject CreateSubject(List<NImage> nImages, string subjectId)
        {
            NSubject subject = new NSubject();
            try
            {
                var nF = new NFinger();
                foreach (var item in nImages)
                {
                    var finger = new NFinger { Image = item };
                    subject.Fingers.Add(finger);
                }
                subject.Id = subjectId;
                return subject;
            }
            catch (Exception e)
            {
                return subject;
            }
        }
        public NSubject CreateSubject(Dictionary<NFPosition, NImage> fingers, NImage faceImg = null)
        {
            if (fingers == null && faceImg == null) throw new Exception("finger or face required for verification");
            NSubject subject = new NSubject();
            try
            {
                //var nF = new NFinger();
                foreach (var item in fingers)
                {
                    var finger = new NFinger { Image = item.Value, Position = item.Key };
                    subject.Fingers.Add(finger);
                }
                //set new Subject Face
                if (faceImg != null)
                {
                    var face = new NFace { Image = faceImg };
                    subject.Faces.Add(face);
                }

                return subject;
            }
            catch (Exception e)
            {
                return subject;
            }
        }

        public NSubject CreateSubject(List<NImage> fingerImgs, NImage faceImg)
        {
            if (fingerImgs == null || faceImg == null) throw new Exception("Finger and Face biometric data required");
            NSubject subject = new NSubject();
            try
            {
                if (fingerImgs != null)
                {
                    foreach (var item in fingerImgs)
                    {
                        var finger = new NFinger { Image = item };
                        subject.Fingers.Add(finger);
                    }
                }

                //set new Subject Face
                var face = new NFace { Image = faceImg };
                subject.Faces.Add(face);

                return subject;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }


        private NSubject CreateEnrollmentSubject(Dictionary<NFPosition, NImage> fingerImgs, NImage faceImg, string subjectId)
        {
            NSubject subject = new NSubject();
            try
            {
                var nFin = new NFinger();
                var nF = new NFace();

                //set new Subject Fingers
                foreach (var item in fingerImgs)
                {
                    var finger = new NFinger { Image = item.Value, Position = item.Key };
                    subject.Fingers.Add(finger);
                }
                //set new Subject Face
                if (faceImg == null)
                {
                    subject.Id = subjectId;
                    return subject;
                }
                else
                {
                    //throw new Exception("Face biometric data required for enrollment");
                    var face = new NFace { Image = faceImg };
                    subject.Faces.Add(face);
                    subject.Id = subjectId;
                    return subject;
                }
            }
            catch (Exception e)
            {
                return subject;
            }
        }
        //private NSubject CreateEnrollmentSubject(Dictionary<NFPosition, NImage> fingerImgs,string subjectId, NImage faceImg = null)
        //{
        //    NSubject subject = new NSubject();
        //    try
        //    {
        //        var nFin = new NFinger();
        //        var nF = new NFace();

        //        //set new Subject Fingers
        //        foreach (var item in fingerImgs)
        //        {
        //            var finger = new NFinger { Image = item.Value, Position = item.Key };
        //            subject.Fingers.Add(finger);
        //        }
        //        //set new Subject Face
        //        if (faceImg == null)
        //        {
        //            subject.Id = subjectId;
        //            return subject;
        //        }
        //        else
        //        {
        //            var face = new NFace { Image = faceImg };
        //            subject.Faces.Add(face);
        //            subject.Id = subjectId;
        //            return subject;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return subject;
        //    }
        //}

        public async Task<(bool success, string message, string voterId, NSubject createdSubject)> IdentifySubjectAsync(Dictionary<NFPosition, NImage> fingers, NImage faceImg = null)
        {
            try
            {
                var subject = CreateSubject(fingers, faceImg);
                var identifyTask = BiometricClient.CreateTask(NBiometricOperations.Identify, subject);

                await BiometricClient.PerformTaskAsync(identifyTask);
                NBiometricStatus status = identifyTask.Status;
                if (status != NBiometricStatus.Ok)
                {
                    if (identifyTask.Error != null)
                    {
                        //Utilities.ShowError(identifyTask.Error);
                        return (false, identifyTask.Error.GetBaseException().Message, null,subject);
                    }
                    switch (identifyTask.Status)
                    {
                        case NBiometricStatus.MatchNotFound:
                            return (false, "No Match Found", null,subject);
                        default:
                            return (false, identifyTask.Status.ToString(), null,subject);
                    }
                }
                else if (subject.MatchingResults.Count > 0)
                {
                    foreach (var matchingResult in subject.MatchingResults)
                    {
                        var matchingSubject = matchingResult.Subject;
                        //matchingResult.MatchingDetails.
                        return (true, $"Matched with Id:{matchingResult.Id}, with score: {matchingResult.Score}", matchingResult.Id,subject);
                    }
                    return (true, "", "",subject);
                }
                return (false, $"No Match Found", null,subject);


            }
            catch (Exception e)
            {
                ////todo::log error here
                return (false, $"MatchNotFound {e.GetBaseException().Message}", null,null);
            }
        }
        public async Task<(bool success, string message, string voterId)> IdentifySubjectAsync(NSubject subject)
        {
            try
            {
                using (var identifyTask = BiometricClient.CreateTask(NBiometricOperations.Identify, subject))
                {

                    await BiometricClient.PerformTaskAsync(identifyTask);
                    var status = identifyTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        if (identifyTask.Error != null)
                        {
                            //Utilities.ShowError(identifyTask.Error);
                            return (false, identifyTask.Error.GetBaseException().Message, null);
                        }
                        switch (identifyTask.Status)
                        {
                            case NBiometricStatus.MatchNotFound:
                                return (false, "No Match Found", null);
                            default:
                                return (false, identifyTask.Status.ToString(), null);
                        }
                    }
                    else if (subject.MatchingResults.Count > 0)
                    {
                        foreach (var matchingResult in subject.MatchingResults)
                        {
                            var matchingSubject = matchingResult.Subject;
                            //matchingResult.MatchingDetails.
                            return (true, $"Matched with Id:{matchingResult.Id}, with score: {matchingResult.Score}", matchingResult.Id);
                        }
                        return (true, "", "");
                    }
                    return (false, $"No Match Found", null);
                }
            }
            catch (Exception e)
            {
                ////todo::log error here
                return (false, $"MatchNotFound {e.GetBaseException().Message}", null);
            }
        }
        public async Task<(bool success, string message)> EnrollSubjectAsync(Dictionary<NFPosition, NImage> nFingerImgs, NImage nFace, string subjectId)
        {
            try
            {
                using(var subject = CreateEnrollmentSubject(nFingerImgs, nFace, subjectId))
                using (var enrollTask = BiometricClient.CreateTask(NBiometricOperations.Enroll, subject))
                {
                    await BiometricClient.PerformTaskAsync(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        if (enrollTask.Error != null)
                        {
                            //todo::log technical error and display friendly error message
                            return (false, enrollTask.Error.GetBaseException().Message);
                        }
                        else
                        {
                            switch (enrollTask.Status)
                            {
                                case NBiometricStatus.DuplicateId:
                                    return (false, enrollTask.Status.ToString());
                                case NBiometricStatus.DuplicateFound:
                                    return (false, enrollTask.Status.ToString());
                                default:
                                    return (false, enrollTask.Status.ToString());
                            }
                        }
                    }
                    else
                        return (true, "Subject Enrolled successfully");
                }
            }
            catch (Exception e)
            {
                return (true, $"Enroll Failed.{e.GetBaseException().Message}");
            }
        }
        public async Task<(bool success, string message)> EnrollSubjectAsync(Dictionary<NFPosition, NImage> nFingerImgs, string subjectId, NImage nFace = null)
        {
            try
            {
                var subject = CreateEnrollmentSubject(nFingerImgs, nFace, subjectId);
                var dbFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data\\user_auth_db.db");
                using (var BiometricClient = new NBiometricClient())
                {
                    BiometricClient.SetDatabaseConnectionToSQLite(dbFilePath);
                    var enrollTask = BiometricClient.CreateTask(NBiometricOperations.Enroll, subject);

                    await BiometricClient.PerformTaskAsync(enrollTask);
                    NBiometricStatus status = enrollTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        if (enrollTask.Error != null)
                        {
                            //todo::log technical error and display friendly error message
                            return (false, enrollTask.Error.GetBaseException().Message);
                        }
                        else
                        {
                            switch (enrollTask.Status)
                            {
                                case NBiometricStatus.DuplicateId:
                                    return (false, enrollTask.Status.ToString());
                                case NBiometricStatus.DuplicateFound:
                                    return (false, enrollTask.Status.ToString());
                                default:
                                    return (false, enrollTask.Status.ToString());
                            }
                        }
                    }
                    else
                        return (true, " registered successfully");
                }
            }
            catch (Exception e)
            {
                return (true, $"Enroll Failed.{e.GetBaseException().Message}");
            }
        }
        public async Task<(bool success, string message)> DeleteSubjectAsync(Dictionary<NFPosition, NImage> nImages, string subjectId)
        {
            try
            {
                using (var subject = CreateSubject(nImages, subjectId))
                using (var deleteTask = BiometricClient.CreateTask(NBiometricOperations.Delete, subject))
                {
                   await BiometricClient.PerformTaskAsync(deleteTask);
                    NBiometricStatus status = deleteTask.Status;
                    if (status != NBiometricStatus.Ok)
                    {
                        if (deleteTask.Error != null)
                        {
                            //todo::log technical error and display friendly error message
                            return (false, deleteTask.Error.GetBaseException().Message);
                        }
                        else
                        {
                            switch (deleteTask.Status)
                            {
                                case NBiometricStatus.DuplicateId:
                                    return (false, deleteTask.Status.ToString());
                                case NBiometricStatus.DuplicateFound:
                                    return (false, deleteTask.Status.ToString());
                                default:
                                    return (false, deleteTask.Status.ToString());
                            }
                        }
                    }
                    else
                        return (true, "Subject deleted successfully");
                }
            }
            catch (Exception e)
            {
                return (true, $"delete Failed.{e.GetBaseException().Message}");
            }
        }

        public static SQLiteConnection DbConnection()
        {
            //var filePath = AppDomain.CurrentDomain.BaseDirectory + "mregister.db";
            var filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\registration_db.db";
            var conn = new SQLiteConnection($"Data Source={filePath};Version=3;", false);
            conn.Open();
            return conn;
        }

        public string CreateUser(string roName,string roUserName,string userType)
        {
            using (var con = DbConnection())
            using (var com = new SQLiteCommand("INSERT INTO Users(name,username,type,password_hash,is_active,selected) " +
                " VALUES(@name,@username,@type,@password_hash,@is_active,@selected);SELECT last_insert_rowid(); ", con))
            {
                if (userType.Contains("DataEntryClerk")) userType = "DataEntryClerk";
                else userType = "RegistrationOfficer";

                com.Parameters.AddWithValue("@name",roName);
                com.Parameters.AddWithValue("@username", roUserName);
                com.Parameters.AddWithValue("@type", userType);
                com.Parameters.AddWithValue("@password_hash", "onHoldForNow");
                com.Parameters.AddWithValue("@is_active", true);
                com.Parameters.AddWithValue("@selected", true);
                
                var retId = com.ExecuteScalar();
                return retId.ToString();
            }
        }
        public (int id,string userName,string name, string role,bool status) SelectUser(int id)
        {
            using (var con = DbConnection())
            using (var com = new SQLiteCommand("select * from users where id="+ id, con))
            {
                var reader = com.ExecuteReader();
                if (reader.Read())
                {
                    return ((int)reader["id"], (string)reader["username"], (string)reader["name"], (string)reader["type"], (bool)reader["is_active"]);
                }
            }
            return (0, null, null, null, false);
        }
        public void DeleteUser(int id)
        {
            using (var con = DbConnection())
            using (var com = new SQLiteCommand("Delete from  users where id =" + id , con))
            {
                com.ExecuteNonQuery();
            }
        }

        public (bool success, string message, string userId) IdentifySubject(byte[] finger)
        {
            try
            {
                var subject = new NSubject();
                subject.SetTemplate(new NTemplate(finger));

                var identifyTask = BiometricClient.CreateTask(NBiometricOperations.Identify, subject);

                BiometricClient.PerformTaskAsync(identifyTask);
                NBiometricStatus status = identifyTask.Status;
                if (status != NBiometricStatus.Ok)
                {
                    if (identifyTask.Error != null)
                    {
                        //Utilities.ShowError(identifyTask.Error);
                        return (false, identifyTask.Error.GetBaseException().Message, null);
                    }
                    switch (identifyTask.Status)
                    {
                        case NBiometricStatus.MatchNotFound:
                            return (false, "No Match Found", null);
                        default:
                            return (false, identifyTask.Status.ToString(), null);
                    }
                }
                else if (subject.MatchingResults.Count > 0)
                {
                    foreach (var matchingResult in subject.MatchingResults)
                    {
                        var matchingSubject = matchingResult.Subject;
                        //matchingResult.MatchingDetails.
                        return (true, $"Matched with Id:{matchingResult.Id}, with score: {matchingResult.Score}", matchingResult.Id);
                    }
                    return (true, "", "");
                }
                return (false, $"No Match Found", null);
            }
            catch (Exception e)
            {
                ////todo::log error here
                return (false, $"MatchNotFound {e.GetBaseException().Message}", null);
            }
        }
        public static async Task<(bool successful, string message)> createTaskAsync(NBiometricClient client, NSubject subject)
        {
            using (var task = client.CreateTask(NBiometricOperations.Enroll, subject))
            {
                await client.PerformTaskAsync(task);
                var status = task.Status;
                if (status != NBiometricStatus.Ok)
                {
                    if (task.Error != null)
                    {
                        //todo::log technical error and display friendly error message
                        return (false, task.Error.GetBaseException().Message);
                    }
                    else
                    {
                        switch (task.Status)
                        {
                            case NBiometricStatus.DuplicateId:
                                return (false, task.Status.ToString());
                            case NBiometricStatus.DuplicateFound:
                                return (false, task.Status.ToString());
                            default:
                                return (false, task.Status.ToString());
                        }
                    }
                }
                else
                    return (true, "subject enrolled successfully");
            }
        }


    }
}