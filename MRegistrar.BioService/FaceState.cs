﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using NLog;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class FaceState
    {
        NFace _face;
        NSubject _subject;
        public event EventHandler<FaceStatePropertyChangedEventArgs> onPropertyChange;
        public NBiometricClient client { private get; set; }
        private NFace currentFace { get; set; }
        private Logger logger { get; } = LogManager.GetCurrentClassLogger();

        private int faceIndex => Config.Configuration.Settings.UseICAO ? 1 : 0;

        public void init()
        {
            client.CurrentBiometricCompletedTimeout = 5000;
            onFaceCaptureDeviceChanged();
        }

        public void cleanUp()
        {
            try { client.Cancel(); } catch { }
            try
            {
                if (currentFace != null) currentFace.PropertyChanged -= onFacePropertyChanged;
                client.PropertyChanged -= onClientPropertyChanged;
                client.CurrentBiometricCompleted -= onClientCurrentBiometricCompleted;
            }
            catch { }
        }

        public async Task beginCapture()
        {
            notifyChanged(FaceProperty.BeginCapture, true);
            //setup
            _face = new NFace { CaptureOptions = Config.Configuration.Settings.UseICAO ? NBiometricCaptureOptions.Stream : NBiometricCaptureOptions.Manual };
            _subject = new NSubject();
            _subject.Faces.Add(_face);
            //setup event handlers
            client.PropertyChanged += onClientPropertyChanged;
            client.CurrentBiometricCompleted += onClientCurrentBiometricCompleted;
            notifyChanged(FaceProperty.NewFaceForCapture, _face);
            var operations = NBiometricOperations.Capture | NBiometricOperations.CreateTemplate;
            if (Config.Configuration.Settings.UseICAO) operations |= NBiometricOperations.Segment;
            var task = client.CreateTask(operations, _subject);
            notifyChanged(FaceProperty.Capturing, true);
            try
            {
                var result = await client.PerformTaskAsync(task);
                notifyChanged(FaceProperty.CaptureStatus, result.Status);
                if (result.Error != null) notifyChanged(FaceProperty.CapturingFailed, result.Error);
                if (result.Status == NBiometricStatus.Ok)
                    notifyChanged(FaceProperty.CaptureComplete, result.Status);
            }
            catch (Exception e)
            {
                notifyChanged(FaceProperty.CapturingFailed, e);
            }
            //remove event handlers
            client.PropertyChanged -= onClientPropertyChanged;
            client.CurrentBiometricCompleted -= onClientCurrentBiometricCompleted;
            notifyChanged(FaceProperty.Capturing, false);
        }
        public byte[] faceJ2KAsBytes()
        {
            var face = _subject.Faces[faceIndex];
            if (face == null) return null;
            return face.Image.Save(NImageFormat.Jpeg2K).ToArray();
        }
        public byte[] faceJpegAsBytes()
        {
            var face = _subject.Faces[faceIndex];
            if (face == null) return null;
            return face.Image.Save(NImageFormat.Png).ToArray();
        }
        public NFace capturedFace() => _subject.Faces[faceIndex];
        public byte[] faceTemplate() => _subject.GetTemplateBuffer().ToArray();
        public NImage faceAsNimage() => _subject.Faces.FirstOrDefault()?.Image;

        private void onClientCurrentBiometricCompleted(object sender, EventArgs e)
        {
            if (client.CurrentBiometric.BiometricType == NBiometricType.Finger) return;

            var face = (NFace)client.CurrentBiometric;
            var status = face.Status;
            if (status == NBiometricStatus.Ok)
            {
                var child = face.Objects.FirstOrDefault()?.Child;
                if (child != null && child.Status != NBiometricStatus.Ok)
                    status = child.Status;
            }
            if (status == NBiometricStatus.Ok)
                client.Force();
        }

        private void onClientPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                switch (e.PropertyName)
                {
                    case "FaceCaptureDevice":
                        onFaceCaptureDeviceChanged();
                        break;
                    case "CurrentBiometric":
                        //if (client.CurrentBiometric.BiometricType == NBiometricType.Finger) return;
                        var face = (NFace)client.CurrentBiometric;
                        if (currentFace != null) currentFace.PropertyChanged -= onFacePropertyChanged;
                        currentFace = face;
                        if (currentFace != null) currentFace.PropertyChanged -= onFacePropertyChanged;
                        notifyChanged(FaceProperty.NewFace, true);
                        break;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

        }

        private void onFacePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Status":
                    notifyChanged(FaceProperty.FaceStatus, currentFace?.Status);
                    break;
            }
        }

        private void onFaceCaptureDeviceChanged()
        {
            notifyChanged(FaceProperty.FaceDeviceOk, client.FaceCaptureDevice?.IsAvailable != true);
        }

        private void notifyChanged(FaceProperty property, object value)
        {
            var args = new FaceStatePropertyChangedEventArgs(property, value);
            onPropertyChange?.Invoke(this, args);
        }
    }
}
