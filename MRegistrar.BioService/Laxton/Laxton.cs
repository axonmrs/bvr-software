﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService.Laxton
{
    public class SerialPortModel
    {
        public string Name { get; set; }
        public string Com { get; set; }
    }
    public static class Laxton
    {
        private static void runApp(string filename) {
            try
            {
                var psi = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    FileName = filename,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardOutput = true
                };
                using (var exeProcess = Process.Start(psi))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch { }
        }

        public static void cameraFlashOn()
        {
            runApp("laxton\\on.exe");
        }

        public static void cameraFlashOff()
        {
            runApp("laxton\\off.exe");
        }
    }
}
