﻿using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Images;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class FingerState
    {
        public event EventHandler<FingerStatePropertyChangedEventArgs> onPropertyChange;
        public NBiometricClient client { get; set; }
        public NFinger subjectFinger { get; private set; }
        public NSubject subject { get; private set; }
        public bool active { get; set; }
        private Logger _logger = LogManager.GetCurrentClassLogger();

        //private Dictionary<NFPosition, NImage> fingersForTemplatesTable;

        public void cleanUp()
        {
            try { client.Cancel();  } catch { }
            if (subjectFinger != null) 
            {
                subjectFinger.PropertyChanged -= onFingerPropertyChanged;
                try { subjectFinger.Image = null; } catch { } 
                subjectFinger = null;
            }
            if (subject != null) 
            {
                try { subject.Dispose(); } catch { }
                subject = null;
            }
        }
        public async Task beginCapture(NFPosition position, NFPosition[] missingFingers)
        {
            //cleanup
            cleanUp();
            notifyChanged(FingerProperty.BeginCapture, true);
            //add finger
            subjectFinger = new NFinger
            {
                Position = position,
                CaptureOptions = NBiometricCaptureOptions.Stream
            };
            subject = new NSubject();
            if (missingFingers != null)
                foreach (var missingFinger in missingFingers)
                    subject.MissingFingers.Add(missingFinger);
            subject.Fingers.Add(subjectFinger);
            subjectFinger.PropertyChanged += onFingerPropertyChanged;
            notifyChanged(FingerProperty.SubjectFingerInitialized, subjectFinger);
            //capture
            client.FingersReturnBinarizedImage = false;
            //var operations = NBiometricOperations.Segment | NBiometricOperations.Capture | NBiometricOperations.CreateTemplate | NBiometricOperations.AssessQuality;
            var operations = NBiometricOperations.Capture | NBiometricOperations.CreateTemplate;
            if (client.FingersCalculateNfiq) operations |= NBiometricOperations.AssessQuality;
            var task = client.CreateTask(operations, subject);
            try
            {
                notifyChanged(FingerProperty.Capturing, true);
                var result = await client.PerformTaskAsync(task);
                notifyChanged(FingerProperty.CaptureStatus, result.Status);
                if (result.Error != null) notifyChanged(FingerProperty.CaptureFailed, result.Error);
                if (result.Status == NBiometricStatus.Ok)
                    notifyChanged(FingerProperty.CaptureComplete, result.Status);
                else notifyChanged(FingerProperty.CaptureFailedStatus, result.Status);
            }
            catch (Exception e)
            {
                notifyChanged(FingerProperty.CaptureFailed, e);
            }
        }

        private void onFingerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Status":
                    _logger.Trace ($"Status Changed: {subjectFinger?.Status}");
                    notifyChanged(FingerProperty.CaptureStatus, subjectFinger.Status);
                    break;
            }
        }

        private void notifyChanged(FingerProperty property, object value)
        {
            var args = new FingerStatePropertyChangedEventArgs(property, value);
            onPropertyChange?.Invoke(this, args);
        }

        public byte[] getFingerAsWSQ()
        {
            if (subject.Status == NBiometricStatus.Ok)
                return subjectFinger.Image.Save(NImageFormat.Wsq).ToArray();
            return null;
        }
        public byte[] getFingerTemplate()
        {
            if (subject.Status == NBiometricStatus.Ok)
                return subject.GetTemplateBuffer().ToArray();
            return null;
        }
        public List<(NFPosition position, byte[] template, byte[] wsq,NImage fingerImage)> getFingerAndTemplates()
        {
            return
                subject.Fingers
                    .Where(x => x?.Image != null)
                    .Where(x => !x.Position.ToString().Contains("PlainLeftFourFingers"))
                    .Where(x => !x.Position.ToString().Contains("PlainRightFourFingers"))
                    .Where(x => !x.Position.ToString().Contains("PlainThumbs"))
                    .Select(x =>
                    {
                        var pos = x.Position;
                        var fingerImg = x.Image;
                        var wsq = fingerImg.Save(NImageFormat.Wsq).ToArray();
                        //fingersForTemplatesTable.Add(pos, fingerImg);

                        var newFinger = new NFinger
                        {
                            Image = fingerImg
                        };
                        var newSubject = new NSubject();
                        newSubject.Fingers.Add(newFinger);

                        var tpl = newSubject.Fingers.FirstOrDefault().Image.Save().ToArray();
                        return (pos, tpl, wsq,fingerImg);
                    })
                    .ToList();
        }
       
        public (NFPosition position,NImage fingerImg) getFingerImage()
        {
            if (subject.Status == NBiometricStatus.Ok)
            {
                var finger = subject.Fingers.FirstOrDefault();
                return (finger.Position, finger.Image);
            }
            return (NFPosition.Unknown, null);
        }

        //public Dictionary<NFPosition, NImage> getFingerImages()
        //{
        //    return fingersForTemplatesTable;
        //}

    }
}
