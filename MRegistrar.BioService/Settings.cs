﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class Settings
    {
        public string selectedFScannerId { get; set; }
        public bool scanSlaps { get; set; }
        public bool scanRolled { get; set; }
        public bool scanPlain { get; set; }
        public bool showOriginal { get; set; }
        public string information { get; set; }
        public string informationThumbnailField { get; set; }
        public string clientProperties { get; set; }

        public bool allowDuplicateOverride { get; set; }

        public static Settings @default { get; } = new Settings
        {
            scanPlain = true,
            scanRolled = true,
            scanSlaps = true,
            showOriginal = true,
            informationThumbnailField = "Thumbnail",
            clientProperties = "",
            selectedFScannerId = "",
            information = "Key = 'Thumbnail', IsThumbnail = 'True', Editable = 'False' ;Key = 'Name' ;Key = 'Middle Name';Key = 'Last Name' ;Key = 'National Id'; Key = 'Nationality'",
            allowDuplicateOverride = true
        };

        public InfoField[] loadInfoFields()
        {
            return
                information.Split(';')
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Select(x => new InfoField(x))
                    .ToArray();
        }
    }
}
