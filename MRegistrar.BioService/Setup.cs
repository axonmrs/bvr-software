﻿using Neurotec;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Devices;
using Neurotec.Licensing;
using NLog;
using System;
using System.Linq;

namespace MRegistrar.BioService
{
    public class Setup
    {
        static Logger logger = LogManager.GetCurrentClassLogger();

        public static (bool successful, string message) init()
        {
            var state = State.singleton;
            //check the license
            //state.licenseStatus = checkLicense(); //is this necessary?
            //if (!state.licenseStatus.successful) return (false, state.licenseStatus.message);
            logger.Trace("Creating client");
            state.client = createClient();
            logger.Trace("Done creating client");
            //set initial device info
            if (state.client.FingerScanner != null)
                state.onSelectedDeviceChanged(state.client.FingerScanner);
            return (true, "Client initialized");
        }
        //private static (bool, string) checkLicense()
        //{
        //    //var license = "FaceMatcher,FaceExtractor,FaceClient,FingerClient,FingerExtractor,FingerMatcher";
        //    var license = "FaceMatcher,FaceClient,FingerClient,FingerMatcher";
        //    try
        //    {
        //        if (!NLicense.Obtain("/local", 5000, license))
        //        {
        //            return (false, $"Could not obtain licenses: {license}");
        //        }
        //        return (true, "Licenses obtained");
        //    }
        //    catch (Exception e)
        //    {
        //        string message = $"Failed to obtain licenses for components.\nError message: {e.GetBaseException().Message}";
        //        if (e is IOException)
        //        {
        //            message += "\n(Probably licensing service is not running. Use Activation Wizard to figure it out.)";
        //        }
        //        return (false, message);
        //    }
        //}
        private static NBiometricClient createClient()
        {
            var client = new NBiometricClient();
            var propertyBag = NPropertyBag.Parse(Settings.@default.clientProperties);
            propertyBag.ApplyTo(client);

            var dbFilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\registration_db.db";
            client.SetDatabaseConnectionToSQLite(dbFilePath);

            client.LocalOperations = NBiometricOperations.All;
            client.BiometricTypes = NBiometricType.Finger | NBiometricType.Face;
            client.FingersReturnBinarizedImage = false;
            client.FacesCheckIcaoCompliance = Config.Configuration.Settings.UseICAO;
            //client.FacesIcaoRemoveBackground = true;
            //client.FacesIcaoRemoveRedEye = true;
            client.UseDeviceManager = true;
            client.FingersCalculateNfiq = NLicense.IsComponentActivated("Biometrics.FingerQualityAssessmentBase");

            client.Initialize();
            client.PropertyChanged += onClientPropertyChanged;
            
            if (!string.IsNullOrEmpty(Settings.@default.selectedFScannerId) &&
                client.DeviceManager.Devices.Contains(Settings.@default.selectedFScannerId))
                client.FingerScanner = (NFScanner)client.DeviceManager.Devices[Settings.@default.selectedFScannerId];
            else
            {
                //set the default to the first finger device find
                var firstScanner = client.DeviceManager.Devices.OfType<NFScanner>().FirstOrDefault();
                if (firstScanner != null)
                    client.FingerScanner = firstScanner;
            }
            //if (client.FingerScanner.IsSpoofDetectionSupported)
            //    client.FingerScanner.SpoofDetection = true;
            return client;
        }

        public static void recreateClient()
        {
            if (State.singleton.clientReferenceId == 0 && State.singleton.client != null) return;
            if (State.singleton.client != null) return;
            destroyClient(State.singleton.client);
            var (successful, message) = init();
            if (!successful)
            {
                logger.Error($"Error recreating client: {message}");
            }
            if (successful) State.singleton.clearClientUsage();
        }
        public static void destroyClient(NBiometricClient client)
        {
            if (client == null) return;
            try
            {
                client.PropertyChanged -= onClientPropertyChanged;
                client.Cancel();
                client.Dispose();
            }
            catch(Exception e)
            {
                logger.Error(e, $"Error destroying client: {e.GetBaseException().Message}");
            }
        }

        //public  NBiometricClient createClient1()
        //{
        //    var client = new NBiometricClient();
        //    var propertyBag = NPropertyBag.Parse(Settings.@default.clientProperties);
        //    propertyBag.ApplyTo(client);

        //    var dbFilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\registration_db.db";
        //    client.SetDatabaseConnectionToSQLite(dbFilePath);

        //    client.LocalOperations = NBiometricOperations.All;
        //    client.BiometricTypes = NBiometricType.Finger | NBiometricType.Face;
        //    client.FingersReturnBinarizedImage = false;
        //    client.UseDeviceManager = true;
        //    client.FingersCalculateNfiq = NLicense.IsComponentActivated("Biometrics.FingerQualityAssessmentBase");

        //    client.Initialize();
        //    client.PropertyChanged += onClientPropertyChanged;

        //    if (!string.IsNullOrEmpty(Settings.@default.selectedFScannerId) &&
        //        client.DeviceManager.Devices.Contains(Settings.@default.selectedFScannerId))
        //        client.FingerScanner = (NFScanner)client.DeviceManager.Devices[Settings.@default.selectedFScannerId];
        //    else
        //    {
        //        //set the default to the first finger device find
        //        var firstScanner = client.DeviceManager.Devices.OfType<NFScanner>().FirstOrDefault();
        //        if (firstScanner != null)
        //            client.FingerScanner = firstScanner;
        //    }
        //    //if (client.FingerScanner.IsSpoofDetectionSupported)
        //    //    client.FingerScanner.SpoofDetection = true;
        //    return client;
        //}

        private static void onClientPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                switch (e.PropertyName)
                {
                    case "FingerScanner":
                        //todo::Error occures here.
                        State.singleton.onSelectedDeviceChanged(State.singleton.client.FingerScanner);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //log exception with Nlog here
            }
        }
    }
}
