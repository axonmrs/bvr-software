﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class FingerStatePropertyChangedEventArgs : EventArgs
    {
        public FingerProperty property { get; }
        public object value { get; }

        public FingerStatePropertyChangedEventArgs(FingerProperty property, object value)
        {
            this.property = property;
            this.value = value;
        }
    }

    public enum FingerProperty
    {
        CaptureFailed,
        CaptureComplete,
        CaptureFailedStatus,
        CaptureStatus,
        Capturing,
        BeginCapture,
        SubjectFingerInitialized
    }
}
