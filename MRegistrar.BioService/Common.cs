﻿using Neurotec.Biometrics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public static class Common 
    {
        public static readonly NFPosition[] slaps = new NFPosition[]
        {
            NFPosition.PlainLeftFourFingers,
            NFPosition.PlainRightFourFingers,
            NFPosition.PlainThumbs,
        };

        public static readonly NFPosition[] fingers = new NFPosition[]
        {
            NFPosition.LeftLittle,
            NFPosition.LeftRing,
            NFPosition.LeftMiddle,
            NFPosition.LeftIndex,
            NFPosition.LeftThumb,
            NFPosition.RightThumb,
            NFPosition.RightIndex,
            NFPosition.RightMiddle,
            NFPosition.RightRing,
            NFPosition.RightLittle,
        };

        public static Dictionary<K, T> Merge<K, T>(this Dictionary<K, T> x, Dictionary<K, T> other)
        {
            foreach (var kvp in other)
            {
                x[kvp.Key] = kvp.Value;
            }
            return x;
        }
    }
}
