﻿using MRegistrar.Data;
using Neurotec;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Devices;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class State
    {
        public static State singleton { get; } = new State();
        public (bool successful, string message) licenseStatus { get; internal set; }
        public List<NFScanner> fingerScanners { get; internal set; }
        public List<NCamera> cameras { get; internal set; }
        public NBiometricClient client { get; internal set; }
        public NBiometricClient staffClient { get; internal set; }
        ///// <summary>
        ///// Used to log when the client is used. This is so we do not recreate the client if not used
        ///// </summary>
        //public int clientReferenceId { get; private set; }

        public bool canCaptureSlaps { get; internal set; }
        public bool canCaptureRolled { get; internal set; }

        public void updateDeviceList()
        {
            var devices = client.DeviceManager.Devices.ToList();
            fingerScanners = devices.OfType<NFScanner>().ToList();
            cameras = devices.OfType<NCamera>().ToList();
        }

        public void onSelectedDeviceChanged(NFScanner newDevice)
        {
            if (newDevice != null)
            {
                canCaptureSlaps =
                    newDevice.GetSupportedPositions() 
                        .Any(NBiometricTypes.IsPositionFourFingers);
                canCaptureRolled =
                    newDevice.GetSupportedImpressionTypes()
                        .Any(NBiometricTypes.IsImpressionTypeRolled);
                if (client.FingerScanner != newDevice)
                {
                    client.FingerScanner = newDevice;
                }
            }
            else
            {
                canCaptureSlaps = false;
                canCaptureRolled = false;
            }

        }

        public async Task<(bool successful, string message)> initStaffClientAsync()
        {
            staffClient = new NBiometricClient();

            var users = Users.getUsers ();

            foreach (var user in users)
            {
                var ns = new NSubject();
                ns.SetTemplate(new NTemplate(user.compositeTemplate));
                ns.Id = user.id.ToString();
                await BiometricTask.createTaskAsync(staffClient, ns);
            }
            return (true, "staff initialized sucessfully");
        }

        public async Task initVoterClientAsync()
        {
            client = new NBiometricClient();

            var propertyBag = NPropertyBag.Parse(Settings.@default.clientProperties);
            propertyBag.ApplyTo(client);

            var dbFilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\registration_db.db";
            client.SetDatabaseConnectionToSQLite(dbFilePath); //todo: is there a need to switch this db when in demo mode

            client.LocalOperations = NBiometricOperations.All;
            client.BiometricTypes = NBiometricType.Finger | NBiometricType.Face;
            client.FingersReturnBinarizedImage = false;
            client.FacesCheckIcaoCompliance = Config.Configuration.Settings.UseICAO;
            client.UseDeviceManager = true;
            client.FingersCalculateNfiq = NLicense.IsComponentActivated("Biometrics.FingerQualityAssessmentBase");

            client.Initialize();
            client.PropertyChanged += onClientPropertyChanged;

            if (!string.IsNullOrEmpty(Settings.@default.selectedFScannerId) &&
                client.DeviceManager.Devices.Contains(Settings.@default.selectedFScannerId))
                client.FingerScanner = (NFScanner)client.DeviceManager.Devices[Settings.@default.selectedFScannerId];
            else
            {
                //set the default to the first finger device find
                var firstScanner = client.DeviceManager.Devices.OfType<NFScanner>().FirstOrDefault();
                if (firstScanner != null)
                {
                    client.FingerScanner = firstScanner;
                    switch (client.FingerScanner.Model)
                    {
                        case "DactyScan84c":
                            client.FingerScanner.SetProperty("BeepAfterScan", true);
                            client.FingerScanner.SetProperty("ExtendedMode",true);
                            break;
                        case "KOJAK":
                            client.FingerScanner.SetProperty("BeepAfterScan", true);
                            break;
                        default:
                            break;
                    }
                    onSelectedDeviceChanged(firstScanner);
                }
            }
        }

        private void onClientPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                switch (e.PropertyName)
                {
                    case "FingerScanner":
                        onSelectedDeviceChanged(client.FingerScanner);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //log exception with Nlog here
            }
        }

        public void releaseClients()
        {
            if (client != null) client.PropertyChanged -= onClientPropertyChanged;
        }
    }
}
 