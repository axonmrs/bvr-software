﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    //? Confirm relevance
    public class InfoField
    {
        public string key { get; private set; }
        public object value { get; set; }
        public bool isEditable { get; private set; }
        public bool showAsThumbnail { get; private set; }

        public InfoField(string value)
        {
            if (value == null) throw new ArgumentNullException("value");

            value.Trim().Split(',')
                .Select(x => x.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Split('='))
                .Where(xs => xs.Length > 1)
                .ToList()
                .ForEach(xs =>
                {
                    var n = xs[0].Trim().ToLower();
                    var v = string.Join("=", xs.Skip(1)).Trim();
                    switch (n)
                    {
                        case "key":
                            key = v;
                            break;
                        case "isthumbnail":
                            showAsThumbnail = Convert.ToBoolean(v);
                            if (showAsThumbnail) value = null;
                            break;
                    }
                });
        }

        public override string ToString()
        {
            var result = $"Key = '{{{key}}}";
            if (showAsThumbnail) result += ", IsThumbnail = 'True'";
            return result;
        }
    }

}
