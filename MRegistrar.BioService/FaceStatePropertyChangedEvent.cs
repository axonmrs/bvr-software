﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.BioService
{
    public class FaceStatePropertyChangedEventArgs : EventArgs
    {
        public FaceProperty property { get; }
        public object value { get; }

        public FaceStatePropertyChangedEventArgs(FaceProperty property, object value)
        {
            this.property = property;
            this.value = value;
        }
    }

    public enum FaceProperty
    {
        FaceStatus,
        FaceDeviceOk,
        NewFace,
        BeginCapture,
        NewFaceForCapture,
        Capturing,
        CapturingFailed,
        CaptureStatus,
        CaptureComplete
    }
}
