﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MRegistrar.Security
{
    public static class AES
    {
        public static byte[] Encrypt(string encryptionKey, string salt, byte[] plainBytes)
        {
            //var plainBytes = Encoding.UTF8.GetBytes(plainText);
            var keyBytes = Encoding.UTF8.GetBytes(encryptionKey);

            if (salt == null) salt = "";
            if (salt.Length < 8) salt = salt.PadLeft(8, '0');
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            keyBytes = SHA256.Create().ComputeHash(keyBytes);

            using (var ms = new MemoryStream())
            {
                using (var aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(keyBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(plainBytes, 0, plainBytes.Length);
                        cs.Close();
                    }
                    return ms.ToArray();
                }
            }
        }

        public static byte[] Decrypt(string encryptionKey, string salt, byte[] cipherBytes)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(encryptionKey);
            keyBytes = SHA256.Create().ComputeHash(keyBytes);

            if (salt == null) salt = "";
            if (salt.Length < 8) salt = salt.PadLeft(8, '0');
            var saltBytes = Encoding.UTF8.GetBytes(salt);

            using (var ms = new MemoryStream())
            {
                using (var aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(keyBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    return ms.ToArray();
                }
            }
        }
    }
}
