﻿using MRegistrar.Data;
using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public class ReportPrinterService
    {
        private static bool? isValid = null;
        public static string printerName { get; set; } = null;
        public static Func<bool> checkPrinter { get; set; }

        public static async Task<(bool, string)> getStatus()
        {
            if (checkPrinter == null) return (false, "Not ready");
            if (checkPrinter()) return (true, "Printer Online");
            return (false, "Printer Offline");
        }
    }
}
