﻿using System;
using System.Diagnostics;

namespace MRegistrar.Services
{
    /// <summary>
    /// Contains functions to 
    /// </summary>
    public class ResourceService
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public static ResourceService singleton { get; private set; } = new ResourceService();

        public void logMemory(string notes)
        {
            if (!Config.Configuration.Settings.EnableResourceUsageLogs) return;
            var memoryUsed = getMemoryUsed();
            var privateMemoryUsed = getPrivateMemoryUsed();
            logger.Trace($"Total Memory Used: {memoryUsed} [{privateMemoryUsed}] ({notes})");
        }

        private string getMemoryUsed ()
        {
            var cnt = GC.GetTotalMemory(false);
            return (cnt / 1024.0 / 1024.0).ToString("N2");
        }

        private string getPrivateMemoryUsed ()
        {
            var cnt = Process.GetCurrentProcess().PrivateMemorySize64;
            return (cnt / 1024.0 / 1024.0).ToString("N2");
        }
    }
}
