﻿using MRegistrar.Data;
using Neurotec.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public class FingerPrintService
    {
        private static bool? isValid = null;

        public static async Task<(bool, string)> getStatus()
        {
            try
            {
                using (var deviceManager = new NDeviceManager { DeviceTypes = NDeviceType.FingerScanner })
                {
                    deviceManager.Initialize();
                    var devices = deviceManager.Devices.Cast<NDevice>().ToList();
                    if (devices.Any())
                    {
                        if (isValid != true) //state has changed
                        {
                            ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceFound.ToString(), $"{devices.Count} scanner(s) found", "System"));
                            isValid = true;
                        }
                        //todo: set the active finger scanner if needed
                        return (true, $"{devices.Count} scanner(s) found");
                    }
                    if (isValid != false) //state has changed
                    {
                        ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceNotFound.ToString(), "No Finger Scanners found", "System"));
                        isValid = false;
                    }
                    return (false, "No Finger Scanners found");
                }
            }
            catch (Exception e)
            {
                return (false, e.GetBaseException().Message);
            }
        }
    }
}
