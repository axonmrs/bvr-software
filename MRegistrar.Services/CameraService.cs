﻿using MRegistrar.Data;
using Neurotec.Devices;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public class CameraService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static bool? isValid = null;
        public static async Task<(bool, string)> getStatus()
        {
            try
            {
                //todo: short circuit
                using (var deviceManager = new NDeviceManager { DeviceTypes = NDeviceType.Camera })
                {
                    deviceManager.Initialize();
                    var devices = deviceManager.Devices.Cast<NDevice>().ToList();
                    devices.ForEach(x => logger.Trace($"Camera found: {x.DisplayName}"));
                    if (!devices.Any())
                    {
                        //log
                        if (isValid != false) //state has changed
                        {
                            var result = ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceNotFound.ToString(), "No cameras found", "System"));
                            isValid = false;
                        }
                        return (false, "No cameras found");
                    }
                    var available = devices.Count(x => x.IsAvailable);
                    if (isValid != true)
                    {
                        var result = ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceFound.ToString(), $"{devices.Count} camera(s) found. {available} available", "System"));
                        isValid = true;
                    }
                    return (true, $"{devices.Count} camera(s) found. {available} available");
                }
            }
            catch (Exception e)
            {
                return (false, e.GetBaseException().Message);
            }
        }
    }
}
