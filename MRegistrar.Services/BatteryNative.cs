﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MRegistrar.Services
{
    public class BatteryNative
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool GetSystemPowerStatus(out SystemPowerStatus sps);

        public enum ACLineStatus : byte
        {
            Offline = 0,
            Online = 1,
            Unknown = 255
        }

        public enum BatteryFlag : byte
        {
            High = 1,
            Low = 2,
            Critical = 4,
            Charging = 8,
            NoSystemBattery = 128,
            Unknown = 255
        }

        public struct SystemPowerStatus
        {
            public ACLineStatus LineStatus;
            public BatteryFlag flgBattery;
            public byte BatteryLifePercent;
            public byte Reserved1;
            public int BatteryLifeTime;
            public int BatteryFullLifeTime;
        }

        /// <summary>
        /// Returns true if a an AC Power Line is detected
        /// </summary>
        public static bool ACPowerPluggedIn()
        {
            SystemPowerStatus ret;
            GetSystemPowerStatus(out ret);

            if (ret.LineStatus == ACLineStatus.Online)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns an integer representing the percent of battery charge remaining.
        /// </summary>
        public static SystemPowerStatus getPowerStatus()
        {
            SystemPowerStatus ret;
            GetSystemPowerStatus(out ret);
            return ret;
        }
    }
}
