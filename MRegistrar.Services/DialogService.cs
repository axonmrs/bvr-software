﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public static class DialogService
    {
        private static Func<string, DialogKind, string, string, Task<bool>> _confirmFn;
        private static Func<string, DialogKind, string, Task<bool>> _alertFn;

        public static async Task<bool> confirm(string message, DialogKind kind = DialogKind.Info, string okLabel = "YES", string cancelLabel = "NO")
        {
            return await _confirmFn?.Invoke(message, kind, okLabel, cancelLabel);
        }

        public static async Task<bool> alert(string message, DialogKind kind = DialogKind.Info, string okLabel = "OK")
        {
            return await _alertFn?.Invoke(message, kind, okLabel);
        }

        public static Func<string, DialogKind, string, string, Task<bool>> confirmFn
        {
            set { _confirmFn = value; }
        }

        public static Func<string, DialogKind, string, Task<bool>> alertFn
        {
            set { _alertFn = value; }
        }

        public enum DialogKind { Info, Error, Warning }
    }
}
