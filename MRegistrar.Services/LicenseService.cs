﻿using MRegistrar.Data;
using Neurotec.Devices;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public class LicenseService
    {
        private static bool? isValid = null;

        public static async Task<(bool, string)> getStatus()
        {
            if (isValid == true) return (true, "Licenses obtained"); //short circuit
            //should we even be logging the intermitent license checks ?
            var activityType = "License Status Check";
            const string Address = "/local";
            const string Port = "5000";
            const string Components = "FaceMatcher,FaceClient,FingerClient,FingerMatcher";
            
            try
            {
                if (!NLicense.Obtain(Address, Port, Components))
                {
                    if (isValid != false)
                    {
                        ActivityLogs.save(new ActivityLogs.ActivityLog(activityType, $"Could not obtain licenses for {Components}", "System"));
                        isValid = false;
                    }
                    return (false, $"Could not obtain license/s. Contact system admin"); 
                }

                if (isValid != true)
                {
                    ActivityLogs.save(new ActivityLogs.ActivityLog(activityType, "Licenses obtained", "System"));
                    isValid = true;
                }

                return (true, "Licenses obtained");
            }
            catch (Exception e)
            {
                string message = $"Failed to obtain licenses for components.\nError message: {e.GetBaseException().Message}";
                if (e is System.IO.IOException)
                {
                    message += "\n(Probably licensing service is not running. Use Activation Wizard to figure it out.)";

                    if (isValid != false)
                    {
                        ActivityLogs.save(new ActivityLogs.ActivityLog(activityType, message, "System"));
                        isValid = false;
                    }

                    return (false, $"Could not obtain license/s. Contact system admin");
                }

                if (isValid != false)
                {
                    ActivityLogs.save(new ActivityLogs.ActivityLog(activityType, message, "System"));
                    isValid = false;
                }

                return (false, $"Could not obtain license/s. Contact system admin");
            }
        }
    }
}
