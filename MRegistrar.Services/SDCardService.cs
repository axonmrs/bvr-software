﻿using MRegistrar.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
  public class SDCardService
  {
    private static bool? isValid = null;

    public static async Task<(bool, string)> getStatus()
    {
      // return (true, "Testing 123");
      try
      {
        var drive = DriveInfo.GetDrives()
            .Where(x => x.IsReady && x.DriveType == DriveType.Removable)
            .FirstOrDefault(x => x.VolumeLabel == "can we achieve this");
        //? How do we tell that it is an sd-card
        //if (drive == null) return (false, "Not found");
        //todo: do other checks
        //for now, assume the world is a good place

        if (isValid != true)
        {
          ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceFound.ToString(), "SD Card found", "System"));
          isValid = true;
        }

        return (true, "Need to do further checks");
      }
      catch (Exception e)
      {
        if (isValid != false)
        {
          ActivityLogs.save(new ActivityLogs.ActivityLog(Models.DeviceStatus.DeviceNotFound.ToString(), "No SD card found", "System"));
          isValid = false;
        }
        //todo: log
        return (false, e.GetBaseException().Message);
      }
    }
  }
}
