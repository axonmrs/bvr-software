﻿using Laxton.Device.BatteryHID;
using Laxton.Device.BatteryHID.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace MRegistrar.Services
{
    public class BatteryService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static BatteryDeviceNew device = new BatteryDeviceNew("5750", "0483");
        /// <summary>
        /// Holds the last battery information
        /// </summary>
        public static BatteryInfo batteryInfo { get; private set; }
        /// <summary>
        /// Gets the percentage of battery remaining.
        /// There can be multiple batteries. This returns only the last battery.
        /// </summary>
        /// <returns></returns>
        public static async Task<(bool, string)> getStatus()
        {
            try
            {
                logger.Trace("Starting...");
                var errorMsg = "";
                var done = false;
                device.SendCommandByGetBatteryInfo(r => { 
                    if (r.IsSuccess)
                    {
                        batteryInfo = r.Data;
                        logger.Trace($"It worked: {batteryInfo.deviceName}, {batteryInfo.batteryLevel}, {batteryInfo.kitTemprature}, {batteryInfo}");
                    }
                    else
                    {

                        errorMsg = r.Mes;
                    }
                    done = true;
                }, 6000);
                if (!done)
                {
                    await Task.Delay(7000); //wait for the timeout
                }
                if (!string.IsNullOrWhiteSpace(errorMsg)) //it worked
                {
                    return (false, errorMsg);
                }
                else
                {
                    //todo: find out what the values mean and populate accordingly
                    return (true, $"Level: {batteryInfo.batteryLevel}");
                }
                //var status = BatteryNative.getPowerStatus();
                //var charging = (status.flgBattery & BatteryNative.BatteryFlag.Charging) == BatteryNative.BatteryFlag.Charging;
                //var percentage = status.BatteryLifePercent > 100 ? 100 : status.BatteryLifePercent;
                //var msg = charging
                //    ? $"Charging ({percentage}%)"
                //    : $"{percentage}% remaining";
                //return (charging ? true : percentage > 25, msg);
            } 
            catch(Exception e)
            {
                //todo: log this
                return (false, e.GetBaseException().Message);
            }
        }

        //private static string formatTime(int minutes)
        //{
        //    var t = TimeSpan.FromMinutes(minutes);
        //    if (t.Minutes == 0) return $"{t.Hours} hr";
        //    if (t.Hours == 0) return $"{t.Minutes} min";
        //    return $"{t.Hours} hr {t.Minutes} min";
        //}
    }
}
