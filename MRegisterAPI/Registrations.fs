﻿module Registrations


open System
open System.IO
open System.IO.Compression
open Rezoom.SQL
open Model.Utils
open FSharp.Data


type OutputXml = XmlProvider<"./Templates/sampleRegisteredVoter.xml">
type private GetCount = SQL<"Select count(*) as cnt from registered_voters">
///Gets the voter info (-finger and face templates)
type FindVoter = SQL<"""
  Select 
    r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
    r.date_of_birth as dateOfBirth, r.estimated_age as estimatedAge, r.sex,
    r.phone_number as phoneNumber,
    r.residential_address as residentialAddress, r.residential_town as residentialTown, r.residential_district_id as residentialDistrictId, d1.name as residentialDistrict,
    r.id_type_id as idTypeId, id.name as idType, r.id_number as idNumber, r.id_expiry as idExpiry,
    r.father_name as fatherName, r.mother_name as motherName,
    r.home_town_address as homeTownAddress, r.home_town as homeTown, r.home_town_district_id as homeTownDistrictId, d2.name as homeTownDistrict,
    r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
    r.photo as photoBytes, '' as photo, r.date_of_birth as dateRegistered
  From registered_voters r
  Inner Join districts d1 on d1.id = r.residential_district_id
  Inner Join districts d2 on d2.id = r.home_town_district_id
  Inner Join id_types id on id.id = r.id_type_id
  Where r.voter_id = @voterId;""">
type GetVoters = SQL<"""
  Select 
    r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
    r.date_of_birth as dateOfBirth, r.estimated_age as estimatedAge, r.sex,
    r.phone_number as phoneNumber,
    r.residential_address as residentialAddress, r.residential_town as residentialTown, r.residential_district_id as residentialDistrictId, d1.name as residentialDistrict,
    r.id_type_id as idTypeId, id.name as idType, r.id_number as idNumber, r.id_expiry as idExpiry,
    r.father_name as fatherName, r.mother_name as motherName,
    r.home_town_address as homeTownAddress, r.home_town as homeTown, r.home_town_district_id as homeTownDistrictId, d2.name as homeTownDistrict,
    r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
    r.photo as photoBytes, '' as photo, r.date_of_birth as dateRegistered
  From registered_voters r
  Inner Join districts d1 on d1.id = r.residential_district_id
  Inner Join id_types id on id.id = r.id_type_id
  Left Join districts d2 on d2.id = r.home_town_district_id
  Order by r.id
  Limit @batchSize Offset @offset;""">
type private GetLastVoterId = SQL<"""Select max(voter_id) as voterId from registered_voters""">
type private InsertVoter = SQL<"""
  Insert into registered_voters 
    (voter_id, ps_name, ps_code, surname, other_names, date_of_birth, estimated_age, sex, phone_number, registered_by, residential_address, residential_town, residential_district_id, id_type_id, id_number, id_expiry, father_name, mother_name, home_town_address, home_town, home_town_district_id, is_visually_impaired, is_disabled, photo, face_template, left_thumb, left_index, left_middle, left_ring, left_pinky, right_thumb, right_index, right_middle, right_ring, right_pinky)
  values 
    (@voterId, @psName, @psCode, @surname, @otherName, @dateOfBirth, @estimatedAge, @sex, @phoneNumber, @registeredBy, @residentialAddress, @residentialTown, @residentialDistrictId, @idTypeId, @idNumber, @idExpiry, @fatherName, @motherName, @homeTownAddress, @homeTown, @homeTownDistrictId, @isVisuallyImpaired, @isDisabled, @photo, @faceTemplate, @leftThumb, @leftIndex, @leftMiddle, @leftRing, @leftPinky, @rightThumb, @rightIndex, @rightMiddle, @rightRing, @rightPinky);""">
type GetForExport = SQL<"""
  Select *
  From registered_voters
  Where @voterId is null or voter_id = @voterId"""> //todo: add a filter and a batch size. Also update when migration is done

type Registration =
  { surname: string
    otherNames: string
    dateOfBirth: DateTime
    estimatedAge: int
    sex: string
    phoneNumber: string
    residentialAddress: string
    residentialTown: string
    residentialDistrictId: int64
    idTypeId: int64
    idNumber: string
    idExpiry: DateTime
    fatherName: string
    motherName: string
    homeTownAddress: string
    homeTown: string
    homeTownDistrictId: int64
    isVisuallyImpaired: bool
    isDisabled: bool
    photo: string
    faceTemplate: string
    leftThumb: string
    leftIndex: string
    leftMiddle: string
    leftRing: string
    leftPinky: string
    rightThumb: string
    rightIndex: string
    rightMiddle: string
    rightRing: string
    rightPinky: string }
type SaveRegistrationResult =
  { idNumber: string }


let toDataUri mime bytes =
  sprintf "data:%s;base64,%s" mime (Convert.ToBase64String bytes)
let readAll (offset, pageSize) =
  GetVoters.Command(int64 pageSize, int64 offset)
  |> execCmd
  |> Result.map (List.ofSeq >> List.map (fun x ->
    //update the photo from byte[] to base64 string
    let photo = toDataUri "image/jpg" x.photoBytes
    let x = FindVoter.Row (x.id, x.voterId, x.surname, x.otherNames, x.dateOfBirth, x.estimatedAge, x.sex, x.phoneNumber, x.residentialAddress, x.residentialTown, x.residentialDistrictId, x.residentialDistrict, x.idTypeId, x.idType, x.idNumber, x.idExpiry, x.fatherName, x.motherName, x.homeTownAddress, x.homeTown, x.homeTownDistrictId, Option.defaultValue "" x.homeTownDistrict, x.isVisuallyImpaired, x.isDisabled, Array.empty, photo, x.dateRegistered)
    x))


let find voterId =
  FindVoter.Command(voterId)
  |> execCmd
  |> Result.map Seq.tryHead
  |> Result.bind (function
    | None -> Error (exn "Not found")
    | Some x -> 
      //update the photo from byte[] to base64 string
      let photo = toDataUri "image/jpg" x.photoBytes
      let x = FindVoter.Row (x.id, x.voterId, x.surname, x.otherNames, x.dateOfBirth, x.estimatedAge, x.sex, x.phoneNumber, x.residentialAddress, x.residentialTown, x.residentialDistrictId, x.residentialDistrict, x.idTypeId, x.idType, x.idNumber, x.idExpiry, x.fatherName, x.motherName, x.homeTownAddress, x.homeTown, x.homeTownDistrictId, x.homeTownDistrict, x.isVisuallyImpaired, x.isDisabled, Array.empty, photo, x.dateRegistered)
      Ok x)

let private b2s (xs: byte[]) = Convert.ToBase64String xs
let private dt2s (x: DateTime) = x.ToString "yyyy-MM-dd HH:mm:ss"
let private d2s (x: DateTime) = x.ToString "yyyy-MM-dd"
let getXml voterId =
  GetForExport.Command(Some voterId)
  |> execCmd
  |> Result.map Seq.tryHead
  |> Result.bind (function
    | None -> Error (exn "Not found")
    | Some x -> 
      let audit = OutputXml.Audit (x.id, dt2s DateTime.Now, x.registered_by)
      let bio = OutputXml.Bio (b2s x.photo, b2s x.face_template, b2s x.left_thumb, b2s x.left_index, b2s x.left_middle, b2s x.left_ring, b2s x.left_pinky, b2s x.right_thumb, b2s x.right_index, b2s x.right_middle, b2s x.right_ring, b2s x.right_pinky)
      let details = OutputXml.Details (x.surname, x.other_names, d2s x.date_of_birth, string x.estimated_age, x.sex, x.phone_number, x.residential_address, x.residential_town, x.residential_district_id, string x.id_type_id, x.id_number, d2s x.id_expiry, x.father_name, x.mother_name, x.home_town_address, x.home_town, string x.home_town_district_id, x.is_visually_impaired, x.is_disabled)
      let source = OutputXml.Source (x.ps_name, x.ps_code)
      let xml = OutputXml.RegisteredVoter (source, audit, details, bio)
      Ok (string xml))
///Generates a zip file with a list of registrations
let getXmls () =
  GetForExport.Command None
  |> execCmd
  |> Result.map List.ofSeq
  |> Result.bind (function
    | [] -> Error (exn "No registrations found pending export")
    | xs ->
      //prepare the stream
      use stream = new MemoryStream()
      do
        use archive = new ZipArchive (stream, ZipArchiveMode.Create, true)
        xs
        |> List.iter (fun x ->
          let audit = OutputXml.Audit (x.id, dt2s DateTime.Now, x.registered_by)
          let bio = OutputXml.Bio (b2s x.photo, b2s x.face_template, b2s x.left_thumb, b2s x.left_index, b2s x.left_middle, b2s x.left_ring, b2s x.left_pinky, b2s x.right_thumb, b2s x.right_index, b2s x.right_middle, b2s x.right_ring, b2s x.right_pinky)
          let details = OutputXml.Details (x.surname, x.other_names, d2s x.date_of_birth, string x.estimated_age, x.sex, x.phone_number, x.residential_address, x.residential_town, x.residential_district_id, string x.id_type_id, x.id_number, d2s x.id_expiry, x.father_name, x.mother_name, x.home_town_address, x.home_town, string x.home_town_district_id, x.is_visually_impaired, x.is_disabled)
          let source = OutputXml.Source (x.ps_name, x.ps_code)
          let xml = OutputXml.RegisteredVoter (source, audit, details, bio)
          let zipEntry = archive.CreateEntry (sprintf "%s.xml" x.voter_id)
          use writer = new StreamWriter (zipEntry.Open())
          writer.Write (string xml))
      Ok (stream.ToArray()))


let mutable private lastVoterId = 0L //get at the start of the app
let private s2b (x: string) =
  match x with
  | null | "" -> Array.empty
  | x -> Convert.FromBase64String x
let save username (x: Registration) =
  lastVoterId <- lastVoterId + 1L
  let voterId = string lastVoterId
  let cmd = InsertVoter.Command (x.dateOfBirth, x.estimatedAge, s2b x.faceTemplate, x.fatherName, x.homeTown, x.homeTownAddress, x.homeTownDistrictId, x.idExpiry, x.idNumber, x.idTypeId, x.isDisabled, x.isVisuallyImpaired, s2b x.leftIndex, s2b x.leftMiddle, s2b x.leftPinky, s2b x.leftRing, s2b x.leftThumb, x.motherName, x.otherNames, x.phoneNumber, s2b x.photo, App.config.name, App.config.code, username, x.residentialAddress, x.residentialDistrictId, x.residentialTown, s2b x.rightIndex, s2b x.rightMiddle, s2b x.rightPinky, s2b x.rightRing, s2b x.rightThumb, x.sex, x.surname, voterId)
  cmd
  |> execCmd
  |> function
    | Ok _ -> Ok { idNumber = voterId }
    | Error e ->
      printfn "Error saving registration: %A" e
      lastVoterId <- lastVoterId - 1L
      Error e

///The number of registrations done so far
let registrationCount () =
  GetCount.Command ()
  |> execCmd
  |> Result.map (Seq.tryHead >> function
    | None -> 0L
    | Some x -> x.cnt)
  

let init () =
  GetLastVoterId.Command ()
  |> execCmd
  |> Result.map Seq.tryHead
  |> function
    | Error _
    | Ok None -> App.config.baseVoterIdNumber
    | Ok (Some x) -> 
      match Int32.TryParse x.voterId with
      | false, _ -> App.config.baseVoterIdNumber
      | _, x -> x
  |> fun x -> lastVoterId <- int64 x