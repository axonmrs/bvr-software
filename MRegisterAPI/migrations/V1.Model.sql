﻿Create table users
  ( id int64 primary key
  , name string (256)
  , username string (64)
  , [type] string (64)
  , password_hash string (128) );


create table app_settings
  ( id int64 primary key
  , name string (128)
  , value string (64) );


create table districts
  ( id int64 primary key
  , code string (256)
  , name string (256) );


create table id_types
  ( id int64 primary key
  , code string (64)
  , name string (128) );


create table registered_voters
  ( id int64 primary key autoincrement
  , voter_id string (64)
  , ps_name string (128)
  , ps_code string (32)
  , surname string (256)
  , other_names string (256)
  , date_of_birth DateTime
  , estimated_age int
  , sex string (8)
  , phone_number string
  , registered_by string
  , residential_address string
  , residential_town string
  , residential_district_id int64
  , id_type_id int64
  , id_number string
  , id_expiry DateTime
  , father_name string
  , mother_name string
  , home_town_address string
  , home_town string
  , home_town_district_id int64
  , is_visually_impaired bool
  , is_disabled bool
  , photo binary
  , face_template binary
  , left_thumb binary
  , left_index binary
  , left_middle binary
  , left_ring binary
  , left_pinky binary
  , right_thumb binary
  , right_index binary
  , right_middle binary
  , right_ring binary
  , right_pinky binary );