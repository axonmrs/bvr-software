﻿module Districts


open System
open Rezoom.SQL
open Model.Utils


type GetDistricts = SQL<"""
  Select id, name
  From districts
  Where name like '%' || @regionName""">


type District =
  { id: int64
    name: string
    regionId: string }


let readByRegion (regionName: string) =
  let regionName = regionName.ToUpper()
  let region = sprintf "(%s)" regionName
  let nameWithoutRegion (x: string) = x.Split('(').[0].Trim()
  GetDistricts.Command region
  |> execCmd
  |> Result.map (List.ofSeq >> List.map (fun x -> { id = x.id; name = nameWithoutRegion x.name; regionId = regionName}))