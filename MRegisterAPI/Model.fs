﻿module Model

open Rezoom.SQL


module Utils =
  open Rezoom.SQL.Synchronous

  let execCmd (cmd: 't Command) = 
    use context = new ConnectionContext()
    try Ok (cmd.Execute context)
    with | e -> Error e


type Db = SQLModel<"./">

