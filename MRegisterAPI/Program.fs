module MRegisterAPI.App

open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open FSharp.Control.Tasks.ContextInsensitive
open Microsoft.AspNetCore.Http


let x404 next (ctx: HttpContext) =
  task {
    let! body = ctx.ReadBodyFromRequestAsync ()
    let msg = sprintf "%s> not found\n%s\n%s" ctx.Request.Method (ctx.GetRequestUrl()) body
    return! text msg next ctx }


let webApp =
  choose
    [ GET >=> route "/" >=> text "Welcome"
      subRoute "/api" ApiHandler.handler
      x404 ]


// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder.WithOrigins("http://localhost:8080")
           .AllowAnyMethod()
           .AllowAnyHeader()
           |> ignore

let configureApp (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IHostingEnvironment>()
    (match env.IsDevelopment() with
    | true  -> app.UseDeveloperExceptionPage()
    | false -> app.UseGiraffeErrorHandler errorHandler)
        .UseHttpsRedirection()
        .UseCors(configureCors)
        .UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddFilter(fun l -> l.Equals LogLevel.Error)
           .AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main _ =
  printfn "MRegistrar API v0.0.2"
  let contentRoot = Directory.GetCurrentDirectory()
  let webRoot     = Path.Combine(contentRoot, "WebRoot")
  try 
    App.init ()
    Registrations.init ()
  with | e -> printfn "Error initializing registrations: %A" e
  WebHostBuilder()
      .UseKestrel()
      .UseContentRoot(contentRoot)
      .UseIISIntegration()
      .UseWebRoot(webRoot)
      .Configure(Action<IApplicationBuilder> configureApp)
      .ConfigureServices(configureServices)
      .ConfigureLogging(configureLogging)
      .Build()
      .Run()
  0