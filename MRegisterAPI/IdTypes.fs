﻿module IdTypes


open System
open Rezoom.SQL
open Model.Utils


type private GetIdTypes = SQL<"""
  Select id, name
  From id_types""">

type IdType =
  { id: int64
    name: string }


let readAll () =
  GetIdTypes.Command ()
  |> execCmd
  |> Result.map (List.ofSeq >> List.sortBy (fun x -> x.name) >> List.map (fun x -> { id = x.id; name = x.name}))