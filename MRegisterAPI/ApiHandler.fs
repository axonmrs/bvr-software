﻿module ApiHandler


open Giraffe
open Microsoft.AspNetCore.Http



let private result = function
  | Ok x -> json x
  | Error (e: exn) -> ServerErrors.INTERNAL_ERROR (e.GetBaseException().Message)
let private rawResult mime = function
  | Ok (x: string) -> setHttpHeader "content-type" mime >=> text x
  | Error (e: exn) -> ServerErrors.INTERNAL_ERROR (e.GetBaseException().Message)
let private bytesResult mime = function
  | Ok xs -> setHttpHeader "content-type" mime >=> setBody xs
  | Error (e: exn) -> ServerErrors.INTERNAL_ERROR (e.GetBaseException().Message)


let private notAuthorized = setStatusCode 401 >=> text "Not authorized"
let private checkAuthorizationHeader (ctx: HttpContext) =
  match ctx.TryGetRequestHeader "Authorization" with
  | None -> false
  | Some header ->
    match header.Split ' ' |> List.ofArray with
    | ["Bearer"; x] ->
      //todo: decrypt to get the username and expiry
      true
    | _ -> false
let private ensureAuthorized = authorizeRequest checkAuthorizationHeader notAuthorized
//todo: add another f in which the username is made available to the next task
let handler next =
  choose
    [ POST >=> route "/login" >=> bindModel None (App.login >> result)
      POST >=> routef "/activation/%s" (App.activate >> result)
      GET >=> routef "/getRegistrationXml/%s" (Registrations.getXml >> rawResult "application/xml")
      GET >=> route "/getRegistrationXmls" >=> warbler (fun _ -> Registrations.getXmls () |> bytesResult "application/zip")
      GET >=> ensureAuthorized >=> route "/regions" >=> result (Regions.readAll())
      GET >=> ensureAuthorized >=> routef "/districts/%s" (Districts.readByRegion >> result)
      GET >=> ensureAuthorized >=> route "/idTypes" >=> result (IdTypes.readAll())
      GET >=> ensureAuthorized >=> route "/getRegistrationCount" >=> result (Registrations.registrationCount())
      GET >=> ensureAuthorized >=> routef "/getRegistrations/%i/%i" (Registrations.readAll >> result)
      GET >=> ensureAuthorized >=> routef "/findRegistration/%s" (Registrations.find >> result)
      POST >=> ensureAuthorized >=> route "/saveRegistration" >=> bindModel None (Registrations.save "username" >> result) //todo: find a way of getting the user
      ] next