﻿module App


open Rezoom.SQL
open Model.Utils
open System.Text
open System.Security.Cryptography


type private GetUser = SQL<"""
  Select id, name, [type]
  From users
  Where username = @username and password_hash = @password collate nocase""">


type private GetSetting = SQL<"""
  Select name, value
  From app_settings
  Where name = @name""">
type private GetSettings = SQL<"""
  Select name, value
  From app_settings""">


let isActivated () =
  GetSetting.Command("activated")
  |> execCmd
  |> Result.map (List.ofSeq)
  |> Result.bind (function
    | [] -> Error (exn "activated setting not found")
    | x :: _ -> Ok (x.value = "true"))

let activate code =
  GetSetting.Command "activation_code"
  |> execCmd
  |> Result.map (List.ofSeq)
  |> Result.bind (function
    | [] -> Error (exn "activation code setting not found")
    | x :: _ -> 
      match x.value = code with
      | false -> Error (exn "Invalid activation code")
      | true -> Ok {| success = true |})

let private hash256 (text: string) =
  let bytes = Encoding.UTF8.GetBytes text
  use hasher = new SHA256Managed ()
  let bytes = hasher.ComputeHash bytes
  (StringBuilder(), bytes)
  ||> Array.fold (fun sb -> sprintf "%02x" >> sb.Append)
  |> string
let login (data: {|username: string; password: string|}) =
  let passwordHash = hash256 (data.username.ToLower() + data.password)
  
  GetUser.Command (passwordHash, data.username.ToLower())
  |> execCmd 
  |> Result.map (List.ofSeq)
  |> Result.bind (function
    | [] -> Error (exn "Invalid username/password combination")
    | x :: _ -> 
      //todo: create the token for all future calls 
      let token = data.username.ToLower()
      Ok {| name = x.name; ``type`` = x.``type``; token = token |})

let mutable config = {| name=""; code=""; activated = false; activationCode = ""; userCount = 0; baseVoterIdNumber = 10100000 |} //todo: find a way of making this readonly

let init () =
  GetSettings.Command ()
  |> execCmd
  |> Result.map List.ofSeq
  |> Result.map (fun xs ->
    (config, xs)
    ||> List.fold (fun s x ->
      match x.name with
      | "ps_code" -> {| s with code = x.value |}
      | "ps_name" -> {| s with name = x.value |}
      | "activation_code" -> {| s with activationCode = x.value |}
      | "activated" -> {| s with activated = System.Boolean.TryParse x.value |> snd |}
      | "number_of_ps_users" -> {| s with userCount = System.Int32.TryParse x.value |> snd |}
      | _ -> s))
  |> function 
    | Error _ -> ()
    | Ok x -> config <- x