﻿module Regions


open System
open System.Text.RegularExpressions
open Rezoom.SQL
open Model.Utils


type private GetDistricts = SQL<"""
  Select name
  From Districts""">

type Region =
  { id: string
    name: string }

let readAll () =
  GetDistricts.Command ()
  |> execCmd
  |> Result.map (fun xs ->
    xs
    |> List.ofSeq
    |> List.map (fun x -> Regex.Match(x.name, "\((.*?)\)").Value.Trim('(', ')'))
    |> List.distinct
    |> List.sort
    |> List.filter (fun x -> not <| String.IsNullOrWhiteSpace x)
    |> List.map (fun x -> { id = x; name = x}))