﻿namespace MRegistrar.Data


module BackupLogs = 
  open System
  open Rezoom.SQL
  open Model.Utils

  type private InsertBackupLog = SQL<"""
    Insert into backups(backup_date, backed_up_by, number_of_records)
    Values(@backupDate, @backedUpBy, @numberOfRecords);""">

  type GetBackupLogs = SQL<"""Select * From backups Limit @batchSize Offset @offset;""">

  type BackupLog = 
    { backupDate: DateTime
      backedUpBy: string
      numberOfRecords: int }

  let save (data: BackupLog) = 
    InsertBackupLog.Command(data.backedUpBy, data.backupDate, data.numberOfRecords)
    |> execCmd