﻿namespace MRegistrar.Data


module Users = 
  open Rezoom.SQL
  open Model.Utils

  type FindByTypeAndSelected = SQL<"""
    Select u.id, u.name, u.username, u.[type], u.is_active as isActive, u.selected
    From users u
    Where [type] = @userType and selected = @selected""">

  type ActivateUser = SQL<"""
    Update users
    Set is_active = @isActive
    Where id = @id;""">

  type SelectUser = SQL<"""
    Update users
    Set selected = @selected
    Where id = @id;""">

  type FindById = SQL<"""
    Select *
    From users 
    Where is_active=true and id = @id; """>

  type GetUsers = SQL<"""
  select id,compositeTemplate from users; """>

  type InsertUser = SQL<""" 
  INSERT INTO Users(name,username,type,password_hash,is_active,selected,compositeTemplate) 
  VALUES(@name,@username,@type,@passwordHash,@isActive,@selected,@compositeTemplate);SELECT last_insert_rowid() as id;
  """>

  type DeleteUser = SQL<""" 
    delete from users where id = @id;
  """>

  type FindByUsername = SQL<"""
    Select * 
    From users
    Where username = @username """>

  type User = 
    { name: string
      username: string
      ``type``: string
      passwordHash : string
      isActive : bool
      selected : bool
      compositeTemplate : byte[] }

  let findByTypeAndSelected userType selected = 
    FindByTypeAndSelected.Command(selected, userType)
    |> execCmd
    |> Result.map Array.ofSeq
    |> function
      | Ok xs -> xs
      | _ -> Array.empty

  let toggleActivateUser id isActive = 
    ActivateUser.Command(id, isActive)
    |> execCmd

  let toggleSelectUser id selected = 
    SelectUser.Command(id, selected)
    |> execCmd

  let findById id = 
    FindById.Command(id)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind (function
      | None -> Error (exn "User Not Found")
      | Some x -> Ok x  )
   
  let getUsers () = 
    GetUsers.Command()
    |> execCmd
    |> Result.map Array.ofSeq
    |> function Ok xs -> xs | _ -> Array.empty

  let insertUser (data: User) = 
    InsertUser.Command(data.compositeTemplate, data.isActive, data.name, data.passwordHash, data.selected, data.``type``, data.username)
    |> execCmd
    |> function
      | Ok x -> Ok(x)
      | Error e -> Error e

        
  let deleteUser id = 
    DeleteUser.Command(id)
    |> execCmd

  let findByUsername username = 
    FindByUsername.Command(username)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind (function 
      | None -> Error (exn "User Not Found")
      | Some x -> Ok x )
