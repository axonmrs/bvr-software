﻿namespace MRegistrar.Data


module Model =
  open Rezoom.SQL


  module Utils =
    open Rezoom.SQL.Synchronous
    let mutable internal connectionStringName = "db"

    let execCmd (cmd: 't Command) = 
      use context = new ConnectionContext()
      use conn = context.GetConnection connectionStringName
      try Ok (cmd.Execute conn)
      with | e -> Error e

    let setDemoMode x =
      connectionStringName <- if x then "demoDb" else "db"


  type Db = SQLModel<"./">

  type User =
    { name: string
      token: string
      ``type``: string
      username: string
      is_active: bool
      selected: bool }

