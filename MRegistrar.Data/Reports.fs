﻿namespace MRegistrar.Data.Reporting


open System
open Rezoom.SQL
open MRegistrar.Data.Model.Utils


type AllRegistrations =
  { voterId: string
    surname: string
    otherNames: string
    dateOfBirth: DateTime
    estimatedAge: int
    sex: string
    father: string
    mother: string
    photo: byte [] }
type StartOfDayStatistics =
  { pollingStationCode: string
    pollingStationName: string
    totalRegistrations: int
    lastRegistrationDate: DateTime
    registrationStartDate: DateTime
    registrationEndDate: DateTime
    presidingOfficer: string }
type EndOfDayStatistics =
  { pollingStationCode: string
    pollingStationName: string
    totalRegistrationsToday: int
    lastRegistrationDate: DateTime
    presidingOfficer: string }
type QueueData =
  { index: int
    row: int
    column: int
    title: string }
type GetAllRegistrations = SQL<"""
  Select voter_id, surname, other_names, date_of_birth, estimated_age, sex,
    father_name, mother_name, photo
  From registered_voters""">
type GetStartOfDayStatistics = SQL<"""
    with r as (
        Select count(*) as total_registrations, max(created_at) as last_registration_date
        From registered_voters
     ),
     p as (
        Select name from users where type = 'PresidingOfficer' limit 1
     )
     Select total_registrations, last_registration_date,
        ps_code.value as ps_code, ps_name.value as ps_name,
        start_date.value as start_date, end_date.value as end_date,
        p.name as officer
     From r
     Inner Join app_settings as ps_code on ps_code.name = 'ps_code'
     Inner Join app_settings as ps_name on ps_name.name = 'ps_name'
     Inner Join app_settings as start_date on start_date.name = 'start_date'
     Inner Join app_settings as end_date on end_date.name = 'end_date'
     Inner Join p on 1 = 1
     """>
type GetEndOfDayStatistics = SQL<"""
with r as (
    Select count(*) as total_registrations, max(created_at) as last_registration_date
    From registered_voters
    Where created_at >= @today
 ),
 p as (
    Select name from users where type = 'PresidingOfficer' limit 1
 )
 Select total_registrations, last_registration_date,
    ps_code.value as ps_code, ps_name.value as ps_name,
    p.name as officer
 From r
 Inner Join app_settings as ps_code on ps_code.name = 'ps_code'
 Inner Join app_settings as ps_name on ps_name.name = 'ps_name'
 Inner Join p on 1 = 1
""">


module ReportData =
  let getAllRegistrations () =
    GetAllRegistrations.Command ()
    |> execCmd
    |> Result.map (fun xs ->
      xs
      |> Array.ofSeq
      |> Array.map (fun x -> 
        { voterId = x.voter_id; surname = x.surname; otherNames = x.other_names
          dateOfBirth = x.date_of_birth; estimatedAge = x.estimated_age; sex = x.sex
          father = x.father_name; mother = x.mother_name; photo = x.photo}))
    |> function Ok xs -> xs | Error _ -> Array.empty

  let getStartOfDayStatistics () =
    GetStartOfDayStatistics.Command ()
    |> execCmd
    |> Result.map (fun xs ->
      xs
      |> Array.ofSeq
      |> Array.map (fun x -> 
        { pollingStationCode = x.ps_code; pollingStationName = x.ps_name; totalRegistrations = x.total_registrations |> int;
          lastRegistrationDate = x.last_registration_date; registrationStartDate = System.DateTime.Parse x.start_date; registrationEndDate = System.DateTime.Parse x.end_date; presidingOfficer = x.officer}))
    |> function Ok xs -> xs | Error _ -> Array.empty
  let today = DateTime.Now.Date
  let getEndOfDayStatistics () =
    GetEndOfDayStatistics.Command (today)
    |> execCmd
    |> Result.map (fun xs ->
      xs
      |> Array.ofSeq
      |> Array.map (fun x -> 
        { pollingStationCode = x.ps_code; pollingStationName = x.ps_name; totalRegistrationsToday = x.total_registrations |> int;
          lastRegistrationDate = x.last_registration_date; presidingOfficer = x.officer}))
    |> function Ok xs -> xs | Error _ -> Array.empty