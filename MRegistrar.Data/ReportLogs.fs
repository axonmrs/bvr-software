﻿namespace MRegistrar.Data


module ReportLogs =
  open System
  open Rezoom.SQL
  open Model.Utils

  type private InsertReportLog = SQL<"""
    Insert into report_logs(type, date, generated_by)
    Values(@type, @date, @generatedBy);""">

  type GetReportLogs = SQL<"""Select * From report_logs Limit @batchSize Offset @offset;""">

  type ReportLog = 
    { ``type``: string
      generatedBy: string }

  let date = DateTime.Now
  let save (data: ReportLog) = 
    InsertReportLog.Command(date, data.generatedBy, data.``type``)
    |> execCmd
