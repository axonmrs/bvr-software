﻿create table backups
  ( id int64 primary key autoincrement 
  , backup_date DateTime
  , backed_up_by string
  , number_of_records int );

create table activity_logs 
	(	id int64 primary key autoincrement 
	, date	DateTime
	, activity_type	string
	, description	string
	, created_by	string );

create table registration_logs
	(	id int64 primary key autoincrement 
	, voter_id	string
	, date	DateTime
	, reason	string
	, ps_code	string );

create table report_logs
	(	id int64 primary key autoincrement 
	, type	string
	, date	DateTime
	, generated_by	string );

create table reasons 
	(	id int64 primary key
	  , code string (64)
	  , name string (128) );

alter table registered_voters add column guarantor1_voter_id string null;
alter table registered_voters add column guarantor2_voter_id string null;
alter table registered_voters add column guarantor1_name string null;
alter table registered_voters add column guarantor2_name string null;
alter table registered_voters add column is_estimated_age bool null;
alter table registered_voters add column card_print_count int;
alter table registered_voters add column transmitted bool;
alter table registered_voters add column transmitted_on DateTime null;

alter table users add column is_active bool;
alter table users add column selected bool;

create table devices 
	(	id int64 primary key autoincrement
	  , serial_number string (64)
	  , identification_code string (128)
	  , sequence string (128)
	  , reference string (128)
	  , activation_code string (128)
	  , activation_code_hash string (255));

create table polling_stations 
	(	id int64 primary key
	  , code string (64)
	  , name string
		, electoral_area string
		, constituency string
		, district string
		, region string );
