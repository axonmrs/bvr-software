﻿Create table users
  ( id int64 primary key autoincrement
  , name string (256)
  , username string (64)
  , [type] string (64)
  , password_hash string (128)
  , compositeTemplate binary);


create table app_settings
  ( id int64 primary key
  , name string (128)
  , value string (64) );


create table districts
  ( id int64 primary key
  , code string (256)
  , name string (256)
  , region_id int64);

create table id_types
  ( id int64 primary key
  , code string (64)
  , name string (128) );

create table regions
  ( id int64 primary key
  , code string (64)
  , name string (128) );


create table registered_voters
  ( id int64 primary key autoincrement
  , voter_id string
  , ps_name string
  , ps_code string
  , surname string
  , other_names string
  , date_of_birth DateTime
  , estimated_age int
  , sex string
  , phone_number string
  , registered_by string
  , residential_address string
  , residential_town string
  , residential_district_id int64
  , id_type_id int64
  , id_number string
  , id_expiry DateTime
  , father_name string
  , mother_name string
  , home_town_address string
  , home_town string
  , home_town_district_id int64
  , is_visually_impaired bool
  , is_disabled bool
  , photo binary
  , face_template binary
  , left_thumb binary
  , left_index binary
  , left_middle binary
  , left_ring binary
  , left_pinky binary
  , right_thumb binary
  , right_index binary
  , right_middle binary
  , right_ring binary
  , right_pinky binary
  , left_thumb_tpl binary
  , left_index_tpl binary
  , left_middle_tpl binary
  , left_ring_tpl binary
  , left_pinky_tpl binary
  , right_thumb_tpl binary
  , right_index_tpl binary
  , right_middle_tpl binary
  , right_ring_tpl binary
  , right_pinky_tpl binary
  , created_at DateTime
  , backed_up bool
  , backed_up_on DateTime
  , hearingImpaired bool
  , leper bool
  , missingFingersL bool
  , missingFingersR bool
  , amputatedHandsL bool
  , amputatedHandsR bool
  , compositeTemplate binary
  , challengeReasons string
  , photo_j2k binary);


create table old_voters_register
  ( id int64 primary key autoincrement
  , voter_id string (64)
  , surname string (256)
  , other_names string (256)
  , date_of_birth DateTime
  , estimated_age int
  , sex string (8)
  , phone_number string
  , registered_by string
  , residential_address string
  , residential_town string
  , residential_district_id int64
  , id_type_id int64
  , id_number string
  , id_expiry DateTime
  , father_name string
  , mother_name string
  , home_town_address string
  , home_town string
  , home_town_district_id int64
  , is_visually_impaired bool
  , is_disabled bool
  , photo binary
  , registered string
  , registered_on DateTime);