﻿namespace MRegistrar.Data


module Reasons = 
  open Rezoom.SQL
  open Model.Utils
  
  
  type private GetReasons = SQL<"""
    Select id, name, code
    From reasons""">
  
  type IdType =
    { id: int64
      name: string 
      code: string }
  
  
  let readAll () =
    GetReasons.Command ()
    |> execCmd
    |> Result.map (Array.ofSeq >> Array.sortBy (fun x -> x.name) >> Array.map (fun x -> { id = x.id;  name = x.name; code = x.code }))
    |> function Ok xs -> xs | _ -> Array.empty