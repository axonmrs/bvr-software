﻿namespace MRegistrar.Data



module Utils =
  let getValueOrDefault def = function
    | Some x -> x
    | _ -> def

