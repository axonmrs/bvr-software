﻿namespace MRegistrar.Data


module Districts =
  open System
  open Rezoom.SQL
  open Model.Utils


  type GetDistricts = SQL<"""
    Select id, name, region_id, code
    From districts
    Where region_id = @regionId""">
  type GetAll = SQL<"""
    Select id, name, region_id, code
    From districts""">


  type District =
    { id: int64
      name: string
      regionId: int64
      code: string }


  let readByRegion regionId =
    let nameWithoutRegion (x: string) = x.Split('(').[0].Trim()
    GetDistricts.Command regionId
    |> execCmd
    |> Result.map (List.ofSeq >> List.map (fun x -> { id = x.id; name = nameWithoutRegion x.name; regionId = x.region_id; code = x.code }))


  let readAll () =
    let nameWithoutRegion (x: string) = x.Split('(').[0].Trim()
    GetAll.Command ()
    |> execCmd
    |> Result.map (Array.ofSeq >> Array.map (fun x -> { id = x.id; name = nameWithoutRegion x.name; regionId = x.region_id; code = x.code}))    
    |> function Ok xs -> xs | _ -> Array.empty

  