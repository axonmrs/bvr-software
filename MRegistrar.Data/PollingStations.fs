﻿namespace MRegistrar.Data


module PollingStations = 
  open System
  open Rezoom.SQL
  open Model.Utils


  type private GetAll = SQL<"""
    Select id, name, code, electoral_area, constituency, district, region
    From polling_stations""">

  type PollingStation =
    { id: int64
      name: string
      code: string
      electoralArea: string
      constituency: string
      district: string
      region: string }

  let readAll () =
    GetAll.Command ()
    |> execCmd
    |> Result.map (fun xs ->
      xs
      |> Array.ofSeq
      |> Array.map (fun x -> { id = x.id; name = x.name; code = x.code; electoralArea = x.electoral_area; constituency = x.constituency; district = x.district; region = x.region }))
    |> function Ok xs -> xs | _ -> Array.empty

