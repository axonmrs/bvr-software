﻿namespace MRegistrar.Data



module App =
  open System
  open Rezoom
  open Rezoom.Execution
  open Rezoom.SQL
  open Rezoom.SQL.Plans
  open Model.Utils
  open System.Text
  open System.Security.Cryptography


  type private GetUser = SQL<"""
    Select id, name, [type], selected, is_active
    From users
    Where username = @username and password_hash = @password collate nocase and is_active = true and selected = true""">


  type private GetSetting = SQL<"""
    Select name, value
    From app_settings
    Where name = @name""">
  type private GetSettings = SQL<"""
    Select name, value
    From app_settings""">
  type private UpdateSetting = SQL<"""
    Update app_settings
    Set value = @value
    Where name = @name;""">
  type GetDeviceBySerial = SQL<"""
    Select * 
    From devices
    Where serial_number = @serialNumber""">

  let isActivated () =
    GetSetting.Command("activated")
    |> execCmd
    |> Result.map (List.ofSeq)
    |> Result.bind (function
      | [] -> Error (exn "activated setting not found")
      | x :: _ -> Ok (x.value = "true"))

  let activate code =
    GetSetting.Command "activation_code"
    |> execCmd
    |> Result.map (List.ofSeq)
    |> Result.bind (function
      | [] -> Error (exn "activation code setting not found")
      | x :: _ -> 
        match x.value = code with
        | false -> Error (exn "Invalid activation code")
        | true -> 
          let updateResult = execCmd (UpdateSetting.Command ("activated", "true"));
          //todo: what do I do if I'm unable to update the db
          //? should activation be logged
          Ok {| success = true |})

  let getDevice serialNumber = 
    GetDeviceBySerial.Command(serialNumber)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind(function 
      | None -> Ok null
      | Some x -> Ok x )

  let private hash256 (text: string) =
    let bytes = Encoding.UTF8.GetBytes text
    use hasher = new SHA256Managed ()
    let bytes = hasher.ComputeHash bytes
    (StringBuilder(), bytes)
    ||> Array.fold (fun sb -> sprintf "%02x" >> sb.Append)
    |> string
  //let login (username: string, password: string) = "Ok"
  let login (username: string, password: string) =
    let passwordHash = hash256 (username.ToLower() + password)
  
    GetUser.Command (passwordHash, username.ToLower())
    |> execCmd 
    |> Result.map (List.ofSeq)
    |> Result.bind (function
      | [] -> Error (exn "Invalid username/password combination")
      | x :: _ -> 
        //todo: create the token for all future calls 
        let token = username.ToLower()
        let user: Model.User = { name = x.name; ``type`` = x.``type``; token = token; username = username; selected = x.selected; is_active = x.is_active }
        Ok user )

  let mutable config = {| name=""; code=""; activated = false; activationCode = ""; userCount = 0; baseVoterIdNumber = 101000000; startDate = new DateTime(); endDate = new DateTime(); startTime = new TimeSpan(); endTime = new TimeSpan(); inDemoMode = false |} //todo: find a way of making this readonly

  let init () =
    GetSettings.Command ()
    |> execCmd
    |> Result.map List.ofSeq
    |> Result.map (fun xs ->
      (config, xs)
      ||> List.fold (fun s x ->
        match x.name with
        | "ps_code" -> {| s with code = x.value |}
        | "ps_name" -> {| s with name = x.value |}
        | "activation_code" -> {| s with activationCode = x.value |}
        | "activated" -> {| s with activated = System.Boolean.TryParse x.value |> snd |}
        | "number_of_ps_users" -> {| s with userCount = System.Int32.TryParse x.value |> snd |}
        | "base_voter_id_number" -> {| s with baseVoterIdNumber = System.Int32.TryParse x.value |> snd |}
        | "start_date" -> {| s with startDate =  System.DateTime.TryParse x.value |> snd |}
        | "end_date" -> {| s with endDate =  System.DateTime.TryParse x.value |> snd |}
        | "start_time" -> {| s with startTime =  System.TimeSpan.TryParse x.value |> snd |}
        | "end_time" -> {| s with endTime =  System.TimeSpan.TryParse x.value |> snd |}
        | _ -> s))
    |> function 
      | Error _ -> ()
      | Ok x -> config <- x

  let updateSettings xs =
    try
      for (k, v) in xs do
        UpdateSetting.Command(k, v)
        |> execCmd
        |> ignore
      init () //reload the data from the db
      Ok ()
    with | e ->
      Error e

  let demoMode state =
    config <- {| config with inDemoMode = state |}
    Model.Utils.setDemoMode state