﻿namespace MRegistrar.Data


module Registrations =
  open System
  open System.IO
  open System.IO.Compression
  open Rezoom.SQL
  open Model.Utils
  open FSharp.Data
  open System.Text
  open MRegistrar.Security


  type OutputXml = XmlProvider<"Templates/sampleRegisteredVoter.xml">
  ///Gets the voter info (-finger and face templates)
  type FindVoter = SQL<"""
    Select 
      r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
      r.date_of_birth as dateOfBirth, is_estimated_age, r.estimated_age as estimatedAge, r.sex,
      r.phone_number as phoneNumber,
      r.residential_address as residentialAddress, r.residential_town as residentialTown, r.residential_district_id as residentialDistrictId, d1.name as residentialDistrict,
      r.id_type_id as idTypeId, id.name as idType, r.id_number as idNumber, r.id_expiry as idExpiry,
      r.father_name as fatherName, r.mother_name as motherName,
      r.home_town_address as homeTownAddress, r.home_town as homeTown, r.home_town_district_id as homeTownDistrictId, d2.name as homeTownDistrict,
      r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
      r.photo, r.created_at as dateRegistered,
      r1.name as residentialRegion, r1.id as residentialRegionId, r2.name as hometownRegion, r2.id as homeTownRegionId, r.card_print_count as cardPrintCount,
      hearingImpaired, leper, missingFingersL, missingFingersR, amputatedHandsL, amputatedHandsR
    From registered_voters r
    Inner Join districts d1 on d1.id = r.residential_district_id
    Inner Join regions r1 on r1.id = d1.region_id
    Inner Join districts d2 on d2.id = r.home_town_district_id
    Inner Join regions r2 on r2.id = d2.region_id
    left Join id_types id on id.id = r.id_type_id
    Where r.voter_id = @voterId;""">
  type FindFromOldRegister = SQL<"""
    Select 
      r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
      r.date_of_birth as dateOfBirth, r.estimated_age as estimatedAge, r.sex,
      r.phone_number as phoneNumber,
      r.residential_address as residentialAddress, r.residential_town as residentialTown, r.residential_district_id as residentialDistrictId, rd.region_id as residentialRegionId,
      r.id_type_id as idTypeId, r.id_number as idNumber, r.id_expiry as idExpiry,
      r.father_name as fatherName, r.mother_name as motherName,
      r.home_town_address as homeTownAddress, r.home_town as homeTown, r.home_town_district_id as homeTownDistrictId, hd.region_id as homeTownRegionId,
      r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
      r.photo,
      hd.name as hometownDistrict, rd.name as residentialDistrict,
      rr.name as residentialRegion, hr.name as hometownRegion
    From old_voters_register r
    Left Join districts as hd on hd.id = r.home_town_district_id
    Inner Join regions hr on hr.id = hd.region_id
    Left Join districts as rd on rd.id = r.residential_district_id
    Inner Join regions rr on rr.id = rd.region_id
    Where r.voter_id = @voterId;""">
  type GetVoters = SQL<"""
    Select 
      r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
      r.date_of_birth as dateOfBirth, is_estimated_age, r.estimated_age as estimatedAge, r.sex,
      r.phone_number as phoneNumber,
      r.residential_address as residentialAddress, r.residential_town as residentialTown, r.residential_district_id as residentialDistrictId, d1.name as residentialDistrict,
      r.id_type_id as idTypeId, id.name as idType, r.id_number as idNumber, r.id_expiry as idExpiry,
      r.father_name as fatherName, r.mother_name as motherName,
      r.home_town_address as homeTownAddress, r.home_town as homeTown, r.home_town_district_id as homeTownDistrictId, d2.name as homeTownDistrict,
      r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
      r.photo as photoBytes, '' as photo, r.created_at as dateRegistered
    From registered_voters r
    Inner Join districts d1 on d1.id = r.residential_district_id
    Inner Join id_types id on id.id = r.id_type_id
    Left Join districts d2 on d2.id = r.home_town_district_id
    Order by r.id
    Limit @batchSize Offset @offset;""">
  type private GetLastVoterId = SQL<"""Select max(voter_id) as voterId from registered_voters where ps_code = @psCode""">
  type private InsertVoter = SQL<"""
    Insert into registered_voters 
      (voter_id, ps_name, ps_code, surname, other_names, date_of_birth, estimated_age, sex, phone_number, registered_by, residential_address, residential_town, residential_district_id, id_type_id, id_number, id_expiry, father_name, mother_name, home_town_address, home_town, home_town_district_id, is_visually_impaired, is_disabled, photo, face_template, left_thumb, left_index, left_middle, left_ring, left_pinky, right_thumb, right_index, right_middle, right_ring, right_pinky, created_at, backed_up, backed_up_on, guarantor1_voter_id, guarantor2_voter_id, guarantor1_name, guarantor2_name, is_estimated_age, card_print_count, transmitted, transmitted_on, left_thumb_tpl, left_index_tpl, left_middle_tpl, left_ring_tpl, left_pinky_tpl, right_thumb_tpl, right_index_tpl, right_middle_tpl, right_ring_tpl, right_pinky_tpl, hearingImpaired, leper, missingFingersL, missingFingersR, amputatedHandsL, amputatedHandsR, compositeTemplate, challengeReasons, photo_j2k)
    values 
      (@voterId, @psName, @psCode, @surname, @otherName, @dateOfBirth, @estimatedAge, @sex, @phoneNumber, @registeredBy, @residentialAddress, @residentialTown, @residentialDistrictId, @idTypeId, @idNumber, @idExpiry, @fatherName, @motherName, @homeTownAddress, @homeTown, @homeTownDistrictId, @isVisuallyImpaired, @isDisabled, @photo, @faceTemplate, @leftThumb, @leftIndex, @leftMiddle, @leftRing, @leftPinky, @rightThumb, @rightIndex, @rightMiddle, @rightRing, @rightPinky, @createdAt, @backedUp, @backedUpOn, @guarantor1VoterId, @guarantor2VoterId, @guarantor1Name, @guarantor2Name, @isEstimatedAge, @cardPrintCount, @transmitted, @transmittedOn, @left_thumb_tpl, @left_index_tpl, @left_middle_tpl, @left_ring_tpl, @left_pinky_tpl, @right_thumb_tpl, @right_index_tpl, @right_middle_tpl, @right_ring_tpl, @right_pinky_tpl, @hearingImpaired, @leper, @missingFingersL, @missingFingersR, @amputatedHandsL, @amputatedHandsR, @compositeTemplate, @challengeReasons, @photoJ2k);""">
  type private UpdateVoter = SQL<"""
    --insert
    Update registered_voters Set
      surname = @surname, other_names = @other_names, date_of_birth = @date_of_birth, estimated_age = @estimated_age, sex = @sex, phone_number = @phone_number, residential_address = @residential_address, residential_town = @residential_town, residential_district_id = @residential_district_id, father_name = @father_name, mother_name = @mother_name, home_town_address = @home_town_address, home_town = @home_town, home_town_district_id = @home_town_district_id, is_visually_impaired = @is_visually_impaired, is_disabled = @is_disabled, is_estimated_age = @is_estimated_age, transmitted = false, hearingImpaired = @hearingImpaired, leper = @leper, missingFingersL = @missingFingersL, missingFingersR = @missingFingersR, amputatedHandsL = @amputatedHandsL, amputatedHandsR = @amputatedHandsR
    Where voter_id = @voter_id
    --todo: add the log
  """>
  type GetForExport = SQL<"""
    Select r.*, rd.code as residentialDistrictCode, hd.code as homeTownDistrictCode, id.code as idCode
    From registered_voters r
    Inner Join districts rd on rd.id = r.residential_district_id
    Inner Join districts hd on hd.id = r.home_town_district_id
    Inner Join Id_Types id on id.id = r.id_type_id
    Where @voterId is null or voter_id = @voterId"""> //todo: add a filter and a batch size. Also update when migration is done

  type GetPendingTransmission = SQL<"""
    Select r.*, rd.code as residentialDistrictCode, hd.code as homeTownDistrictCode, id.code as idCode
    From registered_voters r
    Inner Join districts rd on rd.id = r.residential_district_id
    Inner Join districts hd on hd.id = r.home_town_district_id
    Inner Join Id_Types id on id.id = r.id_type_id
    Limit @batchSize""">

  type GetPendingTransmission2 = SQL<"""
    Select id, voter_id, ps_name, ps_code, surname, other_names, date_of_birth, is_estimated_age, estimated_age, sex, phone_number, registered_by, residential_address, residential_town, residential_district_id, id_type_id, id_number, id_expiry, father_name, mother_name, home_town, 
      transmitted_on
    From registered_voters
    Where transmitted = false
    Limit @batchSize""">

  type FindVoters = SQL<"""
    Select
      r.id, r.voter_id as voterId, r.surname, r.other_names as otherNames,
      r.date_of_birth as dateOfBirth, is_estimated_age, r.estimated_age as estimatedAge, r.sex,
      r.phone_number as phoneNumber,
      r.is_visually_impaired as isVisuallyImpaired, r.is_disabled as isDisabled,
      r.photo as photo, r.created_at as dateRegistered
    From registered_voters as r
    Where (@voterId is not null and voter_id = @voterId) or (@name is not null and (surname like '%' || @name ||  '%' or other_names like '%' || @name ||  '%'))""">

  type private UpdateCardPrintCount = SQL<"""
    Update registered_voters
    Set card_print_count = @cardPrintCount
    Where voter_id = @voterId;""">

  type CountPendingBackups = SQL<"""
    Select count(*) as cnt from registered_voters where backed_up = false;""">

  type CountPendingTransfers = SQL<"""
    Select count(*) as cnt from registered_voters where transmitted = false;""">

  type FindReportLog = SQL<"""
    Select * From report_logs Where [type] = @type and date >= @date""">

  type UpdateTransmittedRegistrations = SQL<"""
    Update registered_voters
    Set transmitted = true, transmitted_on = @transmittedDate
    Where id >= @minId and id <= @maxId""">

  type UpdateRegistrationData =
    { surname: string
      otherNames: string
      dateOfBirth: DateTime
      estimatedAge: int
      sex: string
      phoneNumber: string
      residentialAddress: string
      residentialTown: string
      residentialDistrictId: int64
      fatherName: string
      motherName: string
      homeTownAddress: string
      homeTown: string
      homeTownDistrictId: int64
      isVisuallyImpaired: bool
      isDisabled: bool
      isEstimatedAge: bool
      hearingImpaired: bool
      leper: bool
      missingFingersL: bool
      missingFingersR: bool
      amputatedHandsL: bool
      amputatedHandsR: bool
      voterId: string }

  type Registration =
    { surname: string
      otherNames: string
      dateOfBirth: DateTime
      estimatedAge: int
      sex: string
      phoneNumber: string
      residentialAddress: string
      residentialTown: string
      residentialDistrictId: int64
      idTypeId: int64
      idNumber: string
      idExpiry: DateTime
      fatherName: string
      motherName: string
      homeTownAddress: string
      homeTown: string
      homeTownDistrictId: int64
      isVisuallyImpaired: bool
      isDisabled: bool
      guarantor1VoterNumber: string
      guarantor2VoterNumber: string
      guarantor1Name: string
      guarantor2Name: string
      isEstimatedAge: bool
      photo: byte[]
      photoJ2k: byte[]
      faceTemplate: byte[]
      leftThumb: byte[]
      leftIndex: byte[]
      leftMiddle: byte[]
      leftRing: byte[]
      leftPinky: byte[]
      rightThumb: byte[]
      rightIndex: byte[]
      rightMiddle: byte[]
      rightRing: byte[]
      rightPinky: byte[]
      leftThumbTpl: byte[]
      leftIndexTpl: byte[]
      leftMiddleTpl: byte[]
      leftRingTpl: byte[]
      leftPinkyTpl: byte[]
      rightThumbTpl: byte[]
      rightIndexTpl: byte[]
      rightMiddleTpl: byte[]
      rightRingTpl: byte[]
      rightPinkyTpl: byte[]
      compositeTemplate: byte[]
      hearingImpaired: bool
      leper: bool
      missingFingersL: bool
      missingFingersR: bool
      amputatedHandsL: bool
      amputatedHandsR: bool
      challengeReasons: string }
  type SaveRegistrationResult =
    { idNumber: string }

  type GetCounts = SQL<"""
    Select count(*) as cnt from registered_voters;
    Select count(*) as cnt from registered_voters where created_at >= @date;""">

  let toDataUri mime bytes =
    sprintf "data:%s;base64,%s" mime (Convert.ToBase64String bytes)
  let readAll (offset, pageSize) =
    GetVoters.Command(int64 pageSize, int64 offset)
    |> execCmd
    |> Result.map List.ofSeq


  let find voterId =
    FindVoter.Command(voterId)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind (function
      | None -> Error (exn "Not found")
      | Some x -> Ok x)
  let findFromOldRegister voterId =
    FindFromOldRegister.Command(voterId)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind (function
      | None -> Error (exn "Not found")
      | Some x -> Ok x)
  let findVoters (voterId, name) =
    let name = match isNull name with true -> None | _ -> Some name
    let voterId = match isNull voterId with true -> None | _ -> Some voterId
    FindVoters.Command (name, voterId)
    |> execCmd
    |> Result.map Array.ofSeq
    |> function Ok xs -> xs | _ -> Array.empty


  let private b2s (xs: byte[]) = 
    match xs with
    | null -> ""
    | _ -> Convert.ToBase64String xs
  let private dt2s (x: DateTime) = x.ToString "yyyy-MM-dd HH:mm:ss"
  let private d2s (x: DateTime) = x.ToString "yyyy-MM-dd"
  let getXml voterId =
    GetForExport.Command(Some voterId)
    |> execCmd
    |> Result.map Seq.tryHead
    |> Result.bind (function
      | None -> Error (exn "Not found")
      | Some x -> 
        let audit = OutputXml.Audit (x.id, dt2s DateTime.Now, x.registered_by)
        let fingerTemplates = OutputXml.FingerTemplates(b2s x.left_thumb, b2s x.left_index, b2s x.left_middle, b2s x.left_ring, b2s x.left_pinky, b2s x.right_thumb, b2s x.right_index, b2s x.right_middle, b2s x.right_ring, b2s x.right_pinky)
        let fingerWsqs = OutputXml.FingerWsq(b2s x.left_thumb_tpl, b2s x.left_index_tpl, b2s x.left_middle_tpl, b2s x.left_ring_tpl, b2s x.left_pinky_tpl, b2s x.right_thumb_tpl, b2s x.right_index_tpl, b2s x.right_middle_tpl, b2s x.right_ring_tpl, b2s x.right_pinky_tpl)
        let bio = OutputXml.Bio (b2s x.photo, b2s x.photo_j2k, b2s x.face_template, fingerTemplates, fingerWsqs)
        let details = OutputXml.Details (x.surname, x.other_names, d2s x.date_of_birth, string x.estimated_age, (match x.is_estimated_age with None -> false | Some x -> x), x.sex, x.phone_number, x.residential_address, x.residential_town, x.residential_district_id, x.residentialDistrictCode, string x.id_type_id, x.idCode, x.id_number, d2s x.id_expiry, x.father_name, x.mother_name, x.home_town_address, x.home_town, string x.home_town_district_id, x.homeTownDistrictCode, x.is_visually_impaired, x.is_disabled, x.hearingImpaired, x.leper, x.missingFingersL, x.missingFingersR, x.amputatedHandsL, x.amputatedHandsR, x.challengeReasons)
        let source = OutputXml.Source (x.ps_name, x.ps_code)
        let xml = OutputXml.RegisteredVoter (source, audit, details, bio)
        Ok (string xml))
  ///Generates a zip file with a list of registrations
  let getXmls () =
    GetForExport.Command None
    |> execCmd
    |> Result.map List.ofSeq
    |> Result.bind (function
      | [] -> Error (exn "No registrations found pending export")
      | xs ->
        //prepare the stream
        use stream = new MemoryStream()
        do
          use archive = new ZipArchive (stream, ZipArchiveMode.Create, true)
          xs
          |> List.iter (fun x ->
            let audit = OutputXml.Audit (x.id, dt2s DateTime.Now, x.registered_by)
            let fingerTemplates = OutputXml.FingerTemplates(b2s x.left_thumb, b2s x.left_index, b2s x.left_middle, b2s x.left_ring, b2s x.left_pinky, b2s x.right_thumb, b2s x.right_index, b2s x.right_middle, b2s x.right_ring, b2s x.right_pinky)
            let fingerWsqs = OutputXml.FingerWsq(b2s x.left_thumb_tpl, b2s x.left_index_tpl, b2s x.left_middle_tpl, b2s x.left_ring_tpl, b2s x.left_pinky_tpl, b2s x.right_thumb_tpl, b2s x.right_index_tpl, b2s x.right_middle_tpl, b2s x.right_ring_tpl, b2s x.right_pinky_tpl)
            let bio = OutputXml.Bio (b2s x.photo, b2s x.photo_j2k, b2s x.face_template, fingerTemplates, fingerWsqs)
            let details = OutputXml.Details (x.surname, x.other_names, d2s x.date_of_birth, string x.estimated_age, (match x.is_estimated_age with None -> false | Some x -> x), x.sex, x.phone_number, x.residential_address, x.residential_town, x.residential_district_id, x.residentialDistrictCode, string x.id_type_id, x.idCode, x.id_number, d2s x.id_expiry, x.father_name, x.mother_name, x.home_town_address, x.home_town, string x.home_town_district_id, x.homeTownDistrictCode, x.is_visually_impaired, x.is_disabled, x.hearingImpaired, x.leper, x.missingFingersL, x.missingFingersR, x.amputatedHandsL, x.amputatedHandsR, x.challengeReasons)
            let source = OutputXml.Source (x.ps_name, x.ps_code)
            let xml = OutputXml.RegisteredVoter (source, audit, details, bio)
            let zipEntry = archive.CreateEntry (sprintf "%s.xml" x.voter_id)
            use writer = new StreamWriter (zipEntry.Open())
            writer.Write (string xml))
        Ok (stream.ToArray))
//todo: consider refactoring all the code that generates the xml and zip. They can be set to write to file or return the needed stream or byte []
  let saveBatch (key, salt, data: GetPendingTransmission.Row list) dirToWriteTo indexOfTheBatch =
    //prepare the stream
    let salt = sprintf "%s-%i" salt data.Length
    let encrypt voterId (text: string) =
      let bytes = Encoding.UTF8.GetBytes text
      AES.Encrypt(key, salt, bytes) 
      |> Convert.ToBase64String
      |> sprintf "<encrypted_voter id=%s>%s</encrypted_voter>" voterId
    Directory.CreateDirectory(dirToWriteTo) |> ignore
    let filename = Path.Combine(dirToWriteTo, sprintf "batch-%i.zip" indexOfTheBatch)
    use stream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write)
    do
      use archive = new ZipArchive (stream, ZipArchiveMode.Create, true)
      data
      |> List.iter (fun x ->
        let audit = OutputXml.Audit (x.id, dt2s DateTime.Now, x.registered_by)
        let fingerTemplates = OutputXml.FingerTemplates(b2s x.left_thumb, b2s x.left_index, b2s x.left_middle, b2s x.left_ring, b2s x.left_pinky, b2s x.right_thumb, b2s x.right_index, b2s x.right_middle, b2s x.right_ring, b2s x.right_pinky)
        let fingerWsqs = OutputXml.FingerWsq(b2s x.left_thumb_tpl, b2s x.left_index_tpl, b2s x.left_middle_tpl, b2s x.left_ring_tpl, b2s x.left_pinky_tpl, b2s x.right_thumb_tpl, b2s x.right_index_tpl, b2s x.right_middle_tpl, b2s x.right_ring_tpl, b2s x.right_pinky_tpl)
        let bio = OutputXml.Bio (b2s x.photo, b2s x.photo_j2k, b2s x.face_template, fingerTemplates, fingerWsqs)
        let details = OutputXml.Details (x.surname, x.other_names, d2s x.date_of_birth, string x.estimated_age, (match x.is_estimated_age with None -> false | Some x -> x), x.sex, x.phone_number, x.residential_address, x.residential_town, x.residential_district_id, x.residentialDistrictCode, string x.id_type_id, x.idCode, x.id_number, d2s x.id_expiry, x.father_name, x.mother_name, x.home_town_address, x.home_town, string x.home_town_district_id, x.homeTownDistrictCode, x.is_visually_impaired, x.is_disabled, x.hearingImpaired, x.leper, x.missingFingersL, x.missingFingersR, x.amputatedHandsL, x.amputatedHandsR, x.challengeReasons)
        let source = OutputXml.Source (x.ps_name, x.ps_code)
        let xml = OutputXml.RegisteredVoter (source, audit, details, bio)
        let zipEntry = archive.CreateEntry (sprintf "%s.xml" x.voter_id)
        use writer = new StreamWriter (zipEntry.Open())
        let xmlText = string xml |> encrypt x.voter_id
        writer.Write xmlText)

  let getNexTempFolder () = @"xml_backups_" + string DateTime.Now.Ticks

  let transmit key salt =
    GetPendingTransmission.Command 2000L
    |> execCmd 
    |> function
      | Error x -> Error x
      | Ok xs ->
        try
          let dirToWriteTo = getNexTempFolder ()
          let xs = List.ofSeq xs
          match xs with
          | [] -> Ok (false, "No pending transmissions")
          | xs ->
              xs
              |> List.chunkBySize 100 //the batch sizes
              |> List.iteri (fun i x -> saveBatch (key, salt, x) dirToWriteTo i)
              
              let minId = xs |> List.minBy (fun x -> x.id)
              let maxId = xs |> List.maxBy (fun x -> x.id)
              // update the records
              UpdateTransmittedRegistrations.Command(maxId.id, minId.id, Some DateTime.Now)
              |> execCmd
              |> ignore //todo: log this if it failed
              
              Ok (true, dirToWriteTo) //assuming batch is complete
        with | e ->
          Error e

  let mutable private lastVoterId = 0L //get at the start of the app
  let private getVoterNumber (voterId: int64) =
    let x = string voterId
    let sub = x.[0 .. 8]
    sub
    |> Seq.map (string >> int)
    |> Seq.sum
    |> fun x -> x % 10
    |> sprintf "%s%i" sub
  let isValidVoterNumber (voterId: string) =
    if System.String.IsNullOrWhiteSpace(voterId) then false
    else if (voterId.Length < 10) then false;
    else
      let newId =
        let sub = voterId.[0 .. 8]
        sub
        |> Seq.map (string >> int)
        |> Seq.sum
        |> fun x -> x % 10
        |> sprintf "%s%i" sub
      newId = voterId
      
  
  let save username (x: Registration) =
    let cardPrintCount = 1
    let createdAt = DateTime.UtcNow
    let backedUp = false
    let backedUpOn = DateTime.UtcNow.AddYears(-100)
    let transmitted = false
    let transmittedOn = DateTime.UtcNow.AddYears(-100)
    lastVoterId <- lastVoterId + 1L
    let voterId = getVoterNumber lastVoterId
    let cmd = InsertVoter.Command (x.amputatedHandsL, x.amputatedHandsR, backedUp, backedUpOn, cardPrintCount, x.challengeReasons, x.compositeTemplate, createdAt, x.dateOfBirth, x.estimatedAge, x.faceTemplate, x.fatherName, Some x.guarantor1Name, Some x.guarantor2VoterNumber, Some x.guarantor2Name, Some x.guarantor2VoterNumber, x.hearingImpaired, x.homeTown, x.homeTownAddress, x.homeTownDistrictId, x.idExpiry, x.idNumber, x.idTypeId, x.isDisabled, Some x.isEstimatedAge, x.isVisuallyImpaired, x.leftIndex, x.leftMiddle, x.leftPinky, x.leftRing, x.leftThumb, x.leftIndexTpl, x.leftMiddleTpl, x.leftPinkyTpl, x.leftRingTpl, x.leftThumbTpl, x.leper, x.missingFingersL, x.missingFingersR, x.motherName, x.otherNames, x.phoneNumber, x.photo, x.photoJ2k, App.config.code, App.config.name, username, x.residentialAddress, x.residentialDistrictId, x.residentialTown, x.rightIndex, x.rightMiddle, x.rightPinky, x.rightRing, x.rightThumb, x.rightIndexTpl, x.rightMiddleTpl, x.rightPinkyTpl, x.rightRingTpl, x.rightThumbTpl, x.sex, x.surname, transmitted, Some transmittedOn, voterId)
    cmd
    |> execCmd
    |> function
      | Ok _ -> Ok { idNumber = voterId }
      | Error e ->
        printfn "Error saving registration: %A" e
        lastVoterId <- lastVoterId - 1L
        Error e

  let update (x: UpdateRegistrationData) =
    UpdateVoter.Command (x.amputatedHandsL, x.amputatedHandsR, x.dateOfBirth, x.estimatedAge, x.fatherName, x.hearingImpaired, x.homeTown, x.homeTownAddress, x.homeTownDistrictId, x.isDisabled, Some x.isEstimatedAge, x.isVisuallyImpaired, x.leper, x.missingFingersL, x.missingFingersR, x.motherName, x.otherNames, x.phoneNumber, x.residentialAddress, x.residentialDistrictId, x.residentialTown, x.sex, x.surname, x.voterId)
    |> execCmd
    |> function
      | Ok _ -> Ok ()
      | Error e ->
        //todo: log
        Error e

  let getNextVoterIdNumber () =
    (lastVoterId + 1L) 
    |> getVoterNumber

  ///The number of registrations done so far
  let registrationCount () =
    let date = System.DateTime.Now.Date
    GetCounts.Command date
    |> execCmd
    |> Result.map (fun xs ->
      let allCnt = xs.ResultSet1 |> Seq.tryHead |> function None -> 0L | Some x -> x.cnt
      let todayCount = xs.ResultSet2  |> Seq.tryHead |> function None -> 0L | Some x -> x.cnt
      {| all = allCnt; today = todayCount |})
  
  let updateCardPrintCount voterId count =
    UpdateCardPrintCount.Command(count, voterId)
    |> execCmd

  let hasPendingBackups () =
    CountPendingBackups.Command ()
    |> execCmd
    |> Result.map Seq.tryHead
    |> function
      | Error _
      | Ok None -> false
      | Ok (Some x) -> (x.cnt > 0L)

  let hasPendingTransfers () =
    CountPendingTransfers.Command ()
    |> execCmd
    |> Result.map Seq.tryHead
    |> function
      | Error _
      | Ok None -> false
      | Ok (Some x) -> (x.cnt > 0L)

  let hasPrintedReport reportType date =
    FindReportLog.Command(date, reportType)
    |> execCmd
    |> function
      | Error e -> Error (false, e)
      | Ok x -> Ok (x.Count > 0)

  let init () =
    GetLastVoterId.Command (App.config.code)
    |> execCmd
    |> Result.map Seq.tryHead
    |> function
      | Error _
      | Ok None
      | Ok (Some null) -> App.config.baseVoterIdNumber
      | Ok (Some x) -> 
        match x.voterId with
        | "" | null -> App.config.baseVoterIdNumber
        | _ ->
          match Int32.TryParse x.voterId.[0..8] with
          | false, _ -> App.config.baseVoterIdNumber
          | _, x -> x
    |> fun x -> lastVoterId <- int64 x