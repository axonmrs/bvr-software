﻿namespace MRegistrar.Data


module Regions =
  open System
  open Rezoom.SQL
  open Model.Utils


  type private GetAll = SQL<"""
    Select id, name, code
    From Regions""">

  type Region =
    { id: int64
      name: string
      code: string }

  let readAll () =
    GetAll.Command ()
    |> execCmd
    |> Result.map (fun xs ->
      xs
      |> Array.ofSeq
      |> Array.map (fun x -> { id = x.id; name = x.name; code = x.code }))
    |> function Ok xs -> xs | _ -> Array.empty