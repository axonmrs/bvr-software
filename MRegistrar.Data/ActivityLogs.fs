﻿namespace MRegistrar.Data


module ActivityLogs = 
  open System
  open Rezoom.SQL
  open Model.Utils

  type private InsertActivityLog = SQL<"""
    Insert into activity_logs(date, activity_type, description, created_by)
    Values(@date, @activityType, @description, @createdBy);""">

  type GetActivityLogs = SQL<"""Select * From activity_logs Limit @batchSize Offset @offset;""">

  type ActivityLog = 
    { activityType: string
      description: string
      createdBy: string }

  let date = DateTime.Now
  let save (data: ActivityLog) = 
    InsertActivityLog.Command(data.activityType, data.createdBy, date, data.description)
    |> execCmd

