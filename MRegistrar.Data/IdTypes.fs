﻿namespace MRegistrar.Data


module IdTypes =
  open Rezoom.SQL
  open Model.Utils


  type private GetIdTypes = SQL<"""
    Select id, name, code
    From id_types""">

  type IdType =
    { id: int64
      name: string
      code: string }


  let readAll () =
    GetIdTypes.Command ()
    |> execCmd
    |> Result.map (Array.ofSeq >> Array.sortBy (fun x -> x.name) >> Array.map (fun x -> { id = x.id; name = x.name; code = x.code }))
    |> function Ok xs -> xs | _ -> Array.empty