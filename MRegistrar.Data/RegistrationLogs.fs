﻿namespace MRegistrar.Data


module RegistrationLogs = 
  open System
  open Rezoom.SQL
  open Model.Utils

  type private InsertRegistrationLog = SQL<"""
    Insert into registration_logs(voter_id, date, reason, ps_code)
    Values(@voterId, @date, @reason, @psCode);""">

  type GetRegistrationLogs = SQL<"""Select * From registration_logs Limit @batchSize Offset @offset;""">

  type RegistrationLog = 
    { voterId: string
      reason: string
      psCode: string }
  
  let date = DateTime.Now
  let save (data: RegistrationLog) = 
    InsertRegistrationLog.Command(date, data.psCode, data.reason, data.voterId)
    |> execCmd
